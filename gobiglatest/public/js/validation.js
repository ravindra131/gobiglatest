function checkemail()
    {
		$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	   var email=$('#email').val();
	   if(email != '')
	   {
	       $.ajax({
			   type: 'post',
			   url: '/auth/checkemail',
			   data: {
			   user_email:email
			   },
			   success: function (response) {
			   	//console.log(response);
		       if(response==1)	
               {
               	  $('#emailerror').show();
				  $('#emailerror').html('This e-mail address already exists.');
				  $('#email').val('');	
               }
               else
               {
                 $('#emailerror').hide();
				 $('#emailerror').html('');
				 //$("#continue").prop("disabled",false);
               }
             }
		   });
		}
	}

function checkdomain(value)
{
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	if(value)
	{
		$.ajax({
			   type: 'post',
			   url: '/auth/checkdomain',
			   data: {
			   user_domain:value
			   },
			   success: function (response) {
                            if(response==1)	
                             {
                                 $('#domerror').show();
                                 $('#domerror').html('This domain name already exists.');
                                 //$('#domain').val('');	
                                 $("#continue").prop("disabled",true);
                             }
                             else
                             {
                                 $('#domerror').hide();
                                 $('#domerror').html('');
                                 $("#continue").prop("disabled",false);
                             }
                            }
                });
	}
}