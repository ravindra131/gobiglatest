 var $jq = jQuery.noConflict();
$jq(document).ready(function() {
    $jq('.collapse').on('show.bs.collapse', function() {
        var id = $jq(this).attr('id');
        $jq('a[href="#' + id + '"]').closest('.panel-heading').addClass('active-faq');
        $jq('a[href="#' + id + '"] .panel-title span').html('<i class="fa fa-caret-up"></i>');
    });
    $jq('.collapse').on('hide.bs.collapse', function() {
        var id = $jq(this).attr('id');
        $jq('a[href="#' + id + '"]').closest('.panel-heading').removeClass('active-faq');
        $jq('a[href="#' + id + '"] .panel-title span').html('<i class="fa fa-caret-down"></i>');
    });



    $jq('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    
	
	}); 