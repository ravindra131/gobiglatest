@extends('back.template')
@section('head')
	<style type="text/css">
		.table { margin-bottom: 0; }
		.panel-heading { padding: 0 15px; }
	</style>
	{!! HTML::style('/css/bootstrap-datepicker.css') !!}
@stop
@section('main')
{!! Form::open(['class' => ' form-horizontal', 'method' => 'post']) !!}
{!! csrf_field() !!}
<div class="form-group m-t-20">
<div class="col-md-2 col-sm-3">
	{!! Form::text('agentname', null, [
            'class'                         => 'form-control',
            'id'                            => 'agentname',
            'placeholder'                   => 'Member name'
        ]) !!}
</div>
<div class="col-md-3 col-sm-3">
<select name="package" id="package" class="form-control" style="color:#888383;">
	<option value="">Select Package</option>
	<option value="1">Monthly</option>
	<option value="2">Annual</option>
</select>
</div>
<div class="col-md-3 col-sm-3">
		 <input type="text" name="fromdate" id="fromdate" placeholder="Start Date" class="form-control datepicker input-info">
		 </div>
<div class="col-md-3 col-sm-3">
<input type="text" name="todate" id="todate" placeholder="End Date" class="form-control datepicker input-info ">
</div>
<div class="col-md-1 col-sm-12">
<a class="btn btn-primary" href="#" id="search_order">Search</a>
</div>
</div>
<h1 class="page-header row no-margin"><span class="orders">Orders</span>
<div class="col-md-2 col-sm-5 pull-right">
<label class="fs-15">Records per page</label>
<select name="records" id="records" class="form-control" onchange="doAjaxSearchUsers()">
	<option value="10">10</option>
	<option value="20">20</option>
	<option value="30">30</option>
</select>
</div></h1>

{!! Form::close() !!}
<p class="text-right pcsv"><a href="#" onclick="save_orders()" class="csv-btn"><span> Export as <img src="{{ asset('images/csv-icon.png') }}"></span></a></p>
<p id="totalorders" class="m-t-20 text-right">Total Orders:{{ $total_orders }}</p>
	
		<div class="panel" id="ResultContainer">
				@include('back.order.part')
			</div>
		</div>
@stop
@section('scripts')
<script>
 	$('#search_order').on('click',function()
 	{
        doAjaxSearchUsers();
				 
    });
    $('#agentname').keydown(function (e){
    if(e.keyCode == 13){
        doAjaxSearchUsers();
    }
});
    $('#orderdate').keydown(function (e){
    if(e.keyCode == 13){
        doAjaxSearchUsers();
    }
});

    function doAjaxSearchUsers()
	{
		var agentname = $('#agentname').val();
		var packageid = $('#package').val();
		var fromdate = $('#fromdate').val();
		var todate = $('#todate').val();
		var recordno = $('#records').val();
		var dataString  = {SearchText:agentname,package_id:packageid,from_date:fromdate,to_date:todate,record_no:recordno};
			$.ajax({                       
			   url :  "/admin/search/order",
			   type: "GET",
			   dataType: 'JSON',
			   data : dataString, 
			   cache:false,
			   success : function(data){
				   console.log(data);
				   //alert(data.count);
				   if(data.count > 0)
				   {
				   	 $('#ResultContainer').html(data.html);
				   	 $('#totalorders').html('Total Orders:' +data.count);
				   }
				   else
				   {
				   		$('#ResultContainer').html('No records found');
				   		$('#totalorders').html('Total Orders:' +data.count);
				   }
				 
			   }               
		   });
	}

	function save_orders()
	{
		doAjaxSearchUsers();
		var agentname = $('#agentname').val();
		var packageid = $('#package').val();
		var fromdate = $('#fromdate').val();
		var todate = $('#todate').val();
		var dataString  = {SearchText:agentname,package_id:packageid,from_date:fromdate,to_date:todate};
		window.location.href = '{!! url('/admin/download-orders/') !!}' + "?" + serialize(dataString);
	}

 function serialize(obj) {
  var str = [];
  for(var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}
	
</script>
@stop


