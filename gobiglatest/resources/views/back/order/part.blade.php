@if(!empty($orders))

				<table class="table">
					<thead>
						<tr>
							<th class="col-lg-1">Full name</th>
							<th class="col-lg-1">Package name</th>
							<th class="col-lg-1">Amount</th>
							<th class="col-lg-1">Order Date</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($orders as $order)
					@if($order->agentname != '')
						<tr>
							<td class="text-primary"><strong><a href="{!! route('user.show', [$order->user_id]) !!}">{{ ucfirst($order->agentname) }}</a></strong></td>
							@if($order->package_id == '1')
							<td>Monthly</td>
							@elseif($order->package_id == '2')
							<td>Annual</td>
							@elseif($order->package_id == '3')
							<td>Free</td>
							@endif
							<td>${{ $order->amount }}</td>
							<td>{{ date('Y-m-d',strtotime($order->created_at)) }}</td>
						</tr>
						@endif
					@endforeach
					</tbody>
				</table>
				<div class="pull-right link">
				<?php echo $orders->render(); ?>
			</div>
				@else
                <p style="text-align: center;">There is no data available.</p> 
				@endif