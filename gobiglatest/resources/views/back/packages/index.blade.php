@extends('back.template')

@section('head')

	<style type="text/css">
		.table { margin-bottom: 0; }
		.panel-heading { padding: 0 15px; }
	</style>

@stop

@section('main')

 <!-- Entête de page -->
  @include('back.partials.entete', ['title' => trans('back/messages.dashboard'), 'icone' => 'dropbox', 'fil' => trans('back/messages.messages')])

  
		<div class="panel">
		  <div class="panel-heading2">
		  	@if (Session::has('message'))
    			<div class="alert alert-success">{{ Session::get('message') }}</div>
			@endif
				<table class="table">
					<thead>
						<tr>
							<th class="col-lg-1">{{ trans('back/messages.month') }}</th>
							<th class="col-lg-1">{{ trans('back/messages.annual') }}</th>
							<th class="col-lg-1">Discount (%)</th>
							<th class="col-lg-1">Action</th>
						</tr>
					</thead>
					@foreach ($messages as $message)
					<tbody>
						<tr>
							<td>${{ $message->monthly_fee }}</td>
							<td>${{ $message->annual_fee }}</td>
							<td>{{ round(((($message->monthly_fee*12)-($message->annual_fee))*100)/($message->monthly_fee*12)) }}</td>
							<td>{!! link_to_route('package.edit', trans('back/users.edit'), [$message->id], ['class' => 'btn btn-warning fa fa-pencil ']) !!}</td>
						</tr>
					</tbody>
					@endforeach
				</table>	
			</div>
		</div>
	

@stop
