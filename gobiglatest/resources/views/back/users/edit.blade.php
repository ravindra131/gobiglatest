@extends('back.template')
@section('head')
    {!! HTML::style('css/parsley.css') !!}
@stop
@section('main')

	<!-- Entête de page -->
	@include('back.partials.entete', ['title' => trans('back/users.editdetails'), 'icone' => 'user', 'fil' => link_to('user', trans('back/users.Users')) . ' / ' . trans('back/users.edition')])
	@if (Session::has('message'))
    <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
	<div class="col-sm-12">
		{!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'put', 'class' => 'panel','data-parsley-validate', 'files' => true]) !!}
			<div class="row ">
			<div class=" col-xs-6 col-sm-4">{!! Form::control('text', 0, 'username', $errors, trans('back/users.name')) !!}</div>
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('email', 0, 'email', $errors, trans('back/users.email')) !!}
			</div>
			</div>
			<div class="row">
			<div class="col-xs-6 col-sm-4">
			{!! Form::control('text', 0, 'domain_name', $errors, trans('back/users.domainname')) !!}
			<em> <a class="showDomainNameAnchor" href="" target="_blank">http://<span class="showDomainName"></span>.users.gobignewsletter.stagemyapp.com</a></em>
			</div>
			<div class="form-group  col-xs-6 col-sm-4">
				<label for="companyname" class="control-label">Title</label>
				{!! Form::text('companyname', $companyname, [
            'class'                         => 'form-control',
            
             'id'       => 'inputCompanyname'
        	]) !!}
				
			</div>
			</div>
			<div class="row m-t-50">
			<div class="form-group  col-xs-6 col-sm-4">
				<label for="theme_id" class="control-label">Select Image</label>
				<select name="slider_image" id="slider_image" class="form-control">
					<option value="1" <?php if($slider=="1") echo 'selected="selected"'; ?>>Image 1</option>
					<option value="2" <?php if($slider=="2") echo 'selected="selected"'; ?>>Image 2</option>
					<option value="3" <?php if($slider=="3") echo 'selected="selected"'; ?>>Image 3</option>
				</select>
				
			</div>
			<div class="form-group  col-xs-6 col-sm-4">
				<input type="hidden" name="paymentid" value="{{ $payment_id }}">
                                <input type="hidden" name="transaction_id" value="{{ $transactionid }}">
				<label for="theme_id" class="control-label">Select Package</label>
				<select name="package_id" id="package_id" class="form-control">
					<option value="1" <?php if($package_id=="1") echo 'selected="selected"'; ?>>Monthly</option>
					<option value="2" <?php if($package_id=="2") echo 'selected="selected"'; ?>>Annual</option>
					<option value="3" <?php if($package_id=="3") echo 'selected="selected"'; ?>>Free</option>
				</select>
				@foreach ($packages as $package)
				<input type="hidden" name="monthly_fee" value="{{ $package->monthly_fee }}">
				<input type="hidden" name="annual_fee" value="{{ $package->annual_fee }}">
				@endforeach
			</div>
			</div>
			
			<div class="row">
			<div class="form-group  col-xs-6 col-sm-4">
				<label for="facebookurl" class="control-label">Facebook url</label>
				{!! Form::text('facebookurl', $facebookurl, [
            'class'     =>  'form-control',            
             'id'       => 'inputFacebookurl',
            'data-parsley-required-message' => 'Please enter facebook url.',
            'data-parsley-trigger' => 'change focusout',
			'data-parsley-pattern'          => '/^(https?:\/\/)?(www\.)?facebook.com\//',
            'data-parsley-pattern-message'  =>  'Please enter a valid facebook url.'
    ]) !!}
			</div>
			<div class="form-group  col-xs-6 col-sm-4">
				<label for="twitterurl" class="control-label">Twitter url</label>
				{!! Form::text('twitterurl', $twitterurl, [
            'class'     =>  'form-control',            
             'id'       => 'inputTwitterurl',
            'data-parsley-required-message' => 'Please enter twitter url.',
            'data-parsley-trigger' => 'change focusout',
			'data-parsley-pattern'          => '/^(https?:\/\/)?(www\.)?twitter.com\//',
            'data-parsley-pattern-message'  =>  'Please enter a valid twitter url.'
    ]) !!}
			</div>
			</div>
			<div class="row">
			<div class="form-group  col-xs-6 col-sm-4">
				<label for="facebookurl" class="control-label">Youtube url</label>
				{!! Form::text('youtubeurl', $youtubeurl, [
            'class'     =>  'form-control',            
             'id'       => 'inputYoutubeurl',
            'data-parsley-required-message' => 'Please enter youtube url.',
            'data-parsley-trigger' => 'change focusout',
			'data-parsley-pattern'          => '/^(https?:\/\/)?(www\.)?youtube.com\//',
            'data-parsley-pattern-message'  =>  'Please enter a valid youtube url.'
    ]) !!}
			</div>
			<div class="form-group  col-xs-6 col-sm-4">
				<label for="twitterurl" class="control-label">Linkedin url</label>
				{!! Form::text('linkedinurl', $linkedinurl, [
            'class'     =>  'form-control',            
             'id'       => 'inputLinkedinurl',
            'data-parsley-required-message' => 'Please enter linkedin url.',
            'data-parsley-trigger' => 'change focusout',
			'data-parsley-pattern'          => '/^(https?:\/\/)?(www\.)?linkedin.com\//',
            'data-parsley-pattern-message'  =>  'Please enter a valid linkedin url.'
    ]) !!}
			</div>
			</div>
			<div class="row">
			<div class="form-group  col-xs-6 col-sm-4">
				<label for="facebookurl" class="control-label">Googleplus url</label>
				{!! Form::text('googleurl', $googleurl, [
            'class'     =>  'form-control',            
             'id'       => 'inputGoogleurl',
            'data-parsley-required-message' => 'Please enter Google plus url.',
            'data-parsley-trigger' => 'change focusout',
			'data-parsley-pattern'          => '/^(https?:\/\/)?(www\.)?google.com\//',
            'data-parsley-pattern-message'  =>  'Please enter a valid google plus url.'
    ]) !!}
			</div>
			<div class="form-group  col-xs-6 col-sm-4">
				<label for="twitterurl" class="control-label">Instagram url</label>
				{!! Form::text('instaurl', $instaurl, [
            'class'     =>  'form-control',            
             'id'       => 'inputInstagramurl',
            'data-parsley-required-message' => 'Please enter twitter url.',
            'data-parsley-trigger' => 'change focusout',
			'data-parsley-pattern'          => '/^(https?:\/\/)?(www\.)?instagram.com\//',
            'data-parsley-pattern-message'  =>  'Please enter a valid instagram url.'
    ]) !!}
			</div>
			</div>
			
			<div class="row">
			<div class="form-group  col-xs-6 col-sm-4">
				<label for="logo" class="control-label">Logo</label>
				<input type="file" name="logo" id="logo" class="form-control" onchange="readURL(this);">
				<div class="alert alert-danger parsley" role="alert" id="im_error" style="display:none;">Please enter a valid image.</div>
    		@if($logo !='')
    			<div class="picture-upload"><img id="blah" src="{{ $logo }}"/></div>
    			<a href="{!! url('/admin/remove_content_logo', array('agentid' => $user->id)) !!}" id="remove_logo" class="remove-logo">Remove Logo</a>
     		@else
    			<div class="picture-upload"><img id="blah"/></div>
    		@endif
			</div>
			<div class="form-group  col-xs-6 col-sm-4">
				<label for="domain_status" class="control-label">Domain Status</label>
				<select name="domain_status" id="domain_status" class="form-control">
					<option value="1" <?php if($user->domain_status=="1") echo 'selected="selected"'; ?>>Active</option>
<option value="0" <?php if($user->domain_status=="0") echo 'selected="selected"'; ?>>Inactive</option>
				</select>
				
			</div>
			</div>
			{!! Form::checkHorizontal('confirmed', trans('back/users.confirmed'), $user->confirmed) !!}
			<div class="form-group" style="display:inline-block;">
			<a href="{!! url('/user') !!}" class="k-cancel">Cancel</a>
		</div>
			<div class="form-group" style="display:inline-block; margin-left:20px;">
				<input class="btn btn-primary" type="submit" value="Save">
			</div>
			@if(count($paypal_profile_status))
			<a href="{!! url('/admin/cancel-subscription', array('agentid' => $user->id)) !!}" class="test-emails" id="cancel_subscription" style="margin-left:20px;">Cancel Subscription</a>
			@endif
		{!! Form::close() !!}
	</div>

@stop
@section('scripts')
<script>
$(document).ready(function(){
	$('input[name=phone]').mask('(000) 000-0000');
	
});
$('#domain_name').keyup(function(){
	var value = $('#domain_name').val();
	$(".showDomainName").text(value);
	//$(".showDomainNameAnchor").attr('href',$(".showDomainNameAnchor").html());
})
$( document ).ready(function() {
    var value = $('#domain_name').val();
	$(".showDomainName").text(value);
	$(".showDomainNameAnchor").attr('href',$(".showDomainNameAnchor").text());
});
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="text-danger" role="alert"></div>'
        };
function readURL(input)
{
    if (input.files && input.files[0]) 
    {
         if(input.files[0].type == 'image/jpeg' || input.files[0].type == 'image/jpg' || input.files[0].type == 'image/png')
        {
             $('#im_error').hide();
            var reader = new FileReader();
            reader.onload = function (e) 
            {
                $('#blah')
                .attr('src', e.target.result);
                $( "#blah" ).addClass( "img-thumbnail" );
            };
            reader.readAsDataURL(input.files[0]);
        }
        else
        {
            $('#logo').val('');
            $('#im_error').show();
        }
    }
}
$(document).ready(function(){
    $("#remove_logo").click(function(e){
        if(!confirm('Are you sure to remove logo?')){
            e.preventDefault();
            return false;
        }
        return true;
    });
    $("#cancel_subscription").click(function(e){
        if(!confirm('Are you sure to cancel the subscription?')){
            e.preventDefault();
            return false;
        }
        return true;
    });
});    
</script>
{!! HTML::script('/js/parsley.min.js') !!}
@stop