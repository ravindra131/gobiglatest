@extends('back.template')
@section('head')
    {!! HTML::style('css/parsley.css') !!}
@stop
@section('main')

 <!-- Entête de page -->
  @include('back.partials.entete', ['title' => trans('back/users.agentadd'), 'icone' => 'user', 'fil' => link_to('user', trans('back/users.Users')) . ' / ' . trans('back/users.creation')])

	<div class="row"><div class="col-sm-12">
		{!! Form::open(['url' => 'user', 'method' => 'post', 'class' => 'panel', 'files' => true, 'data-parsley-validate']) !!}	
			<div class="row ">
			<div class=" col-xs-6 col-sm-4">{!! Form::control('text', 0, 'username', $errors, trans('back/users.name')) !!}</div>
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('email', 0, 'email', $errors, trans('back/users.email')) !!}
			</div>
			</div>
			<div class="row ">
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('password', 0, 'password', $errors, trans('back/users.password')) !!}</div>
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('password', 0, 'password_confirmation', $errors, trans('back/users.confirm-password')) !!}
			</div>
			</div>
			<div class="row ">
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('text', 0, 'domain_name', $errors, trans('back/users.domainname')) !!}
			<p class="help-block"> http://www.<span id="showDomainName"></span>.users.gobignewsletter.stagemyapp.com</p>
			</div>
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('text', 0, 'companyname', $errors, trans('back/users.company')) !!}</div>
			</div>
			<div class="row m-t-50">
			<div class="form-group col-xs-6 col-sm-4">
				<label for="theme_id" class="control-label">Select Image</label>
				<select name="slider_image" id="slider_image" class="form-control">
					<option value="1">Image 1</option>
					<option value="2">Image 2</option>
					<option value="3">Image 3</option>
				</select>
				
			</div>
			<div class="form-group col-xs-6 col-sm-4">
				<label for="theme_id" class="control-label">Select Package</label>
				<select name="package_id" id="package_id" class="form-control">
					<option value="1">Monthly</option>
					<option value="2">Annual</option>
					<option value="3">Free</option>
				</select>
				@foreach ($packages as $package)
				<input type="hidden" name="monthly_fee" value="{{ $package->monthly_fee }}">
				<input type="hidden" name="annual_fee" value="{{ $package->annual_fee }}">
				@endforeach
			</div>
			</div>
			<!--div class="row ">
			<div class=" col-xs-6 col-sm-4">{!! Form::control('text', 0, 'city', $errors, trans('back/users.city')) !!}</div>
			<div class=" col-xs-6 col-sm-4">{!! Form::control('text', 0, 'phone', $errors, trans('back/users.phone')) !!}</div>
			</div>
			<div class="row ">
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('text', 0, 'video_url', $errors, trans('back/users.videourl')) !!}</div>
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('text', 0, 'companyname', $errors, trans('back/users.company')) !!}</div>
			</div-->
			<div class="row ">
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('text', 0, 'facebookurl', $errors, trans('back/users.fb')) !!}
			</div>
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('text', 0, 'twitterurl', $errors, trans('back/users.tw')) !!}
			</div>
			</div>
			<div class="row ">
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('text', 0, 'youtubeurl', $errors, trans('back/users.youtube')) !!}
			</div>
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('text', 0, 'linkedinurl', $errors, trans('back/users.li')) !!}
			</div>
			</div>
			<div class="row ">
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('text', 0, 'googleurl', $errors, trans('back/users.googleplus')) !!}
			</div>
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('text', 0, 'instaurl', $errors, trans('back/users.insta')) !!}
			</div>
			</div>
			<!--div class="row ">
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('text', 0, 'monthprice', $errors, trans('back/users.monthprice')) !!}
			</div>
			<div class=" col-xs-6 col-sm-4">
			{!! Form::control('text', 0, 'annualprice', $errors, trans('back/users.annualprice')) !!}
			</div>
			</div-->
			<div class="row ">
			<div class="form-group col-xs-6 col-sm-4">
				<label for="logo" class="control-label">Logo</label>
				<input type="file" name="logo" id="logo" class="form-control" onchange="readURL(this);">
				<div class="alert alert-danger parsley" role="alert" id="im_error" style="display:none;">Please enter a valid image.</div>
    		<div class="picture-upload"><img id="blah"/></div>
			</div>
			<div class="form-group col-xs-6 col-sm-4">
				<label for="domain_status" class="control-label">Domain Status</label>
				<select name="domain_status" id="domain_status" class="form-control">
					<option value="1">Active</option>
                                        <option value="0">Inactive</option>
				</select>
				
			</div>
			</div>
			<div class="form-group" style="display:inline-block;">
			<a href="{!! url('/user') !!}">Cancel</a>
		</div>
			<div class="form-group" style="display:inline-block; margin-left:10px;">
				<input class="btn btn-primary" type="submit" value="Add">
			</div>
		{!! Form::close() !!}
	</div></div>

@stop
@section('scripts')
<script>
$('#domain_name').keyup(function(){
	var value = $('#domain_name').val();
	$("#showDomainName").text(value);
});

$(document).ready(function(){
	$('input[name=phone]').mask('(000) 000-0000');
	
});
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="text-danger" role="alert"></div>'
        };
function readURL(input)
{
    if (input.files && input.files[0]) 
    {
        if(input.files[0].type == 'image/jpeg' || input.files[0].type == 'image/jpg' || input.files[0].type == 'image/png')
        {
             $('#im_error').hide();
            var reader = new FileReader();
            reader.onload = function (e) 
            {
                $('#blah')
                .attr('src', e.target.result);
                $( "#blah" ).addClass( "img-thumbnail" );
            };
            reader.readAsDataURL(input.files[0]);
        }
        else
        {
            $('#logo').val('');
            $('#im_error').show();
        }
    }
}
</script>
{!! HTML::script('/js/parsley.min.js') !!}
@stop