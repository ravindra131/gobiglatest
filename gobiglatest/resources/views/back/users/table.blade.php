	<table class="table">
			<thead>
				<tr>
					<th>Full Name</th>
					<!-- <th>{{ trans('back/users.role') }}</th> -->
          			<th>Website URL</th>
          			<th>Status</th>
					<th>{{ trans('back/users.seen') }}</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			 @foreach ($users as $user)
		@if($user->role_id == '3' && $user->id != '2' && $user->id != '3' && $user->id != '4')
		<tr {!! !$user->seen? 'class="warning"' : '' !!}>
			<td class="text-primary"><strong><a href="{!! route('user.show', [$user->id]) !!}">{{ ucfirst($user->username) }}</a></strong></td>
			<!-- <td>{{ $user->role->title }}</td> -->
			@if($user->domain_status == 1 && $user->confirmed == 1)
			<td>{{ $user->domain_name }}.users.gobignewsletter.stagemyapp.com</td>
			<td>Active</td>
			@elseif(($user->domain_status == 0 && $user->confirmed == 1) || ($user->domain_status == 1 && $user->confirmed == 0) || ($user->domain_status == 0 && $user->confirmed == 0))
			<td>No data</td>
			<td>Inactive</td>
			@endif
			<td>{!! Form::checkbox('seen', $user->id, $user->seen) !!}</td>
			<td style="text-align:right;">{!! link_to_route('user.show', trans('back/users.see'), [$user->id], ['class' => 'btn btn-success  btn fa fa-eye']) !!}
			   {!! link_to_route('user.edit', trans('back/users.edit'), [$user->id], ['class' => 'btn btn-warning fa fa-pencil ']) !!}
             </td>
              <td>
				{!! Form::open(['method' => 'DELETE', 'route' => ['user.destroy', $user->id]]) !!}
				<!-- {!! Form::destroy(trans('back/users.destroy'), trans('back/users.destroy-warning')) !!} -->
				{!! Form::close() !!}
			</td>
			<!-- <td><a href="{!! url('/admin/cancel-subscription', array('agentid' => $user->id)) !!}" id="cancel_subscription">Cancel Subscription</a></td> -->
		</tr>
		@endif
	@endforeach
      </tbody>
		</table>