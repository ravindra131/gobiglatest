@extends('back.template')

@section('head')

<style type="text/css">
  
  .badge {
    padding: 1px 8px 1px;
    background-color: #aaa !important;
  }

</style>

@stop

@section('main')
<a href="{!! route('user.create') !!}" class="btn btn-info pull-right">Add New Member</a>
{!! Form::open(['class' => ' form-horizontal', 'method' => 'post']) !!}
{!! csrf_field() !!}
<div class="form-group">
<div class="col-md-3 col-sm-5">
  {!! Form::text('agentname', null, [
            'class'                         => 'form-control',
            'id'                            => 'agentname',
            'placeholder'                   => 'Member name'
        ]) !!}
</div>

<div class="col-md-3 col-sm-5">
<a class="btn btn-primary" href="#" id="search_users">Search</a>
</div></div>
<h1 class="page-header row no-margin"><span class="orders">All Members</span>
<div class="col-md-2 col-sm-5 pull-right">
<label class="fs-15">Records per page</label>
<select name="records" id="records" class="form-control" onchange="doAjaxSearchUser()">
  <option value="10">10</option>
  <option value="20">20</option>
  <option value="30">30</option>
</select>
</div>
</h1>
{!! Form::close() !!}
@if(session()->has('ok'))
    @include('partials/error', ['type' => 'success', 'message' => session('ok')])
  @endif
  @if (Session::has('message'))
    <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
<p class="text-right pcsv"><a href="#" onclick="save_users()" class="csv-btn"><span> Export as <img src="{{ asset('images/csv-icon.png') }}"></span></a></p>
<div id="recontainer">
	<div class="panel" >
		  @include('back.users.table')
	</div>

	<div class="pull-right link">{!! $links !!}</div>
</div>
@stop

@section('scripts')

  <script>
    
    $(function() {

      // Seen gestion
      $(document).on('change', ':checkbox', function() {    
        $(this).parents('tr').toggleClass('warning');
        $(this).hide().parent().append('<i class="fa fa-refresh fa-spin"></i>');
        var token = $('input[name="_token"]').val();
        $.ajax({
          url: '{!! url('userseen') !!}' + '/' + this.value,
          type: 'PUT',
          data: "seen=" + this.checked + "&_token=" + token
        })
        .done(function() {
          $('.fa-spin').remove();
          $('input[type="checkbox"]:hidden').show();
        })
        .fail(function() {
          $('.fa-spin').remove();
          var chk = $('input[type="checkbox"]:hidden');
          chk.show().prop('checked', chk.is(':checked') ? null:'checked').parents('tr').toggleClass('warning');
          alert('{{ trans('back/users.fail') }}');
        });
      });

    });
    $('#search_users').on('click',function()
  {
        doAjaxSearchUser();
         
    });
    function doAjaxSearchUser()
  {
    var agentname = $('#agentname').val();
    var records = $('#records').val();
      var dataString  = {SearchText:agentname,record:records};
      $.ajax({                       
         url :  "/admin/search/users",
         type: "GET",
         dataType: 'JSON',
         data : dataString, 
         cache:false,
         success : function(data){
           //console.log(data);
           // alert(data.count);
           if(data.count > 0)
           {
              $('#recontainer').html(data.html);
           }
           else
           {
            $('#recontainer').html('No records found');
           }
         
         }               
       });
  }
  function save_users()
  {
    doAjaxSearchUser();
    var agentname = $('#agentname').val();
    var dataString  = {SearchText:agentname};
    window.location.href = '{!! url('/admin/download-users/') !!}' + "?" + serialize(dataString);
  }

 function serialize(obj) {
  var str = [];
  for(var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}
  </script>

@stop