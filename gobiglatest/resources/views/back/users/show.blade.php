@extends('back.template')

@section('main')
	@include('back.partials.entete', ['title' => trans('back/users.agentdetail'). link_to_route('user.edit', trans('back/users.editagent'), [$user->id], ['class' => 'btn btn-warning pull-right']), 'icone' => 'user', 'fil' => link_to('user', trans('back/users.Users')) . ' / ' . trans('back/users.card')])
    
	    <div class="col-sm-6 no-margin-imp">
		
		<h4>Basic details</h4>
	    <p><span class="see-b">Full Name :</span> {{ ucfirst($user->username) }}</p>
	<p><span class="see-b">Email :</span> {{ $user->email }}</p>
	<!-- <p><span class="see-b">Role :</span> {{ $user->role->title }}</p> -->
	@if($user->domain_name != '')
	<p><span class="see-b">Website Domain :</span> {{ $user->domain_name }}</p>
	@endif
	<!-- <p><span class="see-b">Theme :</span> {{ 'Theme' .  $user->theme_id }}</p> -->
	@if($conresults != '')
	<!--p><span class="see-b">City :</span> {{ $conresults[0]->city }}</p>
	<p><span class="see-b">Phone :</span> {{ $conresults[0]->phone }}</p-->
	<!-- <p>{{ trans('back/users.videourl') . ' : ' .  $conresults[0]->video_url }}</p> -->
	<p><span class="see-b">Title :</span> {{ $conresults[0]->companyname }}</p>
	@endif
	
	
	<span class="see-b">{{ $user->confirmed ? trans('back/users.confirmed') : trans('back/users.not-confirmed') }}</span><br>
	</div>
	<div class="col-sm-6">

		@if($conresults[0]->facebookurl || $conresults[0]->twitterurl || $conresults[0]->googleurl || $conresults[0]->youtubeurl || $conresults[0]->linkedinurl || $conresults[0]->instaurl)	
	<h4>Social Link Details </h4>
@endif
	@if($conresults[0]->facebookurl)
	<p><span class="see-b">Facebook Url :</span> {{ $conresults[0]->facebookurl }}</p>
	@endif
	@if($conresults[0]->twitterurl)
	<p><span class="see-b">Twitter Url :</span> {{ $conresults[0]->twitterurl }}</p>
	@endif
		@if($conresults[0]->googleurl)
	<p><span class="see-b">Google Plus Url :</span> {{ $conresults[0]->googleurl }}</p>
@endif
	@if($conresults[0]->youtubeurl)
	<p><span class="see-b">Youtube Url :</span> {{ $conresults[0]->youtubeurl }}</p>
@endif
	@if($conresults[0]->linkedinurl)
		
	<p><span class="see-b">Linkedin Url :</span> {{ $conresults[0]->linkedinurl }}</p>
	@endif
		@if($conresults[0]->instaurl)
	<p><span class="see-b">Instagram Url :</span> {{ $conresults[0]->instaurl }}</p>
	@endif
	
	
	
	</div>
@stop