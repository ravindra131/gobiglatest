@if(!empty($queries))

				<table class="table">
					<thead>
						<tr>
							<th class="col-lg-1">Name</th>
							<th class="col-lg-1">Email</th>
							<th class="col-lg-1" width="55%">Message</th>
							<th class="col-lg-1">Action</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($queries as $query)
						<tr>
							<td class="text-primary">{{ ucfirst($query->name) }}</td>
							<td>{{ $query->email }}</td>
							<td width="55%">{{ ucfirst($query->message) }}</td>
							<td><a href="{!! url('/reply-queries', array('personid' => $query->id)) !!}" class="k-reply">Reply</a></td>
						</tr>
					@endforeach
					</tbody>
				</table>
				<div class="pull-right link">
				<?php echo $queries->render(); ?>
			</div>
				@else
                <p style="text-align: center;">There is no data available.</p> 
				@endif