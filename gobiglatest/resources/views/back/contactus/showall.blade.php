@extends('back.template')
@section('head')
	<style type="text/css">
		.table { margin-bottom: 0; }
		.panel-heading { padding: 0 15px; }
	</style>
@stop
@section('main')
{!! Form::open(['class' => ' form-horizontal', 'method' => 'post']) !!}
{!! csrf_field() !!}
<div class="form-group search-form-k">
<div class="col-md-3 col-sm-5">
	{!! Form::text('name', null, [
            'class'                         => 'form-control',
            'id'                            => 'name',
            'placeholder'                   => 'Name'
        ]) !!}
</div>
<div class="col-md-3 col-sm-5">
{!! Form::text('email', null, [
            'class'                         => 'form-control',
            'id'                            => 'email',
            'placeholder'                   => 'Email address'
        ]) !!}
</div>
<div class="col-md-3 col-sm-5">
<a class="btn btn-primary" href="#" id="search_query">Search</a>
</div></div>
{!! Form::close() !!}
	<h1 class="page-header">Contact Us Queries</h1>
	@if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
		<div class="panel" id="Resultquery">
				@include('back.contactus.part')
			</div>
		</div>
@stop
@section('scripts')
<script>
 	$('#search_query').on('click',function()
 	{
        doAjaxSearchQueries();
				 
    });
    $('#name').keydown(function (e){
    if(e.keyCode == 13){
        doAjaxSearchQueries();
    }
});
    $('#email').keydown(function (e){
    if(e.keyCode == 13){
        doAjaxSearchQueries();
    }
});

    function doAjaxSearchQueries()
	{
		var name = $('#name').val();
		var email = $('#email').val();
		var dataString  = {names:name,email_address:email};
			$.ajax({                       
			   url :  "/admin/search/contactquery",
			   type: "GET",
			   dataType: 'JSON',
			   data : dataString, 
			   cache:false,
			   success : function(data){
				   //console.log(data);
				   //alert(data.count);
				   if(data.count > 0)
				   {
				   	 $('#Resultquery').html(data.html);
				   }
				   else
				   {
				   		$('#Resultquery').html('No records found');
				   }
				 
			   }               
		   });
	}
</script>
@stop


