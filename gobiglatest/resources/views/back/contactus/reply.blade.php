@extends('back.template')
@section('main')
	<h1 class="page-header">Reply Contact Query</h1>
	<div class="row">
	<div class="col-sm-12">
		{!! Form::open(['url' => '/sendreply', 'method' => 'post', 'class' => 'form-horizontal panel', 'id' => 'rform']) !!}
    <input type="hidden" name="pid" value="{{ $id }}">
		<label>Reply Message</label>
		{!! Form::textarea('rmessage', null, [
            'class'                         => 'form-control',
            'id'                            => 'rmessage'
        ]) !!}
        <div style="display:none;" id="message_error" class="text-danger"></div>
        <a href="{!! url('/show-queries') !!}" class="k-cancel">Cancel</a>		
		<input type="button" name="send" value="Send" class="k-submit" id="reply"> 
        {!! Form::close() !!}
	</div>
	</div>
@stop
@section('scripts')
<script type="text/javascript">
  $('#reply').click(function(){
    var message = $('#rmessage').val();
    if(message == '')
    {
        $('#message_error').html('Please enter message.').show();
        return false;
    }
    else
    {
        $('#message_error').html('').hide();
        $("#reply").val('Processing....');
        $('#reply').prop("disabled",true);
        $('#rform').submit();
    }
});
  </script>
@stop
