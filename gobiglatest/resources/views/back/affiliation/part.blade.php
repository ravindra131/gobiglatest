<div class="panel-heading2">
				@if(!empty($results))

				<table class="table">
					<thead>
						<tr>
							<th class="col-lg-1">Member Name</th>
						
							<th class="col-lg-1">Total Commission</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($results as $result)
					
						<tr>
							@if($result->agentname != '')
							<td class="text-primary"><strong><a href="{!! route('user.show', [$result->agent_id]) !!}">{{ ucfirst($result->agentname) }}</a></strong></td>
						
							<td>{{ $result->totalamount }}</td>
							@endif
						</tr>
					@endforeach
					</tbody>
				</table>
				<div class="pull-right link">
				<?php echo $results->render(); ?>
			</div>
				@else
                <p style="text-align: center;">There is no data available.</p> 
				@endif	
			</div>