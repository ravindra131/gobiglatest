@extends('back.template')
@section('head')
	<style type="text/css">
		.table { margin-bottom: 0; }
		.panel-heading { padding: 0 15px; }
	</style>
@stop
@section('main')
	<h1  class="page-header">Set Commission</h1>
	@if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
 	@endif
		<div class="panel">
			<div class="panel-heading1">
				@if($fees != '')
				<table class="table">
					<thead>
						<tr>
							<th class="col-lg-1">Type</th>
							<th class="col-lg-1">Monthly Commission</th>
							<th class="col-lg-1">Annual Commission</th>
							<th class="col-lg-1">Action</th>
						</tr>
					</thead>
					@foreach ($fees as $fee)
					<tbody>
						<tr>
							<td>{{ ucfirst($fee->type) }}</td>
							<td>{{ $fee->monthly_bonus }}</td>
							<td>{{ $fee->annual_bonus }}</td>
							<td><a href="{!! url('/editreferalfee', array('fee_id' => $fee->id)) !!}" class="btn btn-warning fa fa-pencil">Edit</a>&nbsp;
							<!--<a href="{!! url('/deletereferalfee', array('fees_id' => $fee->id)) !!}" class="btn btn-danger fa fa-trash" id="feedelete">Delete</a--></td>
						</tr>
					</tbody>
					@endforeach
				</table>	
				@else
				<p style="text-align: center;">There is no data available.<a href="{!! url('/showaddfee') !!}">Click here to set commission.</a></p>
				@endif
			</div>
		</div>
@stop
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
    $("#feedelete").click(function(e){
        if(!confirm('Are you sure to delete this?')){
            e.preventDefault();
            return false;
        }
        return true;
    });
});
</script>

@stop

