@extends('back.template')
@section('head')
	<style type="text/css">
		.table { margin-bottom: 0; }
		.panel-heading { padding: 0 15px; }
	</style>
<meta name="csrf-token" content="{{ csrf_token() }}" />
@stop
@section('main')
	{!! Form::open(['class' => ' form-horizontal', 'method' => 'post']) !!}
{!! csrf_field() !!}
<div class="form-group search-form-k">
<div class="col-md-3 col-sm-5">
	{!! Form::text('agentname', null, [
            'class'                         => 'form-control',
            'id'                            => 'agent_name',
            'placeholder'                   => 'Member name'
        ]) !!}
</div>
<div class="col-md-3 col-sm-5">
<a class="btn btn-primary" id="search_affiliation">Search</a>
</div>
</div>
<h1 class="page-header row no-margin"><span class="orders">Affiliation</span>
<div class="col-md-2 col-sm-5 pull-right">
<label class="fs-15">Records per page</label>
<select name="records" id="records" class="form-control" onchange="doAjaxSearchAffiliation()">
	<option value="10">10</option>
	<option value="20">20</option>
	<option value="30">30</option>
</select>
</div></h1>
{!! Form::close() !!}
<p class="text-right pcsv"><a href="#" onclick="save_affiliates()" class="csv-btn"><span> Export as <img src="{{ asset('images/csv-icon.png') }}"></span></a></p>
	<div id="statuschange" class="alert alert-success" style="display:none;">Payment status updated successfully.</div>
	<div id="alreadypaid" class="alert alert-danger" style="display:none;">Already Paid.</div>
		<div class="panel" id="resultContainer">
			@include('back.affiliation.part')
		</div>
@stop
@section('scripts')
<script>
function changestatus(id)
{
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
// Returns successful data submission message when the entered information is stored in database.
$.post("/change-status", {
id: id,
amount:100
}, function(data) {
	//console.log(data[0].total_earning);
	if(data != 0)
	{
		$('#alreadypaid').hide();
		$('#statuschange').show();
		$('#tearn'+id).html(data[0].total_earning);
		$('#tdue'+id).html(data[0].payment_due);
		$('#status'+id).html('Paid');
	}
	else
	{
		$('#statuschange').hide();
		$('#alreadypaid').show();
	}
});
}

$('#search_affiliation').on('click',function()
 	{
        doAjaxSearchAffiliation();
				 
    });
$('#agent_name').keydown(function (e){
    if(e.keyCode == 13){
        doAjaxSearchAffiliation();
    }
});
    function doAjaxSearchAffiliation()
	{
		var record = $('#records').val();
		var agentname = $('#agent_name').val();
			var dataString  = {SearchText:agentname,records_no:record};
			$.ajax({                       
			   url :  "/admin/search/affiliation",
			   type: "GET",
			   dataType: 'JSON',
			   data : dataString, 
			   cache:false,
			   success : function(data){
				   //console.log(data);
				   //alert(data.count);
				   if(data.count > 0)
				   {
				   		$('#resultContainer').html(data.html);
				   }
				   else
				   {
				   	 $('#resultContainer').html('No records found');
				   }
				 
			   }               
		   });
	}
	function save_affiliates()
	{
		doAjaxSearchAffiliation();
		var agentname = $('#agent_name').val();
		var dataString = {SearchText:agentname};
		window.location.href = '{!! url('/admin/download-affiliates/') !!}' + "?" + serialize(dataString);
	}

 function serialize(obj) {
  var str = [];
  for(var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}
</script>
@stop
