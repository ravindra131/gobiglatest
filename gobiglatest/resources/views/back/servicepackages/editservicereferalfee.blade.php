@extends('back.template')
@section('head')
	<style type="text/css">
		.table { margin-bottom: 0; }
		.panel-heading { padding: 0 15px; }
	</style>
@stop
@section('main')
	<h3 class="page-header">Set Referal Fee</h3>
	<div class="row">
	<div class="col-sm-12">
		@if (Session::has('message'))
    		<div class="alert alert-success">{{ Session::get('message') }}</div>
		@endif
		{!! Form::open(['url' => '/edit-service-fees', 'method' => 'post', 'class' => 'form-horizontal panel']) !!}
		<input type="hidden" name="feeid" value="{{ $fees[0]->id }}">
		<label>Type</label>
		<select name="type" class="form-control">
			<option name="fixed" value="Fixed" <?php if($fees[0]->type=="Fixed") echo 'selected="selected"'; ?>>Fixed</option>
		</select>
		@if ($errors->has('type'))
            <div class="alert alert-danger" role="alert">{{ $errors->first('type') }}</div>
        @endif
		<br>
		<label>Monthly Bonus</label>	
		<input type="text" name="monthly_bonus" class="form-control" value="{{ $fees[0]->monthly_bonus }}">
		@if ($errors->has('monthly_bonus'))
            <div class="alert alert-danger" role="alert">{{ $errors->first('monthly_bonus') }}</div>
        @endif
        <br>
		<label>Annual Bonus</label>	
		<input type="text" name="annual_bonus" class="form-control" value="{{ $fees[0]->annual_bonus }}">
		@if ($errors->has('annual_bonus'))
            <div class="alert alert-danger" role="alert">{{ $errors->first('annual_bonus') }}</div>
        @endif
		<a href="{!! url('/service-referal-fee') !!}" class="k-cancel">Cancel</a>
		<input type="submit" name="save" value="Save" class="k-submit">	
		
		{!! Form::close() !!}
	</div>
	</div>
@stop