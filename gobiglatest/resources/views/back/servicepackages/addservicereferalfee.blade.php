@extends('back.template')
@section('head')
	<style type="text/css">
		.table { margin-bottom: 0; }
		.panel-heading { padding: 0 15px; }
	</style>
@stop
@section('main')
	<h1 class="page-header">Set Service Referal Fee</h1>
	<div class="col-sm-12">
		@if (Session::has('message'))
    		<div class="alert alert-success">{{ Session::get('message') }}</div>
		@endif
		{!! Form::open(['url' => '/addservicefees', 'method' => 'post', 'class' => 'form-horizontal panel']) !!}
		<label>Type</label>
		<select name="type" class="form-control">
			<option name="fixed" value="Fixed">Fixed</option>
		</select>
		@if ($errors->has('type'))
            <div class="alert alert-danger" role="alert">{{ $errors->first('type') }}</div>
        @endif
		<br>
		<label>Monthly Bonus</label>	
		<input type="text" name="monthly_bonus" class="form-control">
		@if ($errors->has('monthly_bonus'))
            <div class="alert alert-danger" role="alert">{{ $errors->first('monthly_bonus') }}</div>
        @endif
        <br>
		<label>Annual Bonus</label>	
		<input type="text" name="annual_bonus" class="form-control">
		@if ($errors->has('annual_bonus'))
            <div class="alert alert-danger" role="alert">{{ $errors->first('annual_bonus') }}</div>
        @endif
        <a href="{!! url('/service-referal-fee') !!}" class="k-cancel">Cancel</a>
		<input type="submit" name="add" value="Add" class="k-submit">	
		{!! Form::close() !!}
	</div>
@stop