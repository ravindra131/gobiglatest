@extends('back.template')
@section('head')
    {!! HTML::style('css/parsley.css') !!}
@stop
@section('main')

	<!-- Ent�te de page -->
	<h3 class="page-header">Edit Package Fee</h3>
    @if ($errors->has())
    <div class="alert alert-danger">
          @foreach ($errors->all() as $error)
            {{ $error }}<br>
          @endforeach
        </div>
    @endif
	<div class="row">
	<div class="col-sm-12">

		{!! Form::open(['url' => '/editservicepackage', 'method' => 'post', 'role' => 'form', 'data-parsley-validate']) !!}	
		<input type="hidden" name="id" value="{{ $servicepackage->id}}" class="form-control">
		<label>Monthly Fee (in $)</label>
		{!! Form::text('monthly_fee', $servicepackage->monthly_fee, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'monthly_fee',
            'data-parsley-required-message' => 'Please enter monthly fee.',
            'data-parsley-trigger'          => 'submit',
            'data-parsley-pattern'          => '/^[0-9]*$/',
            'data-parsley-pattern-message'  =>  'Please enter valid monthly fee.'
        ]) !!}<br>
		<label>Annual Fee (in $)</label>
		{!! Form::text('annual_fee', $servicepackage->annual_fee, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'annual_fee',
            'data-parsley-required-message' => 'Please enter annual fee.',
            'data-parsley-trigger'          => 'submit',
            'data-parsley-pattern'          => '/^[0-9]*$/',
            'data-parsley-pattern-message'  =>  'Please enter valid annual fee.'
        ]) !!}<br>
        <label>Discount (in %)</label>
        {!! Form::text('discount', round(((($servicepackage->monthly_fee*12)-($servicepackage->annual_fee))*100)/($servicepackage->monthly_fee*12)), [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'discount',
            'data-parsley-required-message' => 'Please enter discount.',
            'data-parsley-trigger'          => 'submit',
            'data-parsley-pattern'          => '/^[0-9\-]*$/',
            'data-parsley-pattern-message'  =>  'Please enter valid discount.'
        ]) !!}<br>
		<a href="{!! url('/package') !!}" class="k-cancel">Cancel</a>
		<input type="submit" name="save" value="Save" class="k-submit">
		{!! Form::close() !!}
	</div>
	</div>

@stop
@section('scripts')
<script>
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger" role="alert" style="width:270px;"></div>'
        };
$('#monthly_fee').keyup(function(){
   update_discountfield();
});
$('#annual_fee').keyup(function(){
   update_discountfield();
});
function update_discountfield()
{
    var monthly_fee_value = $('#monthly_fee').val();
    var annual_fee_value = $('#annual_fee').val();
    if(monthly_fee_value != '' && annual_fee_value != '')
    {
        var discount = Math.round((((monthly_fee_value*12)-(annual_fee_value))*100)/(monthly_fee_value*12));
        $('#discount').val(discount);
    }
    else
    {
       $('#discount').val(''); 
    }
}
$('#discount').keyup(function(){
   update_annualfeefield();
});
function update_annualfeefield()
{
    var monthlyfee = $('#monthly_fee').val();
    var discount = $('#discount').val();
    if(discount != '' && discount != null)
    {
        var annualfee = Math.round((monthlyfee*12)-(discount*monthlyfee*12)/100);
        $('#annual_fee').val(annualfee);
    }
    else
    {
        $('#annual_fee').val('');
    }
}
</script>
{!! HTML::script('/js/parsley.min.js') !!}
@stop
