@extends('back.template')
@section('head')
    {!! HTML::style('css/parsley.css') !!}
@stop
@section('main')
<h1 class="k-heading">Profile</h1>
@if ($errors->has())
    		<div class="alert alert-danger">
          		@foreach ($errors->all() as $error)
            		{{ $error }}<br>
          		@endforeach
    		</div>
    	@endif
		@if (Session::has('message'))
    		<div class="alert alert-success">{{ Session::get('message') }}</div>
		@endif
	<div class="col-sm-12">
		{!! Form::open(['url' => '/admin/editprofile', 'method' => 'post', 'class' => 'form-horizontal panel', 'data-parsley-validate']) !!}	
		<input type="hidden" name="userid" value="{{ $profile_data[0]['id'] }}">
		<div class="form-group">
				<label for="name" class="control-label">Full Name</label>
				{!! Form::text('fullname', $profile_data[0]['username'], [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'fullname',
            'data-parsley-required-message' => 'Please enter full name.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-pattern'          => '/^[a-zA-Z\s]*$/',
            'data-parsley-pattern-message'  =>  'Please enter valid name.'
        	]) !!}
		</div>
		<div class="form-group">
				<label for="email" class="control-label">Email address</label>
				{!! Form::email('email', $profile_data[0]['email'], [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'email',
            'data-parsley-required-message' => 'Please enter e-mail address.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-type'             => 'email',
            'data-parsley-type-message'     => 'Please enter a valid e-mail address.'
        	]) !!}
		</div>
		<div class="form-group" style="display:inline-block;">
			<a href="{!! url('/admin') !!}" class="k-cancel">Cancel</a>
		</div>
			<div class="form-group" style="display:inline-block; margin-left:20px;">
				<input class="btn btn-primary" type="submit" value="Save">
			</div>
		{!! Form::close() !!}
	</div>
@stop
@section('scripts')
<script>
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="text-danger" role="alert"></div>'
        };
</script>
{!! HTML::script('/js/parsley.min.js') !!}
@stop