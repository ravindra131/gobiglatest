@extends('back.template')
@section('head')
    {!! HTML::style('css/parsley.css') !!}
@stop
@section('main')
<h1 class="k-heading">Change Password</h1>
@if ($errors->has())
    		<div class="alert alert-danger">
          		@foreach ($errors->all() as $error)
            		{{ $error }}<br>
          		@endforeach
    		</div>
    	@endif
		@if (Session::has('message'))
    		<div class="alert alert-success">{{ Session::get('message') }}</div>
		@endif
	<div class="col-sm-12">
		{!! Form::open(['url' => '/admin/editpassword', 'method' => 'post', 'class' => 'form-horizontal panel', 'data-parsley-validate']) !!}	
		<div class="form-group">
				<label for="password" class="control-label">New Password</label>
				{!! Form::password('password', [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'password',
            'data-parsley-required-message' => 'Please enter password.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-minlength'        => '6',
            'data-parsley-minlength-message'=> 'Please enter password of minimum length 6.'
        ]) !!}
		</div>
		<div class="form-group">
				<label for="cpassword" class="control-label">Confirm Password</label>
				{!! Form::password('cpassword', [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'cpassword',
            'data-parsley-required-message' => 'Please enter confirm password.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-equalto'          => '#password',
            'data-parsley-equalto-message'  => 'Password and Confirm password not match.'
        ]) !!}
		</div>
		<div class="form-group" style="display:inline-block;">
			<a href="{!! url('/admin') !!}" class="k-cancel">Cancel</a>
		</div>
			<div class="form-group" style="display:inline-block; margin-left:20px;">
				<input class="btn btn-primary" type="submit" value="Save">
			</div>
		{!! Form::close() !!}
	</div>
@stop
@section('scripts')
<script>
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="text-danger" role="alert"></div>'
        };
</script>
{!! HTML::script('/js/parsley.min.js') !!}
@stop