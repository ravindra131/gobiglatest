@extends('back.template')
@section('head')
	<style type="text/css">
		.table { margin-bottom: 0; }
	</style>
@stop
@section('main')
  	<div class="panel">
		<div class="panel-heading">
			<h1 class="k-heading">Upload Pdf</h1>
		  	<div id="validation-errors" class="alert alert-danger" style="display:none;padding:3px 5px 0px 5px;"></div>
		  	<div id="sucess" class="alert alert-success" style="display:none">File uploaded successfully</div>
				<form class="form-horizontal" id="upload" enctype="multipart/form-data" method="post" action="{{ url('/upload') }}" autocomplete="off" style="padding-top:10px;">
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					<input type="hidden" name="filename" value="{{ $filename }}">
					<input type="file" name="image" id="image"/> 
					<a href="/uploadpdf" class="btn btn-links k-cancel" style="margin-top:12px;">Cancel</a>
					<button id="up" class="btn btn-primary" style="margin-top:12px;">Upload</button>

				</form>
				<div id="output" style="display:none">
				</div>
				@if($filename != '')
				<div id="fistfile">
				<h4 style="padding:12px 0px 0px;">Your Uploaded File</h4>
				<iframe src="{{ $filename }}" type='application/pdf' style='position:relative;width:100%;height:700px; border:1px solid #827f7f;'></iframe>
			</div>
				@endif
		</div>
	</div>
@stop
@section('scripts')
{!! HTML::script('js/jquery.form.js'); !!}
<script>
$(document).ready(function() {
	var options = { 
        beforeSubmit:  showRequest,
		success:       showResponse,
		dataType: 'json' 
        }; 
 	$('body').delegate('#up','click', function(e){
 		e.preventDefault();
 		$('#upload').ajaxForm(options).submit(); 		
 	}); 
});		
function showRequest(formData, jqForm, options) 
{ 
	$("#validation-errors").hide().empty();
	$("#output").css('display','none');
	$("#sucess").css('display','none');
    return true; 
} 
function showResponse(response, statusText, xhr, $form)  
{ 
	if(response.success == false)
	{
		var arr = response.errors;
		$.each(arr, function(index, value)
		{
			if (value.length != 0)
			{
				if(value == 'The file must be a file of type: pdf.')
				{
					$("#validation-errors").append('<div class="alert alert-error"><strong>The file should be of .pdf format.</strong><div>');
				}
				else
				{
					$("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
				}
			}
		});
		$("#validation-errors").show();
	}
	else 
	{
	 	$('#fistfile').hide();
		$("#output").html("<iframe src='"+response.file+"' type='application/pdf' style='position:relative;width:100%;height:700px;'></iframe>");
		$("#output").css('display','block');
		$("#sucess").css('display','block');
	}
}
</script>
@stop

