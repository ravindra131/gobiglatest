<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Admin</title>
		<meta name="description" content="">	
		<meta name="viewport" content="width=device-width, initial-scale=1">

		{!! HTML::style('css/main_back.css') !!}

		<!--[if (lt IE 9) & (!IEMobile)]>
			{!! HTML::script('js/vendor/respond.min.js') !!}
		<![endif]-->
		<!--[if lt IE 9]>
			{{ HTML::style('https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js') }}
			{{ HTML::style('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') }}
		<![endif]-->

		{!! HTML::style('http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800') !!}
		{!! HTML::style('http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic') !!}

        @yield('head')

	</head>

  <body>

	<!--[if lte IE 7]>
	    <p class="browsehappy">Vous utilisez un navigateur <strong>obsol�te</strong>. S'il vous pla�t <a href="http://browsehappy.com/">Mettez le � jour</a> pour am�liorer votre navigation.</p>
	<![endif]-->

   <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                @if(session('statut') == 'admin')
                    {!! link_to_route('admin', trans('back/admin.administration'), [], ['class' => 'navbar-brand']) !!}
                @else
                    {!! link_to_route('blog.index', trans('back/admin.redaction'), [], ['class' => 'navbar-brand']) !!}
                @endif
            </div>
            <!-- Menu sup�rieur -->
            <ul class="nav navbar-right top-nav">
                <li>{!! link_to_route('home', trans('back/admin.home')) !!}</li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-user"></span> {{ ucfirst(auth()->user()->username) }}<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{!! url('/admin/profile') !!}"><span class="fa fa-user"></span>Profile</a>
                        </li>
                        <li>
                            <a href="{!! url('/admin/change-password') !!}"><span class="fa fa-key"></span>Change Password</a>
                        </li>
                        <li>
                            <a href="{!! url('auth/logout') !!}"><span class="fa fa-fw fa-power-off"></span> {{ trans('back/admin.logout') }}</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Menu de la barre lat�rale -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    @if(session('statut') == 'admin')
                        <li {!! classActivePath('admin') !!}>
                             <a href="{!! route('admin') !!}"><span class="fa fa-fw fa-dashboard"></span> {{ trans('back/admin.dashboard') }}</a>
                        </li>
                        <li {!! classActiveSegment(1, 'user') !!}>
                            <a href="#" data-toggle="collapse" data-target="#usermenu"><span class="fa fa-fw fa-user"></span>Member Management<span class="fa fa-fw fa-caret-down caret-k"></span></a>
                            <ul id="usermenu" class="collapse">
                                <li><a href="{!! url('user') !!}">All Members</a></li>
                                <li><a href="{!! url('user/create') !!}">{{ trans('back/admin.add') }}</a></li>
                            </ul>
                        </li> 
<!--li {!! classActivePath('package') !!}>
                            <a href="{!! url('package') !!}"><span class="fa fa-fw fa fa-dropbox"></span>{{ trans('back/admin.packages') }}</a>
                        </li--> 
						<li {!! classActivePath('servicepackage') !!}>
                            <a href="{!! url('servicepackage') !!}"><span class="fa fa-fw fa fa-dropbox"></span>Package Management</a>
                        </li>
						
						
                         <li {!! classActivePath('uploadpdf') !!}>
                            <a href="{!! url('/uploadpdf') !!}"><span class="fa fa-fw fa fa-upload"></span> {{ trans('back/admin.pdf') }}</a>
                        </li>
                        <li {!! classActivePath('partner-order') !!}>
                        <a href="#" data-toggle="collapse" data-target="#visitormenu"><span class="fa fa-fw fa fa-credit-card"></span>Order Management<span class="fa fa-fw fa-caret-down caret-k"></span></a>
                            <ul id="visitormenu" class="collapse">
                                <li><a href="{!! url('/member-order') !!}">Member Orders</a></li>
                                <!--<li><a href="{!! url('/member-order') !!}">Member Orders</a></li>-->
                            </ul>
                        </li>
                         <li {!! classActivePath('affiliation') !!}>
                            <a href="{!! url('/affiliation') !!}"><span class="fa fa-fw fa fa-slideshare"></span>Affiliation Management</a>
                        </li> 
                         <li {!! classActivePath('referalfee') !!}>
                            <a href="{!! url('/referalfee') !!}"><span class="fa fa-fw fa fa-money"></span>Commission Settings</a>
                        </li> 
                        <!--<li {!! classActivePath('service-referal-fee') !!}>
                            <a href="{!! url('/service-referal-fee') !!}"><span class="fa fa-fw fa fa-money"></span>Commission Settings</a>
                        </li>-->
                        <li {!! classActivePath('show-visitors') !!}>
                            <a href="#" data-toggle="collapse" data-target="#visitormenu1"><span class="fa fa-fw fa fa-eye"></span>Visitors Management<span class="fa fa-fw fa-caret-down caret-k"></span></a>
                            <ul id="visitormenu1" class="collapse">
                                <li><a href="{!! url('/show-visitors') !!}">All Visitors</a></li>
                                <li><a href="{!! url('/email-campaign') !!}">Email Campaign</a></li>
                            </ul>
                        </li>
                        <!-- <li {!! classActivePath('show-queries') !!}>
                            <a href="{!! url('/show-queries') !!}"><span class="fa fa-question-circle"></span>Contact Us Management</a>
                        </li> -->
                    @endif                  
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper" class="m-l-20">

            <div class="container-fluid">

                @yield('main')

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /.page-wrapper -->

    </div>
    <!-- /.wrapper -->

    	{!! HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js') !!}
        <script>
        window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')
        </script>
    	{!! HTML::script('js/plugins.js') !!}
    	{!! HTML::script('js/main.js') !!}
    	{!! HTML::script('js/jquery.mask.js') !!}
        {!! HTML::script(asset('/js/bootstrap-datepicker.js'), array('type' => 'text/javascript')) !!}
         @yield('scripts')
   <script>
          $(document).ready(function(){
             $('.datepicker').datepicker({
                    format: "yyyy-mm-dd"
                }); 
        });
             </script>

  </body>
</html>
        