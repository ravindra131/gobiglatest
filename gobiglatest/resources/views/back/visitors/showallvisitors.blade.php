@extends('back.template')
@section('head')
	<style type="text/css">
		.table { margin-bottom: 0; }
	</style>
@stop
@section('main')
{!! Form::open(['class' => ' form-horizontal', 'method' => 'post']) !!}
{!! csrf_field() !!}
<div class="form-group search-form-k">
<div class="col-md-3 col-sm-5">
	{!! Form::text('agentname', null, [
            'class'                         => 'form-control',
            'id'                            => 'agentname',
            'placeholder'                   => 'Member name'
        ]) !!}
</div>
<div class="col-md-3 col-sm-5">
	{!! Form::text('visitorsname', null, [
            'class'                         => 'form-control',
            'id'                            => 'visitorsname',
            'placeholder'                   => 'Visitor Name'
        ]) !!}
</div>
<div class="col-md-3 col-sm-5">
	{!! Form::text('visitorsemail', null, [
            'class'                         => 'form-control',
            'id'                            => 'visitorsemail',
            'placeholder'                   => 'Visitor Email'
        ]) !!}
</div>
<div class="col-md-3 col-sm-5">
<a class="btn btn-primary" href="#" id="search_visitors">Search</a>
</div></div>
<h1 class="page-header row no-margin"><span class="orders">All Visitors</span>
<div class="col-md-2 col-sm-5 pull-right">
<label class="fs-15">Records per page</label>
<select name="records" id="records" class="form-control" onchange="doAjaxSearchVisitors()">
	<option value="10">10</option>
	<option value="20">20</option>
	<option value="30">30</option>
</select>
</div></h1>
{!! Form::close() !!}
<p class="text-right pcsv"><a href="#" onclick="save_visitors()" class="csv-btn">
<span> Export as <img src="{{ asset('images/csv-icon.png') }}"></span>
</a></p>
		<div class="panel" id="resultsContainer">
			@include('back.visitors.part')
		</div>
@stop
@section('scripts')
<script>
 	$('#search_visitors').on('click',function()
 	{
        doAjaxSearchVisitors();
				 
    });
    $('#agentname').keydown(function (e){
    if(e.keyCode == 13){
        doAjaxSearchVisitors();
    }
});
    $('#visitorsemail').keydown(function (e){
    if(e.keyCode == 13){
        doAjaxSearchVisitors();
    }
});
    $('#phoneno').keydown(function (e){
    if(e.keyCode == 13){
        doAjaxSearchVisitors();
    }
});
    function doAjaxSearchVisitors()
	{
		var agentname = $('#agentname').val();
		var vname = $('#visitorsname').val();
		var vemail = $('#visitorsemail').val();
		var records = $('#records').val();
			var dataString  = {SearchText:agentname,visitorsname:vname,visitorsemail:vemail,record:records};
			$.ajax({                       
			   url :  "/admin/search/visitors",
			   type: "GET",
			   dataType: 'JSON',
			   data : dataString, 
			   cache:false,
			   success : function(data){
				   //console.log(data);
				   //alert(data.count);
				   if(data.count > 0)
				   {
				   	$('#resultsContainer').html(data.html);
				   }
				   else
				   {
				   	$('#resultsContainer').html('No records found');
				   }
				   
				 
			   }               
		   });
		
		
	}

	function save_visitors()
	{
		doAjaxSearchVisitors();
		var agentname = $('#agentname').val();
		var vname = $('#visitorsname').val();
		var vemail = $('#visitorsemail').val();
		var dataString  = {SearchText:agentname,visitorsname:vname,visitorsemail:vemail};
		window.location.href = '{!! url('/admin/download-visitors/') !!}' + "?" + serialize(dataString);
	}

 function serialize(obj) {
  var str = [];
  for(var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}
</script>
@stop

