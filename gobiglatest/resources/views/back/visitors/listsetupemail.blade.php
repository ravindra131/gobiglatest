@extends('back.template')
@section('head')
<style>
* {
    box-sizing: border-box;
}
.header {
   
    padding: 15px;
}
.main {
    width: 75%;
    float: left;
    padding: 15px;
    
}
</style>
@stop
@section('main')
<h1 class="k-heading">List of email campaigns</h1>
@if (count($eresults) != 6 )
<p style="text-align: right;"><a href="{!! url('/setup-email') !!}">Click here to set up email campaign</a></p>
@else
<p  class="k-bold"style="text-align: right;">Maximum 6 email templates can be set.</p>
@endif
@if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
 @endif
<div class="panel">
            <div class="panel-heading">
                @if($eresults!='')
                <table class="table">
                    <thead>
                        <tr>
                            <th class="col-lg-1" width="25%">Details </th>
                            <th class="col-lg-1" width="55%">Message Template</th>                           
                            <th class="col-lg-1" width="20%">Action</th>
                        </tr>
                    </thead>
                    @foreach ($eresults as $eresult)
                    <tbody>
                        <tr>
                            <td class="text-k" width="25%"><span class="k-bold">Subject :</span> Homeowner, {{ ucfirst($eresult->subject) }} <br /> 
							<span class="k-bold">Duration :</span> @if($eresult->duration > 0)
                            After {{ $eresult->duration }} day
                            @else
                            Immediate
                            @endif
							<br />
                          <span class="k-bold">  Status :</span> {{ ucfirst($eresult->status) }}							
							</td>
                            <td class="text-k" width="55%">							
							{!! ucfirst(html_entity_decode($eresult->message)) !!}
							<br />
							<span class="k-bold">{First Name}  {Last Name}</span> <br />
							<span class="k-bold">{eMail}</span> <br />
							<span class="k-bold">{Telephone}</span> <br />
							<span class="k-bold">{My Website}</span> 
							</td>
                            
							
                            <td width="20%">
                                <a href="{!! url('/edit-emailsetup', array('c_id' => $eresult->id)) !!}" class="btn btn-warning fa fa-pencil">Edit</a>&nbsp;
                                <a href="{!! url('/delete-emailsetup', array('co_id' => $eresult->id)) !!}" class="btn btn-danger fa fa-trash" id="listdelete" onclick="return confirm('Are you sure to delete this email content?')">Delete</a>
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
                @endif   
            </div>
        </div>
@stop
