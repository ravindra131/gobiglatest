@extends('back.template')
@section('head')
    {!! HTML::style('css/parsley.css') !!}
@stop
@section('main')
	<h1 class="page-header">Test set up email</h1>
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    @if ($errors->has())
    <div class="alert alert-danger">
          @foreach ($errors->all() as $error)
            {{ $error }}<br>
          @endforeach
        </div>
    @endif
	<div class="row">
	<div class="col-sm-12">
		{!! Form::open(['url' => '/sendtestmail', 'method' => 'post', 'class' => 'form-horizontal panel', 'data-parsley-validate']) !!}
    <input type="hidden" name="tempid" value="{{ $temp_id }}">
		<label>Email</label>
		{!! Form::email('email', null, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'inputEmail',
            'data-parsley-required-message' => 'Please enter e-mail address.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-type'             => 'email',
            'data-parsley-type-message'     => 'Please enter a valid e-mail address.'
        ]) !!}
         @if ($errors->has('email'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('email') }}</div>
        @endif
<a href="{!! url('/email-campaign') !!}">Cancel</a>		
		<input type="submit" name="send" value="Send" class="k-submit"> 
        {!! Form::close() !!}
	</div>
	</div>
@stop
@section('scripts')
<script type="text/javascript">
  window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
        };
  </script>
  {!! HTML::script('/js/parsley.min.js') !!}
@stop
