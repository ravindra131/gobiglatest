@extends('back.template')
@section('head')
    {!! HTML::style('css/parsley.css') !!}
@stop
@section('main')
	<h3 class="page-header">Set up email</h3>
	@if (Session::has('message'))
    	<div class="alert alert-success">{{ Session::get('message') }}</div>
	@endif
    @if ($errors->has())
    <div class="alert alert-danger">
          @foreach ($errors->all() as $error)
            {{ $error }}<br>
          @endforeach
        </div>
    @endif
	<div class="row">
	<div class="col-sm-12">
		{!! Form::open(['url' => '/editemailcontent', 'method' => 'post', 'class' => 'form-horizontal panel', 'data-parsley-validate']) !!}
        <input type="hidden" name="cid" value="{{ $edit_result[0]->id }}">
		<label>Subject</label>
		{!! Form::text('subject', $edit_result[0]->subject, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'inputSubject',
            'data-parsley-required-message' => 'Please enter subject.',
            'data-parsley-trigger'          => 'change focusout'
        ]) !!}
         @if ($errors->has('subject'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('subject') }}</div>
        @endif		
		<br>
		<label>Message</label>
		<div id="sample">		
		{!! Form::textarea('area', $edit_result[0]->message, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'area',
            'data-parsley-required-message' => 'Please enter message.',
            'data-parsley-trigger'          => 'change focusout'
        ]) !!}	
		{First Name}  {Last Name} <br />
		{eMail} <br />
		{Telephone} <br />
		{My Website} <br />
         @if ($errors->has('message'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('message') }}</div>
        @endif
        <br>
        <label>Duration</label>
        <select name="duration" class="form-control">
            <option name="immediate" value="0" <?php if($edit_result[0]->duration=="immediate") echo 'selected="selected"'; ?>>Immediate</option>
        	<option name="one" value="1" <?php if($edit_result[0]->duration==1) echo 'selected="selected"'; ?>>After 1 day</option>
			<option name="two" value="2" <?php if($edit_result[0]->duration==2) echo 'selected="selected"'; ?>>After 2 days</option>
			<option name="three" value="3" <?php if($edit_result[0]->duration==3) echo 'selected="selected"'; ?>>After 3 days</option>
			<option name="four" value="4" <?php if($edit_result[0]->duration==4) echo 'selected="selected"'; ?>>After 4 days</option>
			<option name="five" value="5" <?php if($edit_result[0]->duration==5) echo 'selected="selected"'; ?>>After 5 days</option>
			<option name="six" value="6" <?php if($edit_result[0]->duration==6) echo 'selected="selected"'; ?>>After 6 days</option>
			<option name="seven" value="7" <?php if($edit_result[0]->duration==7) echo 'selected="selected"'; ?>>After 7 days</option>
			<option name="eight" value="8" <?php if($edit_result[0]->duration==8) echo 'selected="selected"'; ?>>After 8 days</option>
			<option name="nine" value="9" <?php if($edit_result[0]->duration==9) echo 'selected="selected"'; ?>>After 9 days</option>
			<option name="ten" value="10" <?php if($edit_result[0]->duration==10) echo 'selected="selected"'; ?>>After 10 days</option>
        </select>
        @if ($errors->has('duration'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('duration') }}</div>
        @endif
        <br>
        <label>Save as</label>
        <select name="status" class="form-control" id="status">
            <option name="draft" value="draft" <?php if($edit_result[0]->status=="draft") echo 'selected="selected"'; ?>>Draft</option>
            <option name="publish" value="publish" <?php if($edit_result[0]->status=="publish") echo 'selected="selected"'; ?>>Publish</option>
        </select>
         @if ($errors->has('status'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('status') }}</div>
        @endif
		</div>
		<a href="{!! url('/email-campaign') !!}" class="k-cancel">Cancel</a>
		<input type="submit" name="save" value="Save" class="k-submit">
        <a href="#" class="test-emails" id="testemail">Test Email</a> 
        <div id="showreply"></div>
		{!! Form::close() !!}
		
	</div>
	
	</div>
    
@stop
@section('scripts')
{!! HTML::script('/js/editor.js') !!}
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({maxHeight : 200}).panelInstance('area');
        new nicEditor({fullPanel : false,maxHeight : 200}).panelInstance('area1');
  });
  window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
        };
$(document).ready(function()
{
    $("#testemail").click(function()
    {
        $('#status').val('draft');
        $('#testemail').hide(); 
        var $hidden =  $('<input/>').attr({ type: 'hidden', name:'testid', value:'testemail'}).addClass("form-control");
        var $ctrl = $('<input/>').attr({ type: 'email', name:'email', placeholder:'Email Address', required:'true'}).addClass("form-control");
        var $btt = $('<input/>').attr({ type: 'submit', name:'Test',value:'Test'}).addClass("k-submit");
        var $cancel = '<a href="#" onclick="testcancel()">Cancel</a>';
        $("#showreply").append($hidden);
        $("#showreply").append($ctrl);
        $("#showreply").append($btt);
        $("#showreply").append($cancel);
    });
});
function testcancel()
    {
        $('#testemail').show(); 
        $('#showreply').empty();      
    }
  </script>
  {!! HTML::script('/js/parsley.min.js') !!}
@stop
