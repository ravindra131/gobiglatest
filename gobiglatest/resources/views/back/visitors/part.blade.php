<div class="panel-heading1">
				@if(count($vresults))
				<table class="table">
					<thead>
						<tr>
							<th class="col-lg-1">Member's Name</th>
							<th class="col-lg-1">Visitor's Name</th>
							<th class="col-lg-1">Visitor's Email</th>
							<th class="col-lg-1">Member's Notes</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($vresults as $vresult)
						<tr>
							@if($vresult->agentname != '')
							<td class="text-primary"><strong><a href="{!! route('user.show', [$vresult->agent_id]) !!}">{{ ucfirst($vresult->agentname) }}</a></strong></td>
							<td class="text-k">{{ $vresult->firstname }}</td>
							<td class="text-k">{{ ucfirst($vresult->email) }}</td>
							<td>{{$vresult->notes }}</td>
							@endif
						</tr>
					
					@endforeach
					</tbody>
				</table>
				<div class="pull-right link">
				<?php echo $vresults->render(); ?>
			</div>
				@else
				<h4 style="text-align: center;">There are no visitors.</h4> 
				@endif	
			</div>