<div class="main-div">
@include('include.header')
<div class="top-header">


				<div class="top-content">
					<h1>FREE TRIAL PRICING & SIGNUP </h1> 
					<span class="content-line-2">Price below includes printing & mailing!</span>
				</div>
			
		</div>

		<div class="date">
			<div class="text-center">
			     <div class="date-img"><img src="images/18.png"></div>
			</div>
		</div>
			<div class="">
				<div class="table-h">
				Exclusive for GoBig Newsletter Members only
				</div>
			<table class="newsletter">
			  <tr>
				<th>MONTHLY NEWSLETTER WRITTEN FOR YOU EACH MONTH!<br>
				<span class="small-h-font">(saves you hours of writing, designing & marketing)</th>
				<th>SIGN UP BELOW</th>
			  </tr>
			  <tr>
				<td>PERSONALIZE YOUR NEWSLETTER<br> <span class="small-font">With recipients"First Name" in multiple areas</span></td>
				<td><img src="images/5.png"></td>
				
			  </tr>
			  <tr>
				<td>AUTO-POPULATE YOUR NEWSLETTER<br><span class="small-font">With your profile including photo & logo</span></td>
				<td><img src="images/5.png"></td>
				
			  </tr>
			  <tr>
				<td>PERSONALIZE YOUR THEME, BANNERS AND TEXT<br><span class="small-font">Private library for your images, customer lists...</span></td>
				<td><img src="images/5.png"></td>
				
			  </tr>
			  <tr>
				<td>PHONE, EMAIL & LIVE CHAT CUSTOMER SUPPORT<br><span class="small-font">(we actually have 12 customer service reps)</span></td>
				<td><img src="images/5.png"></td>
			   
			  </tr>
			  <tr>
				<td>DATA "LIST CLEANING"<br><span class="small-font">Realtime CASS Certification and NCOA list cleaning</span></td>
				<td><img src="images/5.png"></td>
			   
			  </tr>
			  <tr>
				<td>FREE DATA LISTS!<br><span class="small-font">Filter on any demographic or do a Radius Mailing</span></td>
				<td><img src="images/5.png"></td>
				
			  </tr>
			  
			   
			  
			   <tr>
				<td>GOBIG MEMBERS ONLY<br><span class="small-font">Buyers club pricing</span></td>
				<td><img src="images/5.png"></td>
				
			  </tr>
			  
			   <tr>
				<td>PICK & PACK <br><span class="small-font">(add 20 or more to be shipped back to your office in addition to being printed and mailed. )</span></td>
				<td><img src="images/5.png"></td>
				
			  </tr>
			  
			  <tr>
				<td>SET IT & FORGET IT<br><span class="small-font">( Build your newsletter the way you like it, then walk away... We'll print and mail it for you automatically.)</span></td>
				<td><img src="images/5.png"></td>
				
			  </tr>
			  
			  <tr>
				<td>FREE TRIAL<br><span class="small-font">(Try risk-free for 3 days)</span></td>
				<td><img src="images/5.png"></td>
				
			  </tr>
			  
			  <tr>
				<td>HAVE YOUR OWN NEWSLETTER?<br><span class="small-font">( Simply upload it into our pre-formatted Newsletter Template. Same great print & mail price. )</span></td>
				<td><img src="images/5.png"></td>
				
			  </tr>
			  
			</table>
			</div>

<div class="signup-price">
<div class="signup-price-h">Warning! May cause referrals!</div>
<div class="row m-l-65 ">

<div class="col-md-3 col-sm-4 col-md-offset-2 col-sm-offset-2 col-xs-6 text-center">
<div class="inline">
<div class="price-1">${{ $Packageresult[0]->monthly_fee }}/ <span class="small-mo">Mo</span></div>
<div class="prices-img"><img src="images/19.png"></div>
@if(session('statut') == 'visitor')
<a href="{{ url('auth/get-member-register', array('package_id' => 1, 'amount' => $Packageresult[0]->monthly_fee)) }}"><button class="sign-up-b">SIGN UP</button></a>
@elseif(session('statut') == 'user')
<a href="{{ url('/getmember-payment', array('user_id' => auth()->user()->id,'package_id' => 1, 'amount' => $Packageresult[0]->monthly_fee)) }}"><button class="sign-up-b">SIGN UP</button></a>
@endif
</div>
</div>
<div class="col-md-3 col-md-offset-1 col-sm-4 col-xs-6 text-center">
<div class="inline">
<div class="small-h">save {{round(((($Packageresult[0]->monthly_fee*12)-($Packageresult[0]->annual_fee))*100)/($Packageresult[0]->monthly_fee*12))}}%</div>
<div class="price-1 m-t-27s">${{ $Packageresult[0]->annual_fee }}/ <span class="small-yr">Yr</span></div>
<div class="prices-img"><img src="images/19.png"></div>
@if(session('statut') == 'visitor')
<a href="{{ url('auth/get-member-register', array('package_id' => 2, 'amount' => $Packageresult[0]->annual_fee)) }}"><button class="sign-up-b">SIGN UP</button></a> </div>
@elseif(session('statut') == 'user')
<a href="{{ url('/getmember-payment', array('user_id' => auth()->user()->id,'package_id' => 2, 'amount' => $Packageresult[0]->annual_fee)) }}"><button class="sign-up-b">SIGN UP</button></a>
@endif
</div>
</div>

</div>


<div class="mid-content">
<div class="row">

<div class="col-sm-6">
<h2 class="heading-1"><span class="content-1-box-icon"></span>Why Is Your Printing So Inexpensive?</h2>
<p class="box-1-content">We have the most efficient technology in the world. This allows us to manage our process and gang orders together, although each order is completley different! </p>
</div>

<div class="col-sm-6">
<h2 class="heading-1"><span class="content-1-box-icon"></span>FYI</h2>
<p class="box-1-content">Our prices are better then anyone out there including FedEx-Kinkos, Staples, OfficeMax, or Bob the printer down the street! Just look at our pricing below!</p>
</div>

<div class="col-sm-6">
<h2 class="heading-1"><span class="content-1-box-icon"></span>TRY IT FREE:</h2>
<p class="box-1-content">To log in and see it in action Please <a href class="click-button">Click HERE:</a><br>
Username: demo@demo.com<br>
Password: demo</p>
</div>
</div>

</div>


<div class="fees padding-20">
<div class="row">
<div class="industries">GOBIG NEWSLETTER IS THE TOP CHOICE FOR A MULTITUDE OF INDUSTRIES, <br/><strong>NEW AND EXPERIENCED, BIG AND SMALL.</strong></div>
<div class="second-line">
<b>No</b><span class="highlight-cont"> Stepup fees. </span><b>No </b><span class="highlight-cont">Hidden fees. </span> <b>No </b><span class="highlight-cont"> Contracts.</span>
</div>
<hr class="hori-line"></hr>
<div class="table-2-h">
PRICING | PRINT & POSTAGE! - NO HIDDEN FEES
</div>

<table class="fees-table">
  <tr>
  <th width="30"></th>
    <th >QTY</th>
    <th>COLOR</th>
  
  </tr>
  <tr>
  <td><img src="images/20.png"></td>
    <td>50-249</td>
    <td>.99 ea</td>
 
  </tr>
  <tr>
  <td><img src="images/20.png"></td>
    <td>250-999</td>
    <td>.89 ea</td>
    
  </tr>
  <tr>
  <td><img src="images/20.png"></td>
    <td>1000+</td>
    <td>.83 ea</td>
    
  </tr>
  
</table>

<div class="table-2-h">
20" x 11" FULL COLOR &bull; 24# SMOOTH BOND &bull; TRI-FOLDS TO 8.5" x 5.5"
</div>
<div class="bonus">Bonus</div>
<div class="orange-content">
Customize & Send Your Personalized Handwritten Postcards, Yellow Letters,
Greeting Cards & more!
</div>
<div class="row">
<div class="col-sm-4">
<div class="handwritten-h">Handwritten Posters </div>
 <div class="Handwritten"><img src="images/7.png"></div>
  </div>

  <div class="col-sm-4">
<div class="handwritten-h">Handwritten Yellow Letters </div>
 <div class="Handwritten"><img src="images/8.png"></div>
  </div>
  
  <div class="col-sm-4">
<div class="handwritten-h ">Personalized "Doodle" Letters</div>
 <div class="Handwritten handwritten-mobile"><img src="images/9.png"></div>
  </div>
  </div>

</div>
</div>



<div class="customer-data">
<div class="row">


<div class="bonus-h">Bonus</div>
<div class="down-arrow"><img src="images/10.png"></div>

<div class="customer-h">
UNLIMITED FREE <b>CUSTOMER DATA</b>
</div>
<div class="row no-margin">
<div class="col-sm-6"><img src="images/11.png"></div>
<div class="col-sm-6">
<div class="points">
<h2 class="point-1"><span class="customer-data-icon"></span>Select from the data filters as in the example to the right.<span class="highlight-cont-2"> Target your perfect customer.</span></h2>
</div>

<div class="points">
<h2 class="point-1"><span class="customer-data-icon"></span>Choose how many you wish to have and add it to your data library. <a href ="#"class="highlight-cont-3">FREE.</a></h2>
</div>

<div class="points">
<h2 class="point-1"><span class="customer-data-icon"></span>There is no charge, or hidden fees or limit to the number of lists or data files you select.</h2>
</div>

<div class="points">
<h2 class="point-1"><span class="customer-data-icon"></span> Add it to your existing customer data list when you mail, orchoose several lists from us.</h2>
</div>

<div class="points">
<h2 class="point-1"><span class="customer-data-icon"></span>Don't forget to do a radius search and isolate the exact location you wish to mail to. (Did you want to mail only to specific streets? - No Prob! Simply list the streets you want to mail to)</h2>
</div>

</div>
</div>
</div>
</div>

<div class="last-content padding-20">
<div class="row">


<div class=" col-md-6 content-1-box last-h">
<h2 class="heading-1"><span class="content-1-box-icon"></span>What does the monthly fee include?</h2>
<p class="box-1-content">The monthly subscription gives you access to a pre-created and written newsletter. It gives you the right to use it.
</p>

<h2 class="heading-1"><span class="content-1-box-icon"></span>DOES THE MONTHLY FEE INCLUDE PRINTING OR POSTAGE?</h2>
<p class="box-1-content">No. You pay for printing & postage. It is based on how many you have on your customer list. Our prices are very competitive and have price breaks at different levels.</p>

<h2 class="heading-1"><span class="content-1-box-icon"></span>Why is the "Set it & Forget it" program more popular?</h2>
<p class="box-1-content">We are like you. We're busy and want to manage our time wisely. We would prefer setting our marketing up and walking way. It's working for you while you sleep!</p>

<h2 class="heading-1"><span class="content-1-box-icon"></span>How long does it take to arrive?</h2>
<p class="box-1-content">It takes us under 24 hours to clean your customer list and print your newsletter. After that, it's in the hands of the Post Office. If you choose standard mail, it can take from 4-12 days to deliver. 4 if you are in the southwest and 12 if you are back east!</p>
<h2 class="heading-1"><span class="content-1-box-icon"></span>Why are the newsletters filled with "lifestyle" content?</h2>
<p class="box-1-content">Because it's interesting and fun information that 99% of people like to read. Tidbits of information!
It's not filled with industry specific information. It's meant to "Touch" your customer in a fun and non-salesy way!</p>


</div>

<div class=" col-md-6 content-1-box last-h last">


<h2 class="heading-1"><span class="content-1-box-icon"></span>Why are the newsletters so successful?</h2>
<p class="box-1-content">The secret behind our newsletters is... they are fun! We make them to look like you did it yourself. They are informative and simple and they don't sell anything. We have many built in "Call to actions" to keep your reader interested and "YOU" on their mind.
They are less expensive then a color postcard.
Is your client worth about $.69 a month? We think so!
</p>

<h2 class="heading-1"><span class="content-1-box-icon"></span>When should I order the newsletter if I do it myself?</h2>
<p class="box-1-content">On or about the 15th.
We strongly suggest ordering it on the 15th of each month. That way, it will arrive at your customers door around the first of each month.</p>

<h2 class="heading-1"><span class="content-1-box-icon"></span>Can I change the articles?</h2>
<p class="box-1-content">You can cutomize many different things, yet you can't change the articles. In the 15 years of supplying newsletters, most people find that they don't need to. Sure, some people want to and we understand. SO, simply upload your own newsletter into our Smart Template™ and we'll treat it like it was one of ours...</p>


</div>

</div>
</div>
@include('include.footer')
</div>