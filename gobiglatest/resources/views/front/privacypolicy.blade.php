@extends('include.header')
@section('main')
<section class="all-pagesbg">
<h1>Privacy Policy</h1>
<div class="pagesoverlay"></div>
</section>
<div class="container m-t-40">
<div class="green-heading  about-us"><div class="text-center"><h3>Privacy Policy<span class="icon-cross-headiing"><img src="{{ asset('/images/grey-icon-heading.png') }}"><span></h3></div>
<p class="m-t-20">Legal Disclaimer & Copyright Notice</p>
<p>All rights reserved. No part of this publication may be reproduced, distributed, or transmitted in any form or by any means, including photocopying, recording, or other electronic or mechanical methods, without the prior written permission of the publisher, except in the case of brief quotations embodied in critical reviews and certain other noncommercial uses permitted by copyright law.</p>
</div>
</div><!--@end about us-->
@include('include.footer')
@stop
