<div id="newheader">
@include('include.header')
</div>
<section class="all-pagesbg">
<h1>Contact Us</h1>
<div class="pagesoverlay"></div>
</section>
<div class="container m-t-40">
<div class="green-heading text-center"><h3>contact us form<span class="icon-cross-headiing"><img src="{{ asset('/images/grey-icon-heading.png') }}"><span></h3>
</div>
{!! Form::open(['url' => '/add-contactus', 'class' => ' form-horizontal', 'method' => 'post', 'id' => 'cform']) !!}	
<div class="row m-t-20">
<div class="col-md-8 col-md-offset-2">
<div class="bg-light-form">
	@if (Session::has('message'))
    <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
<div style="display:none;" id="contact_error" class="alert alert-danger parsley"></div>
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Name </label>
<div class="col-md-9 col-sm-8">
	{!! Form::text('name', null, [
            'class'                         => 'form-control',
            'id'                            => 'inputname'
        ]) !!}
</div>
</div>
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Email address </label>
<div class="col-md-9 col-sm-8">
	{!! Form::email('email', null, [
            'class'                         => 'form-control',
            'id'                            => 'email'
        ]) !!}
</div>
</div>
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Message </label>
<div class="col-md-9 col-sm-8">
	{!! Form::textarea('message', null, [
            'class'                         => 'form-control text-area-height',
            'id'                            => 'message'
        ]) !!}
</div>
</div>
<p class="m-t-10 text-center"><input type="button" Value="Submit" class="login_btn fs-22" id="cregister"></p>
</div>
</div>

</div>
</div>
@include('include.footer')


<script>
$('#cregister').click(function(){
    var name = $('#inputname').val();
    var email = $('#email').val();
    var message = $('#message').val();
    var regex_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var regex_name = /^[a-zA-Z\s]*$/;
    if(name == '' && email == '' && message == '')
    {
        $('#contact_error').html('Please fill all the fields.').show();
        return false;
    }
    else if(name == '')
    {
        $('#contact_error').html('Please enter name.').show();
        return false;
    }
    else if(regex_name.test(name) == false)
    {
        $('#contact_error').html('Please enter a valid name.').show();
        return false;
    }

    else if(email == '')
    {
        $('#contact_error').html('Please enter email address.').show();
        return false;
    }
    else if(regex_email.test(email) == false)
    {
        $('#contact_error').html('Please enter a valid email address.').show();
        return false;
    }
    else if(message == '')
    {
        $('#contact_error').html('Please enter message.').show();
        return false;
    }
    else
    {
        $('#contact_error').html('').hide();
        $("#cregister").val('Processing....');
        $('#cregister').prop("disabled",true);
        $('#cform').submit();
    }

});
</script>

