<div class="main-div">
@include('include.header')

<section class="all-pagesbg">
<h1>Pricing</h1>
<div class="pagesoverlay"></div>
</section>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<div class="social-menu">
<div class="addthis_inline_share_toolbox"></div>
</div>


<div class="pricing-testimonial">
  <div class="carousel slide" data-ride="carousel" id="quote-carousel">
        <!-- Bottom Carousel Indicators -->
        <!--ol class="carousel-indicators">
          <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
          <li data-target="#quote-carousel" data-slide-to="1"></li>
          <li data-target="#quote-carousel" data-slide-to="2"></li>
        </ol-->
        
        <!-- Carousel Slides / Quotes -->
        <div class="carousel-inner">
	
          <!-- Quote 1 -->
          <div class="item active">
            <blockquote>
			<div class="min-hegt-100">
               <p> <i class="fa fa-quote-left" aria-hidden="true"></i> Andy and his team at <strong>GoBigWeb360</strong> have a top-notch product and Company.  I've toured their 30k sf Direct Mail Facility and have complete confidence in their capabilities to deliver on time and with complete professionalism. Their team of 60+ and state of the art equipment make this one of the largest Print &amp; Direct Mail houses on the West coast. Their "Easy to use" Web-To-Print user interface is sophisticated - yet simple. I encourage you to feel confident in working with them.<br>
                 <span><b>~ Aaron Halderman, 
                  <br>V.P. GKIC | Glazer-Kennedy </b>
                  <img src="{{ asset('/images/GKIC-Logo-web.jpg') }}" class=" m-t-20-less m-l-15">
                </span>
                </p>
                 </div>
            </blockquote>
          </div>
          <!-- Quote 2 -->
          <div class="item">
            <blockquote>
			<div class="min-hegt-100">
               <p> <i class="fa fa-quote-left" aria-hidden="true"></i> They get it! They've created a solution that delivers quality communication to our customers. With little effort we have leveraged technology with design, providing us a strong relationship with our clients.<br>
               <span><b> ~ Steven Hodges</b></span></p>
                  </div>
            </blockquote>
          </div>
          <!-- Quote 3 -->
          <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> After only one month my website is bringing in a steady flow of quality leads. Couple that with excellent customer service and I couldn't be happier with my New "Hands-Free" Marketing!<br> 
              <span><b> ~ Melanie Schwartz </b></span> </p>
            </div>
            </blockquote>
          </div>
           <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> We chose <strong>GoBigWeb360</strong> because of their excellent customer service, quick turn around and intuitive web-based application. One of the biggest benefits of using this website for investors is that if you want to change something, you do it yourself. You don’t have to call anyone. As a result of such a quick turn around, you will get leads faster to jump start your business.<br>
             <span><b> ~ Sean Terry, Flip2Freedom</b></span>
            </p>
            </div>
            </blockquote>
          </div>
           <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> We tried creating our own website -Not only was it a nightmare, it cost a lot of time and money. Thank goodness we found <strong>GoBigWeb360</strong>. Not only are your prices exceptional, my website runs by itself. AND... It’s pre-written so I can spend my time doing other things!<br>
            <span><b> ~ Amy Jensen, CA</b></span>
             </p>
            </div>
            </blockquote>
          </div>
           <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> I am very appreciative of the fact you bailed me out. I wanted to have my website ready for the investor summit, but I waited too long to place the order.    Thank goodness <strong>GoBigWeb360</strong> offers a turnkey solution that is live the second you buy it. You never hesitated to do everything in your power to accommodate my needs while delivering a high quality product and showing complete flexibility to customize my site at a very reasonable price. Your approach to customer service is refreshing. Thanks for a great job.<br>
            <span><b> ~ Hernando Ramirez - New Horizons Real Estate Solutions</b></span>
             </p>
            </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> From creation to delivery, your program has been nothing but perfect. Not only have you given our members an easy to use system at buyers club pricing, you’ve made their marketing “EASY.” You made all of our current fulfillment challenges obsolete! Thanks again for providing stellar customer service for our Dean Graziosi &amp; PMI Marketing members in the Real Estate Profession.<br>
            <span><b> ~ Brandon Maughan, Dir. Bus Dev | PMI Marketing | Dean Graziosi </b></span>
             </p>
            </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> <strong>GoBigWeb360</strong> is the online solution I've been waiting for! I've tried other sites with results that ranged from so-so product to failure to launch and even simply being ripped off! Not here - <strong>GoBigWeb360</strong> has a quality, working site that has delivered on-time, as ordered, at an exceptional price.<br>
            <span><b> ~ Jen Fredericks, SC </b></span>
             </p>
            </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> <strong>GoBigWeb360</strong> has answered everytime I called with a silly question. I feel like they are my very own personal assistant! If I don't see something online that I want, they'll create it for me. I just love the personal touch!<br>
            <span><b> ~ Laura Santiago, Ohio</b></span>
             </p>
            </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> I know there are lot's of places to get an investor website, but not every place has what I need. <strong>GoBigWeb360</strong> has everything I need for my specific industry. I'm a real estate investor so I use the website to reach out to top folks that need help with their housing situation.<br>
            <span><b> ~ Carl Ewings, Phoenix Az</b></span>
          </p>
            </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> I have to tell you, you really have an impressive program. We were considering doing our website locally but not only are your prices very competitive, you make it much easier to go through the whole process. Thanks so much!<br>
            <span><b> ~ Sam Johnson</b></span>
          </p>
            </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> Through June, we've closed 4 deals from our website for a total $52,366.00. We also gets tons of positive feedback from our customers on how much they love the website as a resource and guide. Personally, we think its an awesome way to keep your business name in front of the customer so that they can't forget who helped them the last time!<br>
            <span><b> ~ Beatrice Alexanders - ABC Investors</b></span>
          </p>
            </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> Thank you for providing a top quality website &amp; excellent customer service for our agents in our Brokerage firm. From the "on-boarding" process to launch, everything was top notch.<br>
            <span><b> ~ Cindy Landers</b></span>
          </p>
            </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> <strong>GoBigWeb360</strong> has been great! Easy to work with and always there to help. We’ve tried several other services but <strong>GoBigWeb360</strong> is by far the best. They’ve taken the hassle and headache out of getting our up! And, our results awesome! Thank you!<br>
            <span><b> ~ Andraya Petersen | All Pro Investor</b></span>
          </p>
            </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> <strong>GoBigWeb360</strong> has been awesome and I'll definitely keep using the Investor site for my marketing. Your personal help getting me over the 'bumps' has been awesome. I look forward to a long relationship with your company.<br>
             <span><b> ~ Dave Lightner, Fl</b></span>
           </p>
            </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
			<div class="min-hegt-100">
            <p><i class="fa fa-quote-left" aria-hidden="true"></i> I have been working with <strong>GoBigWeb360</strong> for the last 8 years with other products. They have been helpful and knowledgable on everything "Marketing." I wouldn't order my creative, marketing material or investor website any place else!<br>
             <span><b> ~ Jenny J, NC</b></span>
           </p>
            </div>
            </blockquote>
          </div>
        </div>
        
        <!-- Carousel Buttons Next/Prev -->
        <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
        <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
      </div>




</div><!--@end Container-->
<section class="green-bg">
<div class="">
<div class="white-heading text-center">
<h3>Choose a plan for your real estate investor business<span class="icon-white-headiing"><img src="images/white-icon-heading.png"><span></span></span></h3>
</div>
<div class="drop-icon">
</div>
</div>
</section>
<div class="website-section">
<h3>"Get your website free"</h3>
<a href="{!! url('/get-website-free') !!}">Click here for details</a>
</div>
<div class="container">
<section class="pricing">
<div class="row choose-plan">
	<div class="col-xs-12">
		<div class="col-xs-10 col-sm-11">Auto Populates with Your Profile
	</div>
		<div class="col-xs-2 col-sm-1"><i class="fa fa-check"></i></div>
	</div>
</div>
<div class="row choose-plan odd">
	<div class="col-xs-12">
		<div class="col-xs-10 col-sm-11">Built in Auto EMailSequence
	</div>
		<div class="col-xs-2 col-sm-1"><i class="fa fa-check"></i></div>
	</div>
</div>
<div class="row choose-plan">
	<div class="col-xs-12">
		<div class="col-xs-10 col-sm-11">Google & SEO Compliant
	</div>
		<div class="col-xs-2 col-sm-1"><i class="fa fa-check "></i></div>
	</div>
</div> 
<div class="row choose-plan odd">
	<div class="col-xs-12">
		<div class="col-xs-10 col-sm-11">Effective & Friendly Design
	</div>
		<div class="col-xs-2 col-sm-1"><i class="fa fa-check"></i></div>
	</div>
</div>
</section>
<section class="package-montyear m-t-30">
	@foreach ($packages as $package)
	<div class="row">
		<div class="col-sm-6 col-md-5 col-md-offset-1">
		   <div class="discount">&nbsp;</div>
				<span class="rate-set">${{ $package->monthly_fee }}/<small>Mo</small> </span>
				<input type="button" Value="Order /  month" class="login_btn fs-22 inline" onclick="window.location='{{ url("auth/getregister") }}'">
		</div>
		<div class="col-sm-6 col-md-5 col-md-offset-1">
		    <div class="discount">Save {{ round(((($package->monthly_fee*12)-($package->annual_fee))*100)/($package->monthly_fee*12)) }}%</div>
			<span class="rate-set">${{ $package->annual_fee }}/<small>Yr</small> </span>
			<input type="button" Value="Order / Year" class="login_btn fs-22 inline" onclick="window.location='{{ url("auth/getregister") }}'">
		</div>
	</div>
	@endforeach
</section>
</div>


	

<!--Footer start-->
@include('include.footer')

<!---#End Footer--->
</div>