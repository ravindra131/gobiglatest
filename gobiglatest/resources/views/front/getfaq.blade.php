@extends('include.header')
@section('main')
<section class="all-pagesbg">
<h1>Frequently Asked Questions</h1>
<div class="pagesoverlay"></div>
</section>
<div class="container m-t-40">
<div class="green-heading  about-us">
    <p>We understand that you probably have a few questions regarding the way our Real Estate Investor Website service works. Take a look at a few of our most frequently asked questions along with the answers to them! While we have tried to cover all of the basics, you may still have a couple of questions more specific to your needs. If you still happen to have any questions after reading over our list below, please do not hesitate to give us a call or fill out one of our contact forms. We would be more than happy to provide you with detailed answers to all of your questions.</p>
    <p class="m-b-10"><strong>Q: How does your service differ from that of other companies that sell websites?</strong></p>
    <p>A: We have been working with Real Estate Coaching companies for over a decade. We are the premier direct mail vendor for over 20 coaching companies and their students. We have learned a lot along the way. We’ve applied our knowledge into these websites. However, the key element to our success is that you can be up and running in less then 5 minutes with a performance driven website built to make you look like a pro. BUT, a sympathetic and compassionate “Family Oriented” small business. Not a conglomerate that is unreachable and disconected.</p>
    <p class="m-b-10"><strong>Q: What makes your service so good?</strong></p>
    <p>A: The website you will get is built by pros to dynamically apply your profile all over. You can be “hands­free” as everything is “DONE FOR YOU.” We know how busy you are. The last thing you need to worry about is how to build a website. Furthermore, we know the keywords and phrases that encourage a visitor to enter their information onto your website. These “Call to Actions” are very important. Once more, we then automatically send out emails to that visitor on your behalf.</p>
    <p class="m-b-10"><strong>Q: Are your prices fair?</strong></p>
    <p>A: We think so. Not only can you get it FREE, you can quit anytime. There are no contracts. Our competitive monthly charge is well below most website vendors. We can charge so little because we built the system so that you can order and manage it without asking us to do anything.</p>
    <p class="m-b-10"><strong>Q: Do you offer referral or affiliation commissions?</strong></p>
    <p>A: YES. See our “Affiliate” page on how you can earn by just sharing what you are already doing.</p>
    <p class="m-b-10"><strong>Q: How do I resell my website?</strong></p>
    <p>A: Easy. In the back office are dozens of beautiful banners pre­built with your affiliate code. All you have to do is share them on social websites. When clicked, it will take them to your website. AND, the link will always remember your affiliate code.</p>
    <p class="m-b-10"><strong>Q: Can I change the video on my website?</strong></p>
    <p>A: Yes, simply upload it in your back office.</p>
    <p class="m-b-10"><strong>Q: Why is the Free eBook important?</strong></p>
    <p>A: This Free eBook written with you in mind. It will give the visitor multiple options on selling their home. However, it is written to make you the best option.</p>
    <p class="m-b-10"><strong>Q: What domain name can I use?</strong></p>
    <p>A: You can use any domain name you wish. Simply enter it on the sign up page. However, we have conveniently made it very simple for you to use ours. We own “BuysHouses4Cash.com” SO, you can simply use your First Name on our sign up page. Therefore, if your name is “Mike” - your website would be: Mike.BuysHouses4Cash.com - Pretty cool eh?
    <p class="m-b-10"><strong>Q: I don’t have any testimonials yet. I’m a newbie!</strong></p>
    <p>A: That is ok. We have conveniently written them for you. AND, your company name will automatically be placed in them as though they were yours in the first place.</p>
    <p class="m-b-10"><strong>Q: What happens when a visitor fills out the form?</strong></p>
    <p>A: You get notified! AND, better yet, your visitor will get 5 emails sent out on your behalf every other day. Each email pre-written with your profile and logo on it. AND, each email is packed with valuable information that ultimately makes you look like a hero and the best option when selling their home.</p>
    <p class="m-b-10"><strong>Q: I noticed that I enter the City I want to buy in. What if I change Cities?</strong></p>
    <p>A: Simply change it in your back office. It will then update all the pages in your website.</p> 
    <p>If you have any questions, please don’t hesitate to contact us at: help@gobigprinting.com</p>
</div>
</div><!--@end about us-->
@include('include.footer')
@stop



