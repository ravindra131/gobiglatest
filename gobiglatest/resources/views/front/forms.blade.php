@extends('include.header')
@section('main')
{!! Form::open(['url' => '/getpackage', 'method' => 'post', 'class' => 'bg-light-form form-horizontal']) !!}
<fieldset id="theme">
	<section class="all-pagesbg">
<h1>Select Your Theme</h1>
<div class="pagesoverlay"></div>
</section>
<div class="container">
<div class="row m-t-30">
<div class="col-sm-4 m-t-20">
<div class="view-demo-theme">
<img src="images/theme-1.png">
<div class="captionoverlay"><a href="javacript:void(0);" class="gowib-btn pull-left">View Demo</a>
<a href="javascript:void(0);" class="gowib-btn pull-right" onclick="choose();">Select your Theme</a>
</div>
</div>
</div>
<div class="col-sm-4 m-t-20">
<div class="view-demo-theme">
<img src="images/theme-2.png">
<div class="captionoverlay"><a href="javacript:void(0);" class="gowib-btn pull-left">View Demo</a>
<a href="javascript:void(0);" class="gowib-btn pull-right" onclick="choose();">Select your Theme</a>
</div>
</div>
</div>
<div class="col-sm-4 m-t-20">
<div class="view-demo-theme">
<img src="images/theme-3.png">
<div class="captionoverlay"><a href="javacript:void(0);" class="gowib-btn pull-left">View Demo</a>
<a href="javascript:void(0);" class="gowib-btn pull-right" onclick="choose();">Select your Theme</a>
</div>
</div>
</div>
</div>
</div>
	</fieldset>
<fieldset id="first" style="display:none;">
<section class="all-pagesbg">
<h1>Buying Site</h1>
<div class="pagesoverlay"></div>
</section>

<div class="container m-t-40">
<div class="green-heading text-center"><h3>registration form<span class="icon-cross-headiing"><img src="images/grey-icon-heading.png"><span></h3>
</div>
<div class="row m-t-20">
<div class="col-md-8 col-md-offset-2">
 
<div class="form-group">
<label class="col-md-3 control-label">Name </label>
<div class="col-md-9"><input type="text" name="name" class="form-control"></div>
</div>
<div class="form-group">
<label class="col-md-3 control-label">Email address </label>
<div class="col-md-9"><input type="text" name="email" class="form-control"></div>
</div>
<div class="form-group">
<label class="col-md-3 control-label">Password </label>
<div class="col-md-9"><input type="password" name="password" class="form-control"></div>
</div>
<div class="form-group">
<label class="col-md-3 control-label">Confirm Password  </label>
<div class="col-md-9"><input type="password" name="cpassword" class="form-control"></div>
</div>
</div><!--#End form section-->

<div class="col-md-12 m-t-50">
<div class="bg-light-form form-horizontal">
<div class="form-group">
<label class="col-md-3 www-label text-right col-sm-3">www.</label>
<div class="col-md-6 col-sm-5 ">
	<input type="text" name="domain" class="form-control" placeholder="Dummy">
</div>
<label class="col-md-3 col-sm-4 www-label">gobigweb360.com</label>
</div>
</div>
</div>
</div>
<p class="m-t-30 text-center"><input type="button" Value="Previous" class="gowib-btn-all fs-22" id="pre" onclick="previous('pre_reg');"></p>
<p class="m-t-30 text-center"><input type="button" Value="Continue" class="gowib-btn-all fs-22" id="continue"></p>
</div>
</fieldset>
<fieldset id="second" style="display:none;">
	<section class="all-pagesbg">
<h1>Select Package</h1>
<div class="pagesoverlay"></div>
</section>
<div class="container m-t-40">
<div class="green-heading text-center">
<p>Lorem ipsum dolor sit amet, moderatius reformidans eu vis, at vis primis molestiae. Nec feugait vulputate eu, case theophrastus et eum, labore nominati inciderint ei sea. Per ex ipsum accumsan, eam no principes voluptaria. Tempor accumsan eos eu, scripta erroribus duo cu, soleat perpetua theophrastus per ea.</p>
</div>

<div class="row payshow ">
<div class="col-md-1 hidden-sm hidden-xs">&nbsp;</div>
<div class="col-md-4 col-sm-6 m-t-170">
<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						<div class="pay-out">
						<p>User pay</p>
						<h3>$39</h3>
						<p>Per month</p>
						</div>
					</h3>
				</div>
				<ul class="per-month">
					<li>Lorem ipsum dolor sit amet</li>
					<li>Lorem ipsum dolor sit amet</li>
					<li>Lorem ipsum dolor sit amet</li>
					<li>Lorem ipsum dolor sit amet</li>
				</ul>
				<div class="panel-footer">
					<input type="button" Value="Get your website" class="gowib-btn-all fs-22" id="getweb">
				</div>
			</div>
		</div>


<div class="col-md-2 hidden-sm hidden-xs text-center m-t-30">&nbsp;</div>
<div class="col-md-4 col-sm-6 m-t-170"><div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						<div class="pay-out">
						<p>User pay</p>
						<h3>$375</h3>
						<p>annually</p>
						</div>
					</h3>
				</div>
				<ul class="per-month">
					<li>Get 20% discount</li>
					<li>Lorem ipsum dolor sit amet</li>
					<li>Lorem ipsum dolor sit amet</li>
					<li>Lorem ipsum dolor sit amet</li>
				</ul>
				<div class="panel-footer">
					<input type="button" Value="Get your website" class="gowib-btn-all fs-22" id="getwebs">
				</div>
			</div></div>
			<p class="m-t-30 text-center"><input type="button" Value="Previous" class="gowib-btn-all fs-22" id="pre" onclick="previous('pre_package');"></p>
</div><!---@End Pay show -->
</div>
</fieldset>
<fieldset id="third" style="display:none">
		<section class="all-pagesbg">
<h1>Review & Checkout</h1>
<div class="pagesoverlay"></div>
</section>
<div class="container m-t-40">
<table class="checkout-table" cellspacing="0" cellpadding="0">
<tr class="bg-black">
<th>Description</th>
<th>Price</th>
</tr>
<tr>
<td colspan="2" height="15px"></td>
</tr>
<tr class="bor-all">
<td><a href="javascript:void(0);"><i class="fa fa-times-circle"></i></a> $39 per Month</td>
<td>$15.00 USD</td>
</tr>
<tr class="bor-all">
<td class="text-right"> Subtotal:</td>
<td>$15.00 USD</td>
</tr>
<tr class="bor-all green-bg">
<td class="text-right"> Total Due Today:</td>
<td>$15.00 USD</td>
</tr>
<tr class="bor-all">
<td class="text-right"> Total Recurring:</td>
<td>$15.00 USD Monthly</td>
</tr>
</table><!--# End table-->

<div class="green-heading text-center m-t-40"><h3>get your website<span class="icon-cross-headiing"><img src="images/grey-icon-heading.png"><span></span></span></h3>
</div> 
<div class="text-center m-t-30">
<span class="domain-regist">Domain Registrant Information</span>
</div>
<div class="bg-light-payment m-t-50">
<div class="row domain-reg">
<div class="col-md-5">
<h3>Promotional Code</h3>
<div class="form-group">
<input type="text" value="" name="" class="form-control" >
<input type="submit" class="gowib-btn-black m-t-20" value="Validate Code">
</div>
</div>
<div class="col-md-6 col-md-offset-1">
<h3>Payment Method</h3>
<form action="" class="form-horizontal"> 
<div class="form-group">
	<label class="col-md-3 control-label">Card Type </label>
		<div class="col-md-9">
			<select class="form-control">
			<option>Visa</option>
			</select>
		</div>
</div>
<div class="form-group">
	<label class="col-md-3 control-label">Card Number </label>
		<div class="col-md-9">
			<input type="text" value="" class="form-control">
		</div>
</div>
<div class="form-group">
	<label class="col-md-3 control-label">Expiry Date </label>
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-6">
				 <select class="form-control">
					<option>01</option>
				 </select>
				</div>
				<div class="col-md-6">
				 <select class="form-control">
					<option>2016</option>
				 </select>
				</div>
			</div>		 
		</div>
</div>
<div class="form-group">
	<label class="col-md-3 control-label">CVV/CVV2 Number  </label>
		<div class="col-md-9">
			<select class="form-control">
			<option>CVV</option>
			<option>CVV2</option>
			</select>
		</div>
</div>
</form>
</div>

</div>
</div><!--#End BG LIGHT PAYMENT-->

<div class="text-center check-style m-t-30">
<p>
<input id="iagree" name="iagree" type="checkbox" value="iagree">
<label for="iagree"> I have read and agree to the Terms of Service <label>
</p>
<p class="m-t-20 text-center"><input type="button" Value="Previous" class="gowib-btn-all fs-22" id="pre" onclick="previous('pre_checkout');"></p>
<p class="m-t-20"><input type="button" Value="Complete order" class="gowib-btn-all fs-22" data-toggle="modal" data-target=".bs-example-modal-sm">
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="congrates-box">
	    <div class="right-box">
		<img src="images/right-icon.png" alt="">
		</div>
         <h2>Congratulations </h2> 
          <h3> You have filled the basic steps/details</h3>
           <h3 class="text-2a"> Now Continue to building</h3>	
			<h2 class="m-t-0 fs-22">YOUR WEBSITE </h2> 
           <p><a href="javascript:void(0);" class="gowib-btn-all fs-18 btn-block">Click To Continue</a></p>	
<p class="text-center no-margin"><a href="javascript:void(0);" data-dismiss="modal" class="text-9a fs-18"><u>Cancel</u></a></p>		   
		</div>
	  </div>
    </div>
  </div>
</div></p>
</div>
</div>
	</fieldset>
{!! Form::close() !!}
@include('include.footer')
@stop
@section('scripts')
<script>
$('#continue').click(function() {
  $('#second').show();
  $('#theme').hide();
  $('#first').hide();
  $('#third').hide();
});
$('#getweb').click(function() {
$('#first').hide();
 $('#theme').hide();
$('#second').hide();
$('#third').show();
});
$('#getwebs').click(function() {
$('#first').hide();
 $('#theme').hide();
$('#second').hide();
$('#third').show();
});
function choose()
{
$('#theme').hide();
$('#first').show();
$('#second').hide();
$('#third').hide();
}
function previous(id)
{
	if(id=='pre_reg')
	{
		$('#theme').show();
		$('#first').hide();
		$('#second').hide();
		$('#third').hide();
	}
	else if(id=='pre_package')
	{
		$('#theme').hide();
		$('#first').show();
		$('#second').hide();
		$('#third').hide();
	}
	else if(id=='pre_checkout')
	{
		$('#theme').hide();
		$('#first').hide();
		$('#second').show();
		$('#third').hide();
	}
}
</script>
@stop