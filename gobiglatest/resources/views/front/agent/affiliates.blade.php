<div class="main-div">
@include('include.header')


<div class="container m-t-40 min-height450">
<div class="panel">
      <div class="panel-heading">
        @if($aff_results!='')
        <table class="checkout-table visitor-table">
          <thead>
            <tr class="bg-black">
              <th>Affiliate Name</th>
              <th>Affiliate Email</th>
              <th>Affiliate Website URL</th>
			  <th>Total Payment</th>
            </tr>
          </thead>
          @foreach ($aff_results as $result)
          <tbody>
            <tr>
              <td data-label="Total Clicks">{{ $result->username }}</td>
              <td data-label="Total Signups">{{ $result->email }}</td>
              <td data-label="Total Payment"><strong><a href="http://{{ $result->domain_name }}.users.gobignewsletter.stagemyapp.com" target="_blank">{{ $result->domain_name }}.users.gobignewsletter.stagemyapp.com</a></strong></td>
			  @if($result->payment != '')
				<td>${{ $result->payment }}</td>
			  @else
				  <td>$0</td>
			  @endif
				
            </tr>
          </tbody>
          @endforeach
        </table>
        @else
                <h2 class="m-t-30 text-center">There are no affiliates.</h2> 
        @endif  
      </div>
    </div>
  </div>
@include('include.footer')
</div>
