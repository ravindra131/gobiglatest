<div class="main-div">
@include('include.header')


    {!! HTML::style('css/parsley.css') !!}
	


@if(session('statut') == 'user' || session('statut') === 'redac')
{!! Form::open(['url' => '/editpassword', 'class' => 'form-horizontal', 'method' => 'post', 'data-parsley-validate']) !!}
<input type="hidden" name="userid" value="{{ $id }}">
<div class="container m-t-40">
<div class="green-heading text-center"><h3>change password<span class="icon-cross-headiing"><img src="/images/grey-icon-heading.png"><span></h3>
</div>	
<div class="row m-t-20">
<div class="col-md-8 col-md-offset-2">

<div class="bg-light-form ">
	@if ($errors->has())
    	<div class="alert alert-danger">
          	@foreach ($errors->all() as $error)
            	{{ $error }}<br>
          	@endforeach
    	</div>
    @endif
@if (Session::has('message'))
    <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">New Password(*)</label>
<div class="col-md-9 col-sm-8">
{!! Form::password('password', [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'password',
            'data-parsley-required-message' => 'Please enter password.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-minlength'        => '6',
            'data-parsley-minlength-message'=> 'Please enter password of minimum length 6.'
        ]) !!}
</div>
</div>
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Confirm Password(*)</label>
<div class="col-md-9 col-sm-8">
	{!! Form::password('cpassword', [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'cpassword',
            'data-parsley-required-message' => 'Please enter confirm password.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-equalto'			=> '#password',
            'data-parsley-equalto-message'  => 'Password and Confirm password not match.'
        ]) !!}
</div>
</div>
</div>
</div></div>
<p class="m-t-30 text-center"><input type="submit" Value="Save" class="login_btn fs-22"></p>
</div>
{!! Form::close() !!}
@endif
@include('include.footer')
</div>

<script>
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
        };
</script>
{!! HTML::script('/js/parsley.min.js') !!}
