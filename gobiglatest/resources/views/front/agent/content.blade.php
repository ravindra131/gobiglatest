<div class="main-div">
@include('include.header')

    {!! HTML::style('css/parsley.css') !!}
	 


@if(session('statut') == 'user' || session('statut') === 'redac')
{!! Form::open(['url' => '/addcontent', 'class' => 'form-horizontal', 'method' => 'post','files' => true, 'data-parsley-validate']) !!}
<input type="hidden" name="userid" value="{{ auth()->user()->id }}">
<input type="hidden" name="userdomain" value="{{ auth()->user()->domain_name }}">
<input type="hidden" name="domain" value="{{ $domain }}">
<input type="hidden" name="themeid" value="{{ auth()->user()->theme_id }}">
<input type="hidden" name="contentid" value="{{ $id }}">
<div class="container m-t-40">
@if (Session::has('message'))
    <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
@if(session()->has('ok'))
            @include('partials/error', ['type' => 'success', 'message' => session('ok')])
        @endif
@if($id == '')
<div class="green-heading text-center"><h3>one more step!<span class="icon-cross-headiing"><img src="/images/grey-icon-heading.png"><span></h3>
</div>
<p class="fs-18 text-center">Please fill out this form as you would like it to appear on your website. All of your information will auto­populate on your website and all corresponding emails that are sent on your behalf.</p> 
@else 
<div class="green-heading text-center"><h3>content form<span class="icon-cross-headiing"><img src="/images/grey-icon-heading.png"><span></h3>
</div>
@endif
<div class="row m-t-20">
<div class="col-md-8 col-md-offset-2"> 
<div class="bg-light-form ">
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Title(*)</label>
<div class="col-md-9 col-sm-8">
    {!! Form::text('companyname', $companyname, [
            'class'     =>  'form-control',
            'required',
             'id'       => 'inputCompanyname',
            'data-parsley-required-message' => 'Please enter company name.',
            'data-parsley-trigger' => 'change focusout'
    ]) !!}
     @if ($errors->has('companyname'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('companyname') }}</div>
    @endif
</div>
</div>
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Youtube URL</label>
<div class="col-md-9 col-sm-8">
    {!! Form::text('youtubeurl', $youtubeurl, [
            'class'     =>  'form-control',           
            'id'       => 'inputYoutubeurl',
            'data-parsley-required-message' => 'Please enter youtube url.',
            'data-parsley-trigger' => 'change focusout',
			'data-parsley-pattern'          => '/^(https?:\/\/)?(www\.)?youtube.com\//',
            'data-parsley-pattern-message'  =>  'Please enter a valid youtube url.'
    ]) !!}
    <em>(Our social links will be the default. You can enter your own if you wish)</em>
     @if ($errors->has('youtubeurl'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('youtubeurl') }}</div>
    @endif
</div>
</div>
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Linkedin URL</label>
<div class="col-md-9 col-sm-8">
    {!! Form::text('linkedinurl', $linkedinurl, [
            'class'     =>  'form-control',           
            'id'       => 'inputLinkedinurl',
            'data-parsley-required-message' => 'Please enter linkedin url.',
            'data-parsley-trigger' => 'change focusout',
			'data-parsley-pattern'          => '/^(https?:\/\/)?(www\.)?linkedin.com\//',
            'data-parsley-pattern-message'  =>  'Please enter a valid linkedin url.'
    ]) !!}
    <em>(Our social links will be the default. You can enter your own if you wish)</em>
     @if ($errors->has('linkedinurl'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('linkedinurl') }}</div>
    @endif
</div>
</div>
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Facebook URL</label>
<div class="col-md-9 col-sm-8">
    {!! Form::text('facebookurl', $facebookurl, [
            'class'     =>  'form-control',           
            'id'       => 'inputFacebookurl',
            'data-parsley-required-message' => 'Please enter facebook url.',
            'data-parsley-trigger' => 'change focusout',
			'data-parsley-pattern'          => '/^(https?:\/\/)?(www\.)?facebook.com\//',
            'data-parsley-pattern-message'  =>  'Please enter a valid facebook url.'
    ]) !!}
    <em>(Our social links will be the default. You can enter your own if you wish)</em>
     @if ($errors->has('facebookurl'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('facebookurl') }}</div>
    @endif
</div>
</div>
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Twitter URL</label>
<div class="col-md-9 col-sm-8">
    {!! Form::text('twitterurl', $twitterurl, [
            'class'     =>  'form-control',            
            'id'       => 'inputTwitterurl',
            'data-parsley-required-message' => 'Please enter twitter url.',
            'data-parsley-trigger' => 'change focusout',
			'data-parsley-pattern'          => '/^(https?:\/\/)?(www\.)?twitter.com\//',
            'data-parsley-pattern-message'  =>  'Please enter a valid twitter url.'
    ]) !!}
    <em>(Our social links will be the default. You can enter your own if you wish)</em>
     @if ($errors->has('twitterurl'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('twitterurl') }}</div>
    @endif
</div>
</div>
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Google Plus URL</label>
<div class="col-md-9 col-sm-8">
    {!! Form::text('googleurl', $googleurl, [
            'class'     =>  'form-control',           
            'id'       => 'inputgoogleurl',
            'data-parsley-required-message' => 'Please enter google plus url.',
            'data-parsley-trigger' => 'change focusout',
			'data-parsley-pattern'          => '/^(https?:\/\/)?(www\.)?plus.google.com\//',
            'data-parsley-pattern-message'  =>  'Please enter a valid googleplus url.'
    ]) !!}
    <em>(Our social links will be the default. You can enter your own if you wish)</em>
     @if ($errors->has('googleurl'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('googleurl') }}</div>
    @endif
</div>
</div>
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Instagram URL</label>
<div class="col-md-9 col-sm-8">
    {!! Form::text('instaurl', $instaurl, [
            'class'     =>  'form-control',           
            'id'       => 'inputInstaurl',
            'data-parsley-required-message' => 'Please enter instagram url.',
            'data-parsley-trigger' => 'change focusout',
			'data-parsley-pattern'          => '/^(https?:\/\/)?(www\.)?instagram.com\//',
            'data-parsley-pattern-message'  =>  'Please enter a valid instagram url.'
    ]) !!}
    <em>(Our social links will be the default. You can enter your own if you wish)</em>
     @if ($errors->has('instaurl'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('instaurl') }}</div>
    @endif
</div>
</div>
<div class="form-group">
    <label class="col-md-3 col-sm-4 control-label">Logo</label>
    <div class="col-md-9 col-sm-8">
    <input type="file" name="logo" id="logo" class="form-control" onchange="readURL(this);">
    <div class="alert alert-danger parsley" role="alert" id="im_error" style="display:none;">Please enter a valid image.</div>
     @if($logo !='')
    <div class="logo-file-show m-t-10">
    <img id="blah" src="{{ $logo }}" class="img-thumbnail"/>
        </div>
        <a href="{!! url('/remove_content_logo', array('agentid' => auth()->user()->id)) !!}" id="remove_logo" class="remove-logo">Remove Logo</a>
     @else
     <em>(Please upload your logo or Accept ours as default)</em>
     <div class="logo-file-show">
    <img id="blah"/>
</div>
    @endif
</div>
</div>
<div class="form-group">
    <label class="col-md-3 col-sm-4 control-label">Image</label>
    <div class="col-md-9 col-sm-8">
	<div class="row m-l-0 m-t-15 m-r-0">
    <input type="radio" name="sliderimage" id="sliderimage" value="1" class="m-t-50 pull-left" <?php if($slider1=="1") echo 'checked="true"'; ?>>
	<div class="img-shows"><img src="{{ asset('/') }}images/banner_img.png" class="img-thumbnail"/>
    </div></div>
	<div class="row m-l-0 m-t-15 m-r-0">
    <input type="radio" name="sliderimage" id="sliderimage" value="2" class="pull-left m-t-50" <?php if($slider1=="2") echo 'checked="true"'; ?>>
	<div class="img-shows"><img src="{{ asset('/') }}images/banner_img2.png" class="img-thumbnail"/>
    </div>
    </div>
	<div class="row m-l-0 m-t-15 m-r-0">
    <input type="radio" name="sliderimage" id="sliderimage" value="3" class="pull-left m-t-50" <?php if($slider1=="3") echo 'checked="true"'; ?>>
	<div class="img-shows"><img  src="{{ asset('/') }}images/banner_img3.png" class="img-thumbnail"/>
    </div>
    </div>
	
	  
</div>
</div>





</div>
</div></div>
<p class="m-t-30 text-center"><input type="submit" Value="Submit" class="login_btn fs-22"></p>
</div>
{!! Form::close() !!}
@endif
@include('include.footer')
</div>

<script>
var $j = jQuery.noConflict();
$j(document).ready(function(){

	$j('#inputPhone').mask('(000) 000-0000');
	
});
$j(document).ready(function(){
    
    $j('#Phone').mask('(000) 000-0000');
    
});
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="text-danger parsley" role="alert"></div>'
        };
function readURL(input)
{
    if (input.files && input.files[0]) 
    {
        if(input.files[0].type == 'image/jpeg' || input.files[0].type == 'image/jpg' || input.files[0].type == 'image/png')
        {
             $j('#im_error').hide();
            var reader = new FileReader();
            reader.onload = function (e) 
            {
                $j('#blah')
                .attr('src', e.target.result);
                $j( "#blah" ).addClass( "img-thumbnail" );
            };
            reader.readAsDataURL(input.files[0]);
        }
        else
        {
            $j('#logo').val('');
            $j('#im_error').show();
        }
    }
}


//function to remove logo
$j(document).ready(function(){
    $j("#remove_logo").click(function(e){
        if(!confirm('Are you sure to remove logo?')){
            e.preventDefault();
            return false;
        }
        return true;
    });
});   

</script>

{!! HTML::script('/js/parsley.min.js') !!}
{!! HTML::script('/js/jquery.mask.js') !!}



