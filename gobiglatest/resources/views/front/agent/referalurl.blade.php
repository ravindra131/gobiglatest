<div class="main-div">
@include('include.header')


<div class=" container m-t-40">
<h2 class="text-black">Affiliate / Referral Marketing Resource:</h2>
<h3 class="text-black"><i>Get paid for sharing!</i></h3>
<p class="fs-18 align-left">We have supplied you with the perfect Graphics and Links to promote your Affiliate business.</p>
<p class="fs-18 align-left">All you have to do is copy and paste. Don’t worry, your affiliate link is already built into the code.</p>
<p class="fs-18 align-left">You can place these on your blog or website or any other place you’d like to share. If you want, you can place the graphics on Facebook, Twitter and all social media outlets.</p>
<p class="fs-18 about-us">You can even use the “Referral URL” we’ve conveniently supplied. If you have any questions, please reach out to <a href="mailto:help@gobigprinting.com">help@gobignewsletter.com</a>. We’d be happy to assist.</p>
</div>
<div class="container m-t-40">
<div class="row m-t-20 form-horizontal">
<div class="col-md-8 col-md-offset-2">
<div class="bg-light-form">
<div class="form-group">
<label class="col-md-3 control-label">Referral URL</label>
<div class="col-md-9">
	<input type="text" id="website" value="{{ $referal_url }}" readonly class="form-control">
</div>
</div>
</div>
</div>
</div>
<p class="m-t-30 text-center"><button data-copytarget="#website" class="gowib-btn-all fs-22">Copy</button></p>
</div>
@include('include.footer')
</div>

<script type="text/javascript">
(function() {

  'use strict';

  // click events
  document.body.addEventListener('click', copy, true);

  // event handler
  function copy(e) {

    // find target element
    var
      t = e.target,
      c = t.dataset.copytarget,
      inp = (c ? document.querySelector(c) : null);

    // is element selectable?
    if (inp && inp.select) {
      // select text
      inp.select();

      try {
        // copy text
        document.execCommand('copy');
        inp.blur();
        alert('URL copied.');
      }
      catch (err) {
        alert('please press Ctrl/Cmd+C to copy');
      }

    }

  }

})();
</script>


