<div class="main-div">
@include('include.header')

    {!! HTML::style('css/parsley.css') !!}
	


@if(session('statut') == 'user' || session('statut') === 'redac')
{!! Form::open(['url' => '/editprofile', 'class' => 'form-horizontal', 'method' => 'post', 'data-parsley-validate']) !!}
<input type="hidden" name="userid" value="{{ auth()->user()->id }}">
<div class="container m-t-40">
<div class="green-heading text-center"><h3>my profile<span class="icon-cross-headiing"><img src="/images/grey-icon-heading.png"><span></h3>
</div>	
<div class="row m-t-20">
<div class="col-md-8 col-md-offset-2">

<div class="bg-light-form ">
@if (Session::has('message'))
    <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Full Name(*)</label>
<div class="col-md-9 col-sm-8">
{!! Form::text('username', $user_info[0]->username, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'username',
            'data-parsley-required-message' => 'Please enter full name.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-pattern'          => '/^[a-zA-Z\s]*$/',
            'data-parsley-pattern-message'  =>  'Please enter a valid name.'
        ]) !!}
</div>
</div>
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Address(*)</label>
<div class="col-md-9 col-sm-8">
{!! Form::text('address', $user_info[0]->address, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'address',
            'data-parsley-required-message' => 'Please enter address.',
            'data-parsley-trigger'          => 'change focusout'
        ]) !!}
</div>
</div>
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Email address(*)</label>
<div class="col-md-9 col-sm-8">
	{!! Form::email('email', $user_info[0]->email, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'email',
            'data-parsley-required-message' => 'Please enter e-mail address.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-type'             => 'email',
            'data-parsley-type-message'     => 'Please enter a valid e-mail address.'
        ]) !!}
         @if ($errors->has('email'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('email', 'This e-mail address already exists.') }}</div>
        @endif
</div>
</div>
@if(session('statut') === 'user')
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Domain Name(*)</label>
<div class="col-md-9 col-sm-8">
	{!! Form::text('domain_name', $user_info[0]->domain_name, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'domain_name',
            'data-parsley-required-message' => 'Please enter domain name.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-pattern'          => '/^[a-zA-Z0-9]*$/',
            'data-parsley-pattern-message'  =>  'Please enter a valid domain name.'
        ]) !!}
        <p class="help-block"> http://<span id="showDomainName"></span>.users.gobignewsletter.stagemyapp.com</p>
         @if ($errors->has('domain_name'))
            <div class="alert alert-danger parsley" role="alert">{{ $errors->first('domain_name', 'This domain name already exists.') }}</div>
        @endif
</div>
</div>
@endif
</div>
</div></div>
<p class="m-t-30 text-center"><input type="submit" Value="Save" class="login_btn fs-22"></p>
</div>
{!! Form::close() !!}
@endif
@include('include.footer')
</div>

<script>
$('#domain_name').keyup(function(){
    var value = $('#domain_name').val();
    $("#showDomainName").text(value);
});
$( document ).ready(function() {
    var value = $('#domain_name').val();
    $("#showDomainName").text(value);
});
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
        };
</script>
{!! HTML::script('/js/parsley.min.js') !!}


