<div class="main-div">
@include('include.header')


    {!! HTML::style('css/parsley.css') !!}

@section('main')

<section class="form-horizontal min-height450">
<div class="container m-t-40">
@if(session('statut') == 'user')
  @if($userconfirmed == 0)
  <div class="alert alert-danger">
    You must verify your email. If you have not received the verification email, check your spam folder. <a href="{!! url('auth/resend', array('user_id' => auth()->user()->id)) !!}" style="text-decoration:underline;">Resend Verification Email</a>
  </div>
  @endif
<div class="green-heading text-center"><h3>Hello {{ auth()->user()->username }}<span class="icon-cross-headiing"><img src="/images/grey-icon-heading.png"><span></h3>
</div>
@if($subdomain_name != '' && auth()->user()->domain_status != 0)
<div class="row m-t-20">
<div class="col-sm-6 m-b-20">
<div class="bg-light-form min-height300 bg-light-box">
<div class="form-group">
  @if (Session::has('message'))
    <div class="alert alert-success m-b-5">{{ Session::get('message') }}</div>
@endif
<label class="col-md-12 col-sm-12 text-black fs-20  font-normal">Your website domain is</label>
<div class="col-md-12 col-sm-12">
<label class="control-label">	<a class="fs-18" href="http://{{ $subdomain_name }}" target="_blank">{{ $subdomain_name }}</a></label>
</div>
@if(count($paypal_profile_status))
<div class="col-md-12 col-sm-12">
<label class="control-label"> Selected Package - @if($package == '1')<b>Monthly</b>@elseif($package == '2')<b>Annual</b>@endif </label> <br />
<label class="control-label"> Paypal Profile ID - <b>{{ $paypal_profile_status['PROFILEID'] }} </b> </label> <br />
<label class="control-label">	Paypal Profile Status - <b> {{ $paypal_profile_status['STATUS'] }} </b></label> <br />
	<a href="#" id="cancelSubscription" class="cancel-btn m-t-10"> Cancel Subscription </a> 
</div>
@endif
<!--div class="col-md-3 col-sm-4">
	<p class="text-center"><button data-copytarget="#domain" class="gowib-btn-all fs-22">Copy</button></p>
</div-->
</div>
</div>
</div>

<div class="col-sm-6 m-b-20">
<div class="bg-light-form min-height300 bg-light-box">
  @if (Session::has('messages'))
    <div class="alert alert-success">{{ Session::get('messages') }}</div>
@endif
   @if ($errors->has('agent_domain'))
            <div class="alert alert-danger">{{ $errors->first('agent_domain', 'This domain already exists. Please enter another one.') }}</div>
        @endif
{!! Form::open(['url' => '/adddomainname', 'class' => 'form-horizontal', 'method' => 'post', 'data-parsley-validate']) !!}
@if($domainname == '')
<div class="form-group" id="checkdiv">
<div class="text-center check-style">
<p class="no-margin">
<input id="owndomain" name="owndomain" type="checkbox" value="1" onclick="ShowHideDiv(this)" />
<label for="owndomain" class="text-black fs-20">Do you want to create your website on your own domain?<label>
</p>
<p class="fs-18">You will have to add A2 record on your domain hosting. Point this URL to 107.180.39.243</p>
<p class="fs-18"><a class="fs-18" href="https://www.godaddy.com/help/change-an-a-record-19239" target="_blank">Click here to see how</a></p>
</div>
</div>
@else
<input type="hidden" name="owndomain" value="1">
@endif
<div id="own_domain" style="display:none">
<div class="form-group">
<input type="hidden" name="agentid" id="agentid" value="{{ auth()->user()->id }}">
<label class="col-md-12 col-sm-12 fs-17 font-normal text-black">Domain name</label>
<div class="col-md-12 col-sm-12">
	{!! Form::text('domain_name', $domainname, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'inputDomainname',
            'data-parsley-required-message' => 'Please enter domain name.',
            'data-parsley-trigger'          => 'change focusout',
			      'data-parsley-pattern'          => '/^[a-zA-Z0-9]+([a-zA-Z0-9-.]+)?.(com|org|net|mil|edu|de|COM|ORG|NET|MIL|EDU|DE|cf)$/',
            'data-parsley-pattern-message'  =>  'Please enter a valid domain name.'
        ]) !!}	

</div>
</div>
@if($domainname == '')
<p class="m-t-10 text-right"><input type="submit" Value="Submit" class="gowib-btn-all fs-16"></p>
@else
<p class="m-t-10 text-right">
  <a href="#" onclick="canceleditview()" class="m-r-10">Cancel</a>
  <input type="submit" Value="Save" class="gowib-btn-all fs-16">
</p>
@endif
</div>
{!! Form::close() !!}
<div id="perdomain" style="display:none;">
  <label class="col-md-12 col-sm-12 text-black fs-20  font-normal">Your domain is</label>
<div class="col-md-12 col-sm-12">
<label class="control-label"><a class="fs-18" href="http://{{ $domainname }}" target="_blank">http://{{ $domainname }}</a></label>
<label class="control-label" style="float:right;"><a class=" fs-18 m-r-10" href="#" onclick="showeditview()">Edit</a>
<a class="gowib-btn-all fs-18" id="removedomain" href="{!! url('/remove_agent_domain', array('agentid' => auth()->user()->id)) !!}" style="background-color:#ec3023;">Remove</a></label>
</div>
  </div>
</div>
</div></div>
@elseif($subdomain_name == '')
<p class="m-t-30 text-center fs-20 please-fill">Please fill <a href="{!! url('/content', array('user_id' => auth()->user()->id)) !!}">My Content</a> form to access your website.</p>
@elseif(auth()->user()->domain_status == 0)
<p class="m-t-30 text-center fs-20 please-fill">Now please do <a href="{!! url('/getpackages', array('user_id' => auth()->user()->id)) !!}">Payment</a> to access your website.</p>
@endif
@elseif(session('statut') === 'redac')
<div class="green-heading text-center"><h3>Hello {{ auth()->user()->username }}<span class="icon-cross-headiing"><img src="/images/grey-icon-heading.png"><span></h3>
</div>
@endif
</div>
</section>
@include('include.footer')

</div>
<script type="text/javascript">
(function() {

  'use strict';

  // click events
  document.body.addEventListener('click', copy, true);

  // event handler
  function copy(e) {

    // find target element
    var
      t = e.target,
      c = t.dataset.copytarget,
      inp = (c ? document.querySelector(c) : null);

    // is element selectable?
    if (inp && inp.select) {
      // select text
      inp.select();

      try {
        // copy text
        document.execCommand('copy');
        inp.blur();
        alert('URL copied.');
      }
      catch (err) {
        alert('please press Ctrl/Cmd+C to copy');
      }

    }

  }

})();
	
	 function ShowHideDiv(chkDomain) {
        var dvDomain = document.getElementById("own_domain");
        dvDomain.style.display = chkDomain.checked ? "block" : "none";
		//$("#inputDomainname").val('');
    }		
    $(document).ready(function(){
		
		$('#cancelSubscription').on('click',function(){
			
			var r = confirm('This is irreversible process. It can not be undone.');
			if(r == true)
			{
				window.location.href = "{{ url('/cancel-subscription/') }}";
			}
			
		});
    var dom_name = '{{ $domainname }}';
    if(dom_name !='')
    {
      $('#perdomain').show();
      $('#own_domain').hide();
    }
    else
    {
      $('#perdomain').hide();
      $('#own_domain').hide();
    }
    });
    function showeditview()
    {
      $('#perdomain').hide();
      $('#own_domain').show();
    }
    function canceleditview()
    {
      $('#perdomain').show();
      $('#own_domain').hide();
    }
	window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
        };
        $(document).ready(function(){
    $("#removedomain").click(function(e){
        if(!confirm('Are you sure to remove this?')){
            e.preventDefault();
            return false;
        }
        return true;
    });
});		
	</script>
{!! HTML::script('/js/parsley.min.js') !!}




