<div id="newheader">
@include('include.header')
</div>
<div class=" container m-t-40">
<span class="fs-20 align-left">BONUS:</span><br/>
<h3 class="text-black" style="display:inline-block;">Business Cards:</h3>
<p class="fs-20 align-left" style="display:inline-block;">Get 1000 Business Cards to show off your new site at 50% off: Use promo code: GoBigNewsletter</p><br/>
<p class="fs-20 align-left about-us" style="display:inline-block;"><a href="http://www.gobigyellowletter.com/business-cards" target="_blank">Click here</a></p><br/>
<h3 class="text-black" style="display:inline-block;">Logos:</h3>
<p class="fs-20 align-left about-us" style="display:inline-block;">Get a logo for your company. <a href="http://www.gobigyellowletter.com/logos/" target="_blank">Click here</a></p>
</div>
<div class="container m-t-40">
	<div class="row">
		                                <div class="col-md-12">
                                    <!-- Nav tabs --><div class="banner-tabs">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#BlueBanner" aria-controls="BlueBanner" role="tab" data-toggle="tab" class="blue-banner">Blue Banner</a></li>
                                        <li role="presentation"><a href="#GreenBanner" aria-controls="GreenBanner" role="tab" data-toggle="tab" class="green-banner">Green Banner</a></li>
                                        <li role="presentation"><a href="#silverBanner" aria-controls="silverBanner" role="tab" data-toggle="tab" class="silver-banner">Silver Banner</a></li>
                                       
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="BlueBanner">
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/blue/GoBig-Printing-blue-120x240.jpg') }}" alt="banner"> 
										<figcaption>Size 120x240</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/blue/GoBig-Printing-blue-120x240.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/blue/GoBig-Printing-blue-120x600.jpg') }}" alt="banner"> 
										<figcaption>Size 120x600</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/blue/GoBig-Printing-blue-120x600.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/blue/GoBig-Printing-blue-125x125.jpg') }}" alt="banner"> 
										<figcaption>Size 125x125</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/blue/GoBig-Printing-blue-125x125.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/blue/GoBig-Printing-blue-160x600.jpg') }}" alt="banner"> 
										<figcaption>Size 160x600</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/blue/GoBig-Printing-blue-160x600.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/blue/GoBig-Printing-blue-250x250.jpg') }}" alt="banner"> 
										<figcaption>Size 250x250</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/blue/GoBig-Printing-blue-250x250.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/blue/GoBig-Printing-blue-300x250.jpg') }}" alt="banner"> 
										<figcaption>Size 300x250</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/blue/GoBig-Printing-blue-300x250.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/blue/GoBig-Printing-blue-468x60.jpg') }}" alt="banner"> 
										<figcaption>Size 468x60</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/blue/GoBig-Printing-blue-468x60.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/blue/GoBig-Printing-blue-728x90.jpg') }}" alt="banner"> 
										<figcaption>Size 728x90</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/blue/GoBig-Printing-blue-728x90.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										 </div><!--#end Blue Banner-->
                                        <div role="tabpanel" class="tab-pane" id="GreenBanner">
                                        	<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/green/GoBig-Printing-green-120x240.jpg') }}" alt="banner"> 
										<figcaption>Size 120x240</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/green/GoBig-Printing-green-120x240.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/green/GoBig-Printing-green-120x600.jpg') }}" alt="banner"> 
										<figcaption>Size 120x600</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/green/GoBig-Printing-green-120x600.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/green/GoBig-Printing-green-125x125.jpg') }}" alt="banner"> 
										<figcaption>Size 125x125</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/green/GoBig-Printing-green-125x125.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/green/GoBig-Printing-green-160x600.jpg') }}" alt="banner"> 
										<figcaption>Size 160x600</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/green/GoBig-Printing-green-160x600.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/green/GoBig-Printing-green-250x250.jpg') }}" alt="banner"> 
										<figcaption>Size 250x250</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/green/GoBig-Printing-green-250x250.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/green/GoBig-Printing-green-300x250.jpg') }}" alt="banner"> 
										<figcaption>Size 300x250</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/green/GoBig-Printing-green-300x250.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/green/GoBig-Printing-green-468x60.jpg') }}" alt="banner"> 
										<figcaption>Size 468x60</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/green/GoBig-Printing-green-468x60.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/green/GoBig-Printing-green-728x90.jpg') }}" alt="banner"> 
										<figcaption>Size 728x90</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/green/GoBig-Printing-green-728x90.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
                                        </div><!--#end Green Banner-->
                                        <div role="tabpanel" class="tab-pane" id="silverBanner">
                                        	<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/silver/GoBig-Printing-silver-120x240.jpg') }}" alt="banner"> 
										<figcaption>Size 120x240</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/silver/GoBig-Printing-silver-120x240.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/silver/GoBig-Printing-silver-120x600.jpg') }}" alt="banner"> 
										<figcaption>Size 120x600</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/silver/GoBig-Printing-silver-120x600.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/silver/GoBig-Printing-silver-125x125.jpg') }}" alt="banner"> 
										<figcaption>Size 125x125</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/silver/GoBig-Printing-silver-125x125.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/silver/GoBig-Printing-silver-160x600.jpg') }}" alt="banner"> 
										<figcaption>Size 160x600</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/silver/GoBig-Printing-silver-160x600.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/silver/GoBig-Printing-silver-250x250.jpg') }}" alt="banner"> 
										<figcaption>Size 250x250</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/silver/GoBig-Printing-silver-250x250.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/silver/GoBig-Printing-silver-300x250.jpg') }}" alt="banner"> 
										<figcaption>Size 300x250</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/silver/GoBig-Printing-silver-300x250.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/silver/GoBig-Printing-silver-468x60.jpg') }}" alt="banner"> 
										<figcaption>Size 468x60</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/silver/GoBig-Printing-silver-468x60.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										<div class="row">
										<div class="col-xs-4 col-md-3"><figure class="banners-div"><img src="{{ asset('/images/silver/GoBig-Printing-silver-728x90.jpg') }}" alt="banner"> 
										<figcaption>Size 728x90</figcaption>
										</figure></div>
										<div class="col-xs-8 col-md-9">
										<div class="banner-code">
										&lt;div&gt; &lt;a href="{{ $referal_url }}" target="_blank"&gt; &lt;img src="{{ asset('/images/silver/GoBig-Printing-silver-728x90.jpg') }}" alt="banner"&gt; &lt;/a&gt;										
										&lt;/div&gt; 
										</div>
										</div>
										</div>
										</div><!--#end silver Banner-->
                                                </div>
</div>
                                </div>
	</div>
</div>






@include('include.footer')

