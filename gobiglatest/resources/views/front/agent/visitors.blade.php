<div class="main-div">
@include('include.header')

<meta name="csrf-token" content="{{ csrf_token() }}" />

@if(session('statut') == 'user' || session('statut') === 'redac')
	<div class="container m-t-40 min-height450">
			@if($visitor_result!='')
			<table class="checkout-table visitor-table" cellspacing="0" cellpadding="0">
					<thead>
						<tr class="bg-black">
							
							<th>Name</th>
							<th>Email</th>
							<th class="width-30perst">Notes</th>
							<th>Action</th>
						</tr>
					</thead>
					@foreach ($visitor_result as $message)
					<tbody>
						<tr>
							<td data-label="Name">{{ $message->firstname }}</td>
							<td data-label="Email">{{ $message->email }}</td>
							<td data-label="Notes" id="vinot{{ $message->id }}">{{ $message->notes }} &nbsp;</td>
							<td data-label="Notes" id="vnote{{ $message->id }}" style="display:none;">
							
								<form id="noteForm{{ $message->id }}" style="display: none;">
								<input type="hidden" name="_token" value="{{ csrf_token() }}" />
								<input type="hidden" name="visitorid" id="visitorid" value="{{ $message->id }}">
         						<textarea rows="2" id="note{{ $message->id }}" name="note" value=""/>{{ $message->notes }}</textarea>
         						<div class="visitor-btn-row">
								<a href="#" onclick="cancel({{ $message->id }})" class="">Cancel</a>
								<input id="submit" type="button" value="Save" class="gowib-btn" onclick="submitnotes({{ $message->id }}, event);">
         						
								</div>
							</form>
							</td>
							@if($message->notes == '')
							<td data-label="Action"><a href="#" id="{{ $message->id }}" onclick="addnotes(this.id)" class="gowib-btn">Add notes</a></td>
							@else
							<td data-label="Action"><a href="#" id="{{ $message->id }}" onclick="addnotes(this.id)" class="gowib-btn">Edit notes</a></td>
							@endif
						</tr>
					</tbody>
					@endforeach
				</table>
				@else
				<h2 style="text-align: center;">There are no visitors.</h2> 
				@endif		
				
			</div>
		
@endif
@include('include.footer')
</div>

<script>
function addnotes(id)
{
	$('#vinot'+id).hide();
	$('#vnote'+id).show();
	$('#noteForm'+id).show();
}
function cancel(id)
{
	$('#vnote'+id).hide();
	$('#noteForm'+id).hide();
	$('#vinot'+id).show();
}

function submitnotes(id,e)
{
	e.preventDefault();
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

var note = $("#note"+id).val();
if (note == '') {
alert("Please enter notes....!!");
} else 
{
// Returns successful data submission message when the entered information is stored in database.
$.post("/notes", {
vid: id,
notes: note
}, function(data) {
	//console.log(data);
$('#vnote'+id).hide();
$('#noteForm'+id).hide();
$('#vinot'+id).show(); 
$('#vinot'+id).html(data);
});
}
}
</script>



