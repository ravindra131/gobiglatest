@extends('include.header')
@section('main')
<section class="all-pagesbg">
<h1>Affiliate Program</h1>
<div class="pagesoverlay"></div>
</section>
<div class="container m-t-40">
<div class="green-heading  about-us" id="scrollbox3">
<h4>GoBig Affiliate Program</h4>
<h4 class="m-t-20">Calling all Real Estate Coaches, Mentors, Gurus, Investors, Brokers, Agencies.<br>We want you to look like a hero!</h4>
<p class="m-t-20">How many times have you shared your opinion of a great movie, fantastic restaurant, great realtor, plumber, auto guy, or clothing store you just love? I bet you do it several times a day! You have to know that your opinion counts... and because of your recommendations the business you recommended got more customers... right?<br>
Wouldn’t it be cool if you got a piece of that? I mean...why shouldn’t you? You’ve helped them grow their business...  and they don’t even know you!<br>
That’s sorta unfair don’t ya think? You should get something for your recommendation; or at least we think you should!</p>
<p class="m-t-10">In fact, we think so highly of you recommending our business that we want to give you a HUGE piece of it. 25% to be exact. Simply unreal commissions just for sharing something you are already doing.</p> 
<p class="m-t-10">So, here’s how it works. With our GoBig Affiliate Program called “Club GoBig” you get a whopping 25% of every purchase you send our way!</p>
<p class="m-t-10">How do you send them our way? That’s easy...You don’t! You send them YOUR WAY! You see, you get your very own personal website and you simply send them there!</p>
<p class="m-t-10">By ordering an Investor website you are automatically enrolled into our Affiliate Program. There are no additional forms or contracts to sign. You will automatically be assigned an Affiliate ID. This Affiliate ID will automatically be assigned to any banner-Ad or marketing material in your affiliate portal. Your Affiliate ID is active as long as you are actively paying for an Investor Website.</p>
<p class="m-t-10"><i>If you do a little research you’ll find that there are a lot of affiliate programs out there and that the average commission is a puney 4%-7% AND you only get a long confusing link to stick on your blog. Seems silly if you ask me. We think you are worth way more then that. What’s also cool about the personal website you get from us is that once someone visits you they are a customer for life! They can’t even delete the cookies to remove you. (too much technical stuff.. I know)</i><br>We also have banners and promotional items for your use to make it super easy to earn an affiliate commission!</p>
<h4 class="no-margin">Let’s sweeten the deal!</h4>
<p>We know how easy it is to share on FaceBook or send an email with your personal website...That’s simple!    We also know that people will stop you on the street and ask you about your business. When that happens, simply hand them a beautiful business card with your info! It’s a no brainer!<br>We’re going to give you 1000 business cards to hand out for only $35. That’s a 50% savings!</p>
<p class="m-t-10">We give all this away because we know it works, it takes ZERO effort and you don’t feel like you’re selling anything! Yuck! Who wants to sell anything anyways?</p>
<p class="m-t-10">We believe in you so much that we are giving you your very own Investor Website totally RISK FREE and a ton of business cards to share with everyone! There is... No Contract, No Monthly Sales Quota & No Inventory.</p>
<h4 class="no-margin">Here are a few good Reasons why you should join the “Club GoBig” affiliate program!</h4>
<p>1.Get 25% commission on every sale that comes through your website<br>
 2.Turn Key Investor Website Business<br> 
 3.Great tax write off for home based business<br> 
 4.Unlimited cookies. (send a shopper to your website once... and it remembers forever)<br> 
 5.Ongoing coupons, Daily Deals, and promotions to improve your conversion<br> 
 6.High average sale due to high-end niche market<br> 
 7.A dedicated affiliate team to make your program a success<br> 
 8.Hassle free tracking, just money in the bank.<br> 
 9.Risk FREE! Quit anytime!</p>
 <p>So, what are you waiting for? Isn’t it time for you to make a little extra money by simply sharing something you’re already doing & love anyways?</p>
 <p>
 	@if(session('statut') != 'user')
 	<a href="{{ url('auth/register') }}">Click here</a>
 	@else
 	<a href="{!! url('/referal-url', array('user_id' => auth()->user()->id )) !!}">Click here</a>
 	@endif
 	...&nbsp It’s your turn for payback</p>
 	<p><a href="{!! url('/affiliate-term') !!}">Affiliate Terms</a></p>
</div>
</div><!--@end about us-->
@include('include.footer')
@stop



