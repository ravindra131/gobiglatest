<div class="main-div">
@include('include.header')

<section class="all-pagesbg">
<h1>Testimonials</h1>
<div class="pagesoverlay"></div>
</section>
<div class="container m-t-30 green-heading  about-us">
<div class="text-center"><h3>Testimonials<span class="icon-cross-headiing"><img src="{{ asset('/images/grey-icon-heading.png') }}"><span></span></span></h3></div>
<div class="testimonail-wrap">
		      <p class="text-black">"We chose GoBig Printing because of their excellent customer service, quick turn around and intuitive web-based (Web2Print) application. One of the biggest benefits of using this mail center is that if you place your order before 8pm, it will be produced and delivered to USPS within 24 hours. USPS First Class turn around is usually within 4 days. As a result of such a quick turn around, you will get leads faster to jump start your business."


               <span><b>~ Sean Terry, Flip2Freedom</b>
			   </span>
               </p>
				<p class="text-black">"We tried creating our own newsletter each month. Not only was it a nightmare, it cost a lot of time and money. Thank goodness we found GoBig Newsletter. Not only are your prices exceptional, my newsletter goes out automatically each month for me."

AND... It’s pre-written so I can spend my time doing other things!


               <span><b> ~ Amy Jensen, CA</b></span></p>
				<p class="text-black">"Andy, I am very appreciative of the fact you bailed me out. I wanted to have my order for the Vegas Buying Summit but I waited too long to place the order giving you just 4 days to deliver. You never hesitated to do everything in your power to accommodate my needs while delivering a high quality product and showing complete flexibility to customize the order and all at a very reasonable price. Your approach to customer service is refreshing."
Thanks for a great job.

               <span><b> ~ Hernando Ramirez - New Horizons Real Estate Solutions </b></span>
                </p>
				<p class="text-black">"From creation to delivery, your program has been nothing but perfect. Not only have you given our members an easy to use system at buyers club pricing, you’ve made their marketing “EASY.” You made all of our current fulfillment challenges obsolete! Thanks again for providing stellar customer service for our Dean Graziosi & PMI Marketing members in the Real Estate Profession."

               <span><b> ~ Brandon Maughan, Dir. Bus Dev | PMI Marketing | Dean Graziosi</b></span>
               </p>
				<p class="text-black">“We tried creating our own website -Not only was it a nightmare, it cost a lot of time and money. Thank goodness we found <strong>GoBigWeb360</strong>. Not only are your prices exceptional, my website runs by itself. AND... It’s pre-written so I can spend my time doing other things!”
               <span><b> ~ Amy Jensen, CA</b></span>
               </p>
               <p class="text-black">“Go Big Printing is the online print solution I've been waiting for! I've tried other sites with results that ranged from so-so product to failure to launch and even simply being ripped off! Not here - Go Big has a quality, working site that has delivered my newsletter on-time, as ordered, at an exceptional price for the quality of the product.”
               <span><b>~ Jen Fredericks, SC</b></span>
               </p>
               <p class="text-black">“GoBig has answered everytime I called with a silly question. I feel like they are my very own personal assistant! If I don't see something online that I want, they'll create it for me. I just love the personal touch!
”
               <span><b>~ Laura Santiago, Ohio </b></span>
               </p>
               <p class="text-black">“I know there are lot's of places to print my marketing material, but not every place has what I need. GoBig has everything I need for my specific industry. I'm a home inspector so I use the newsletter to reach out to top real estate agents...
”
               <span><b> ~ Carl Ewings, Phoenix Az. </b></span>
               </p> 
               <p class="text-black">“	
I have to tell you, you really have an impressive program.   We were considering doing our periodic newsletter locally but not only are your prices very competitive,  you make it much easier to go thru the whole process.  Thanks so much!
”
               <span><b> ~ Sam Johnson</b></span>
               </p>
               <p class="text-black">“Through June, we've done 17 jobs from our newsletters for a total $4,366.00. We also gets tons of positive feedback from our customers on how much they love the newsletters and look forward to getting them each month. Personally, we think its an awesome way to keep your business name in front of the customer so that they can't forget who cleaned for them the last time!
”
               <span><b> ~ Beatrice Alexanders - ABC Cleaning Service</b></span>
               </p>
               <p class="text-black">“Thank you for providing a top quality Variable Web2Print Solution & excellent customer service for our agents in our Brokerage firm. From the "on-boarding" process to launch, everything was top notch.
”
               <span><b> ~ Cindy Landers</b></span>
               </p>
               <p class="text-black">“Andy from GoBig has been great! Easy to work with and always there to help. We’ve tried several other services but GoBig is by far the best. They’ve taken the hassle and headache out of getting our newsletter out! And, our newsletters look awesome! Thank you!
”
               <span><b>~ Andraya Petersen | All Pro Mobile-Clean</b></span>
               </p>
               <p class="text-black">“GoBig has been awesome and I'll definitely keep using the Print on Demand site for my marketing materials.  Your personal help getting me over the 'bumps' has been awesome.  I look forward to a long relationship with your company.”
”
               <span><b> ~ Dave Lightner, Fl</b></span>
               </p>
               <p class="text-black">“I have been working with Andy at GoBig for the last 8 years. He has been helpful and knowledgable on everything "Marketing." I wouldn't order my creative or marketing material any place else!
”
               <span><b> ~ Jenny J, NC</b></span>
               </p>
               
               
		     </div>
			</div>

<!--Footer start-->
@include('include.footer')

<!---#End Footer--->
</div>