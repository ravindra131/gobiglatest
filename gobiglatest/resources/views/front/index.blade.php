<div class="main-div">
@include('include.header')
<header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <!-- Wrapper for Slides -->
		@if($slider1_image == null && $slider1_image == '' )
        <div class="carousel-inner">
            <div class="item active">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/banner_img.png');"></div>
            </div>
            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/banner_img2.png');"></div>				
            </div>   
            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill" style="background-image:url('images/banner_img3.png');"></div>				
            </div> 
			
            </div>
        @elseif($slider1_image == '1')
		<div class="carousel-inner2">
		<div class="fill" style="background-image:url('images/banner_img.png');"></div>
		</div>
		@elseif($slider1_image == '2')
		<div class="carousel-inner2">
		<div class="fill" style="background-image:url('images/banner_img2.png');"></div>
		</div>
		@elseif($slider1_image == '3')
		<div class="carousel-inner">
		<div class="fill" style="background-image:url('images/banner_img3.png');"></div>
		</div>
		@endif
			
		
   
        <!-- /.container-fluid -->
<!--Start of banner_content div-->
			 <div class="banner_content">
			     <div class="content_wrap">
				    <div class="row no-margin">
					 <div class=" col-sm-6 col-md-6 col-lg-8 m-t-50">
					    <h1>life <strong>style</strong></h1>
						<span> Monthly Newsletter</span>
	<a href="{{ url('auth/getregister') }}"><input class="btn btn-default login_btn features_btn" type="submit" value="see the features"></a>
					 </div>
					 <div class=" col-sm-6 col-md-6 col-lg-4">
					    <div class=" referral_box">
					    <span>quadruple your referrals! </span>
						<p>Simple fill out the form below to receive your gift </p>
						@if (Session::has('emailer'))
                                        <div class="text-danger">{{ Session::get('emailer') }}</div>
                        @endif
						{!! Form::open(['url' => '/add-visitor', 'method' => 'post', 'id' => 'vform']) !!}
						<input type="hidden" name="memberid" value="{{ $member_id }}">
						  <div class="form-group">
							<label for="exampleInputName">First Name:</label>
							<input type="text" name="firstname" class="form-control">
							@if ($errors->has('firstname'))
                                <div class="text-danger parsley" role="alert">{{ $errors->first('firstname') }}</div>
                            @endif
						  </div>
						  <div class="form-group">
							<label for="exampleInputEmail">Email:</label>
							<input type="text" name="email" class="form-control">
							@if ($errors->has('email'))
                                <div class="text-danger parsley" role="alert">{{ $errors->first('email') }}</div>
                            @endif
						  </div>
						   <button type="submit" class="btn btn-submit login_btn ">Submit</button>
						{!! Form::close() !!}
						</div>
						</div>
					</div>
				 </div>
			 </div>
			 <!--End of banner_content div-->
		   </header>
		   <!--End of main_banner div-->
		    
			<!--Start of newsletter_section-->
		   <div class="newsletter_section">
		   
		       <div class="row newsletter_wrap">
				<div class="col-sm-6  col-md-7">
				 <h2 class="fs-30 bold"><span class="text-red"> Print & Mail "LifeStyle Newsletter"</span></h2><h2><span> GREAT FOR ANY <strong class="arrow-img">SMALL BUSINESS<i></i></strong></span></h2>
				 <p class="m-t-45">Andy and his team at GoBig have a top-notch product and Company. I’ve toured their 25k sf Direct Mail Facility and have complete confidence in their capabilities to deliver on time and with complete profesionalism.</p>
				 <p>Their team of 60+ and state of the art equipment make this one of the largest Print & Direct Mail houses on the west coast. Their”Easy to use” Web2Print user interface is sophisticated - yet simple. I encourage you to feel confident in working wih them.</p>
				   <p class="author_name">Aaron Helderman<br/>V.P.GKIC | Glazer-Kennedy <img src="images/gkic_img.png" alt="" title="" class="auth-img"></p>
				</div>
				<div class="col-sm-6 col-md-5">
				<div class="iframe-border">
				<iframe width="100%" height="260" src="https://www.youtube.com/embed/poLjq0u4_5A" frameborder="0" allowfullscreen></iframe>
				</div>
				</div>
				</div>
				<div class="row no-margin">
				<div class="col-lg-12 text-center setup-section">
				<h2 class="arialblack wid-95 text-blues">too busy to write a newsletter? looking for one that's customizable and extremely affordable?</h2>
				<p class="m-b-0">Each month we provide you with a new newsletter readt for <a href="javacript:void(0);" class="text-red bold"> <u>PRINT & MAIL!</u> </a></p>
				<p class="">They're complete with new liftstyle content,but you can edit & personalize them online in our design center.</p>
				<h3 class="arialblack wid-95 text-red fs-28 uppercase">SET YOUR MARKETING ON AUTOPILOT! <BR/>QUADRUPLE YOUR LEADS & REFERRALS!</h3>
				<p class="m-t-20"><img src="img/no-setup.png" alt="" title=""> </p>
				</div>
				<div class="clearfix"></div>
				 <!--Start of edit_section-->
		   <div class="edit_section">
		   <div class="yellow-arrow2right"><img src="img/yellow-arrow-2.png"></div>
		       <div class="row no-margin">
				<div class="col-md-5 col-sm-6 newsletters  hidden-xs ">
				   <img src="img/GBN-Fan-SAMPLE-Master-v1.png" alt="" title="">
				</div>
				<div class="col-md-3 promp_wrap hidden-xs hidden-sm">
				  <div class="promption_img"><img src="img/girl.png" alt="" title=""></div>
				</div>
				<div class="col-md-4 col-sm-6 edit_content">
				   <h3>easy to edit!<br/> no computer skills necessary</h3>
				   
				   <p><u>PERSONALIZE THE THEME</u> in our easy to use "Online Designer!" Select from our ever-growing
					number of themes & styles to match your brand.
					Monthly print & mail newsletters that are
					informative and fun built with "Call to Actions" 
					& Personalization.</p>
					<p>Printed & Mailed to your customers at the click of
					mouse!</p>
					<p><u>Done for you!</u> Each month, we create a "New" Newsletter & place it online. You can customize & order on the day of your choice.
					Printing starting at just $.85each!</u>
				</div>
				</div>
			 
		   </div>
		   <!--End of edit_section-->
				<!--Start of easystep_section-->
		   <div class="easystep_section">
		      
		          <div class="row m-t-20 text-center">
				  <h2> Easy as <strong>1,2,3...</strong><img src="img/text123.png" class="m-t-14less"></h2>
				     <div class="col-md-4 col-sm-4 m-t-20">
					    <img src="images/security.png" alt="" title="">
						<h3>Referrals & Retention</h3>
						<p>We take security seriously and back our product with a Security Bounty Program to encourage responsible disclosure by researchers. </p>
						</div>
						<div class="col-md-4 col-sm-4 m-t-20">
					    <img src="images/custom.png" alt="" title="">
						<h3>Customize & Edit</h3>
						<p>A flexible and powerful templating engine and many built-in-options allows you to fully customise the system to your needs. </p>
						</div>
						<div class="col-md-4 col-sm-4 m-t-20">
					    <img src="images/mobile.png" alt="" title="">
						<h3>Personalized</h3>
						<p>Customer can access, manage and place orders from mobile, teblit or desktop thanks to a fully responsive fronted UI </p>
						</div>
				  </div>
				   <!--End of easystep_section-->
		   		<section class="testimonial-home">
				<div class="yellow-arrow2left"><img src="img/yellow-arrow.png"></div>	
				<div class="yellow-bottom10">
				<img src="images/team.png" alt="" title="" class="img-responsive">
				</div>
				   <div class="carousel slide" data-ride="carousel" id="quote-carousel">
        <!-- Bottom Carousel Indicators -->
              <h3 class="text-yellow text-center testim-heading">GREAT FOR ANY PROFESSION</h3>
        <!-- Carousel Slides / Quotes -->
        <div class="carousel-inner">
         <!-- Quote 1 -->
          <div class="item active">
             <p>I have to tell you, you really have an impressive program. We were considering doing our website locally but not only are your prices very competitive, you make it much easier to go through the whole process. Thanks so much!</p>
                <small> ~ Sam Johnson</small>
             </div>
            <!-- Quote 2 -->
          <div class="item">
             <p>I have to tell you, you really have an impressive program. We were considering doing our website locally but not only are your prices very competitive, you make it much easier to go through the whole process. Thanks so much!</p>
                <small> ~ Sam Johnson</small>
          </div>
          <!-- Quote 3 -->
          <div class="item">
           <p>I have to tell you, you really have an impressive program. We were considering doing our website locally but not only are your prices very competitive, you make it much easier to go through the whole process. Thanks so much!</p>
                <small> ~ Sam Johnson</small>
                </div>
         </div>
        
        <!-- Carousel Buttons Next/Prev -->
        <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
        <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
      </div>                          
   	</section>
				</div>
				
				<div class="col-lg-12 text-center setup-section m-t-10">
				<h2 class="arialblack wid-95 text-blues">YOUR CUSTOMERS WILL LOOK FORWARD TO RECEIVING YOUR NEWSLETTER EACH MONTH!</h2>
				<p class="m-b-0">Our"LifeStyle" Newsleters are "Non-Salesy"-"Non-Religous"-"Non-Polotcal"</p>
				<p class="">Packed with fun articles and Call To Actions to get them to call you.</p>
				<h3 class="arialblack wid-95 text-red fs-28">RESPONSE MARKETING ON AUTOPILOT!</h3>
				<h3 class="arialblack wid-95 text-red fs-28 m-t-0">BUILDING RELATIONSHIPS WITHOUT SELLING ANYTHING.</h3>
				</div>
				 </div>
		 
		   <!--End of newsletters_section-->
		   
		   
		  
		   
		    <!--Start of mailed_newsletter-->
		    <div class="mailed_newsletter">
			 <div class="">
			    <div class="row no-margin">
				<h2>why personalized <strong>"mailed" newsletters?</strong></h2>
					 <div class="col-lg-12">
					     <p><span>Simple:</span> Newsletters are the best self promoting,referral generating marketing tool available. It's Marketing without Marketing.</p>
						   <!--Start of accordin panel-->
					        <div class="tab-content faq-cat-content">
					          <div class="tab-pane active in fade" id="faq-cat-1">
						          <div class="panel-group" id="accordion-cat-1">
							          <div class="panel panel-default panel-faq">
								         <div class="panel-heading">
									      <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-1">
										    <h4 class="panel-title">
											Grow your business and achieve long-term success
											  <span class="pull-left accordion_arrow"><i class="fa fa-caret-down"></i></span>
										   </h4>
									   </a>
								  </div>
								  <div id="faq-cat-1-sub-1" class="panel-collapse collapse">
									<div class="panel-body">
									  <ul>
									  <li>• Building relationships is the #1 way to gain and retain customers.</li>
									  <li>• There is no better way to promote your business then to send fun, "non-salesy" newsletters.</li>
									  <li>• It's more important to send "authentic" printed newsletters consistently then any other form of marketing!</li>
									  </ul>
									 </div>
								</div>
							</div>
							<div class="panel panel-default panel-faq">
								         <div class="panel-heading">
									      <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-2">
										    <h4 class="panel-title">
											  If you aren't keeping in touch with your clients, your competitors will!
											  <span class="pull-left accordion_arrow"><i class="fa fa-caret-down"></i></span>
										   </h4>
									   </a>
								  </div>
								  <div id="faq-cat-1-sub-2" class="panel-collapse collapse">
									<div class="panel-body">
									  <ul>
									  <li>• Your goal should be to connect with your clients at least once a month. A good rule of thumb to remember is for every month you DON’T connect with your clients, you lose 10% of the value of your relationship. So if you let it go for 10 months, you’ve probably lost them.</li>
									  <li>• We spend so much time trying to GAIN new customers, but then forget about nurturing the relationship once we have them. It’ll cost you 4-5 times as much trying to regain them or generate new clients.</li>
									  </ul>
									 </div>
								</div>
							</div>
							<div class="panel panel-default panel-faq">
								         <div class="panel-heading">
									      <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-3">
										    <h4 class="panel-title">
											  What makes print newsletters so special?
											  <span class="pull-left accordion_arrow"><i class="fa fa-caret-down"></i></span>
										   </h4>
									   </a>
								  </div>
								  <div id="faq-cat-1-sub-3" class="panel-collapse collapse">
									<div class="panel-body">
									  <ul>
									     <li>• They make it easy to build solid client relationships.</li>
										 <li>• Each month you continue to “mend the fence” with your sphere of influence.</li>
										 <li>• It’s a monthly reminder that keeps you in the forefront of their mind. Rentention is key.</li>
										 <li>• It’s a refreshing break from the stresses of the day. They will quickly start looking forward to receiving it each month.</li>
										 <li>• It’s a chance for you to offer specials and sell more services without selling…</li>
										 <li>• It’s an easy way to educate your customers without any hard sell.</li>
										 <li>• It gives you a chance to ask for referrals and recognize customers who do refer you.</li>
									 </ul>
									 </div>
								</div>
							</div>
							<div class="panel panel-default panel-faq">
								         <div class="panel-heading">
									      <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-4">
										    <h4 class="panel-title">
											  Keep existing clients & gain new ones!
											  <span class="pull-left accordion_arrow"><i class="fa fa-caret-down"></i></span>
										   </h4>
									   </a>
								  </div>
								  <div id="faq-cat-1-sub-4" class="panel-collapse collapse">
									<div class="panel-body">
									  <ul>
									  <li>• When newsletters are mailed to prospects, the sender becomes the first ones they call when they are ready to hire their service!
                                             <strong>Why?</strong>  - Because they kept hearing from them in a fun, non-salesy way. They didn’t keep trying to “sell” them, they just kept sending the newsletter every month – a subtle reminder that they’re always available and ready to go to work whenever they’re ready.</li>
									   </ul>
									 </div>
								</div>
							</div>
							<div class="panel panel-default panel-faq">
								         <div class="panel-heading">
									      <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-5">
										    <h4 class="panel-title">
											 The challenge with newsletters is...
											  <span class="pull-left accordion_arrow"><i class="fa fa-caret-down"></i></span>
										   </h4>
									   </a>
								  </div>
								  <div id="faq-cat-1-sub-5" class="panel-collapse collapse">
									<div class="panel-body">
									  <ul>
									     <li> • It takes a lot of time to write and format a new Newsletter every month, and it’s not easy coming up with new and interesting topics.</li>
									     <li> • Most people think they should send a Newsletter about their industry.</li>
										  <li>• They are very expensive to produce.</li>
										  <li>• It is time consuming to create a Newsletter, gather your Customer List and send it to a printer.</li>
									</ul>
									 </div>
								</div>
							</div>
							<div class="panel panel-default panel-faq">
								         <div class="panel-heading">
									      <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-6">
										    <h4 class="panel-title">
											  The magic behind our service!
											  <span class="pull-left accordion_arrow"><i class="fa fa-caret-down"></i></span>
										   </h4>
									   </a>
								  </div>
								  <div id="faq-cat-1-sub-6" class="panel-collapse collapse">
									<div class="panel-body">
									  <ul>
									   <li> • Our newsletters are NOT a bunch of boring or technical articles or other topics your clients have no interest in. Instead, it’s a fun collection of articles, puzzles, cartoons, DIY’s and contests that gets them involved in reading and sharing it, and also has direct response areas that you can customize, that will prompt them to contact you.</li>
										<li>• Our Newsletters subtly ask for referrals which is the # 1 way to retain, earn & gain new clients.</li>
										<li>• Our Newsletters are VERY AFFORDABLE! Roughly 50% less then going to a local printer like Kinkos, Office Max, Staples etc…</li>
										<li>• Everything is done online in one location.</li>
									  </ul>
									 </div>
								</div>
							</div>
							<div class="panel panel-default panel-faq">
								         <div class="panel-heading">
									      <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-7">
										    <h4 class="panel-title">
											  What should your Newsletter be about?
											  <span class="pull-left accordion_arrow"><i class="fa fa-caret-down"></i></span>
										   </h4>
									   </a>
								  </div>
								  <div id="faq-cat-1-sub-7" class="panel-collapse collapse">
									<div class="panel-body">
									    <h5>#1 Question…</h5>
										  <p>"I don't want to send out a generic newsletter that no one is going to read that is filled with recipes, crossword puzzles and monthly articles about “what happened in June.” - or that has content that’s not about my business. I’m a “Real Estate Agent” - <strong> I want to send out a newsletter about real estate.</strong> Or, I’m a “Home Inspector..” - <strong> I want to send out a newsletter about fixing homes ! </strong></p>
									       <h5>Let me ask you this hypothetical question???</h5>
										    <p> We are a printing company - what if we sent you a Newsletter each month filled with content about printing, design, resolution, pixels, type setting, color specs, production, postage requirements & paper stock - Would you read it? OR, would you chuck it? I bet you’d chuck it. </p>
                                            <h5> #2 Question…</h5>
                                            <p> "I don't want to spend my money sending out a printed Newsletter when I can just eMail it to my clients. Do you have an eMail version? </p>
											<h5>Let me ask you this???</h5>
											 <p>If we sent you a four page Newsletter via eMail would you spend time reading it? Would you play the crossword puzzle & read the articles? OR, would you click delete or save it for another day? (which you’d never get back to).</p>
									            <ul>
												<li> • eMails don’t get left on the table for other people to read (and see your name).</li>
												 <li>• The eMail open rate is about 10% the first hour followed by a decline all the way to 0 over a 48 hour period.</li>
												 <li>• eMails aren’t tangible. People like to hold something… Just like your favorite mail order catalog.</li>
												 <li>• Our Newsletters are created with the intention of being “Fun” & “Non-Salesy” with “Call to Actions,” and subtle reminders that you are in business and ready to help whenever they are ready!</li>
												 <li>• A print and mail Newsletter is sticky! It may start out on your desk and end up on a table at Starbucks… eMails can’t do that.</li>
									 </ul>
									 </div>
								</div>
							</div>
							<div class="panel panel-default panel-faq">
								         <div class="panel-heading">
									      <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-8">
										    <h4 class="panel-title">
											  Myth behind the madness…
											  <span class="pull-left accordion_arrow"><i class="fa fa-caret-down"></i></span>
										   </h4>
									   </a>
								  </div>
								  <div id="faq-cat-1-sub-8" class="panel-collapse collapse">
									<div class="panel-body">
									  <ul>
									       <li> • The idea behind our our Newsletter is to build a relationship over time, consistently, whether they’re looking for what you have to offer or not.</li>
                                           <li> • Stop selling to people. Share with them. We get enough sales pitches already. Build your relationship by giving and informing, not selling and promoting.</li>
									 </ul>
									 </div>
								</div>
							</div>
							<div class="panel panel-default panel-faq">
								         <div class="panel-heading">
									      <a data-toggle="collapse" data-parent="#accordion-cat-1" href="#faq-cat-1-sub-9">
										    <h4 class="panel-title">
											  Print Newsletters as Content Marketing Tactic: Pros, Cons, Examples and Best Practices
											  <span class="pull-left accordion_arrow"><i class="fa fa-caret-down"></i></span>
											  <div class="pull-right writer_name">By Nicolette Beard | TopRank Online Marketing </div>
											  </h4>
									   </a>
									   
								  </div>
								  <div id="faq-cat-1-sub-9" class="panel-collapse collapse">
									<div class="panel-body">
									       <span>By Nicolette Beard | TopRank Online Marketing</span>
                                               <p>One of the oldest forms of content marketing are printed newsletters that arrive by snail mail, providing valuable, information-rich content that your customers and prospects want. They can be an effective compliment to online promotions or serve niche markets where print is a preferred format.</p>
												<p>Given the overall decrease in postal mail, a print newsletter can stand out to the recipient, unlike an online e-newsletter that can get lost in an already too full in-box. Conversely, due to the continuing rise of postal costs, newsletters are more likely to be concise and targeted, providing special appeal. <p>
												<p>According to the <strong> 2014 B2C & B2B Content Marketing Benchmarks, Budgets, and Trends  </strong> report for North America, B2C marketers use print newsletters with greater frequency at 29% compared to their B2B counterparts at 22%. In addition, customer retention/loyalty is cited as a top marketing goal for B2C compared to lead generation for B2B. <p>
												<p>But, both types consider brand awareness to be a crucial content marketing goal, which could explain why print newsletters continue to hold value as a content marketing tactic. <p>
									            <h5>Consumers still love getting mail.</h5>
									             <p>Direct mail, which includes print newsletters, also resonates with consumers in a way that email simply does not. According to marketing firm Epsilon Targeting’s Consumer Channel Preference Study, direct mail continues to deliver as consumers’ preferred means of receiving marketing messages from brands. Here are some of their key findings: </p>
									              <ul>
												       <li> * Six out of 10 U.S. consumers surveyed say they “enjoy getting postal mail from brands about new products.”</li>
														<li> * Across all key verticals – from financial and insurance to retail and personal care – direct mail is preferred over email by all respondents.<li> 
														<li> * The propensity for direct mail also extends to the 18-34 year old demographic.<li> 
														<li> * Consumers report getting an emotional boost from receiving direct mail; 60% agree they “enjoy checking their postal mail boxes.”<li> 
														<li> * Half of all respondents concur with the statement, “I pay more attention to information I receive by postal mail than if it was received by email.”<li> 
												         <p>As with any print medium, a brand has an opportunity to develop a relationship based on a mutually beneficial value proposition. Print newsletters represent an open invitation to communicate your company story as well as its products and services and to optimize beyond the sales life cycle.</p>
												   </ul>
												  <h5>Demonstrate value to cut through the clutter.</h5>
												  <p>As digital marketers, we sometimes forget that good old-fashioned print meets the conditions of content marketing: demonstrate your value proposition while providing value.</p>
												  <p>Print newsletters, just like print magazines, are a mainstay for many companies wanting a credible way to cut through the clutter. Typically, your newsletter periodical will include articles around one specific subject or topic related to your brand or company and written for a group of people with a common interest — namely your products, services or cause.</p>
												  <p>Like any marketing communication, it’s important to establish a consistent distribution cycle to be the most effective. Whether it’s weekly, monthly or quarterly, determine a regular schedule and stick to it.</p>
												  <p>Although the recipients of your newsletter gave you implicit permission to enter your home or office, remember, newsletters are not ads. If you are sending out a promotional newsletter, your goal, of course, is to turn prospects into customers and customers into repeat buyers. You just want to steer clear of any overt sales pitch. But calls to action are more than acceptable.</p>
												  <p>A print newsletter allows you to give more in-depth information to readers, which is especially important when trying to educate buyers about complex products or services. Once they understand fully the benefits of what your company has to offer, they will be more likely to buy.</p>
												  <h5>Pros of Print Newsletters for Content Marketing:</h5>
												   <ul>
												        <li> * It grabs attention.
														 <li>* It’s a proven way to nurture customers, members and leads. </li>
														 <li>* There are no costs for distribution because you already own your customer list.  </li>
														 <li>* Print is enjoying resurgence because it feels new when compared to digital marketing.  </li>
														 <li>* The printed word is perceived to be more substantive and therefore more credible.  </li>
														 <li>* Print allows people to unplug from the online chatter. Its noise to signal ratio is zero, compared to the Internet.  </li>
														 <li>* Print is tactile, providing a deeper visceral connection to the brand. (Read Using neuroscience to understand the role of direct mail.).  </li>
														 <li>* For niche demographics, like seniors, print is perceived as more legitimate.  </li>
														 <li>* Print newsletters enjoy a longer shelf life, leading to more active engagement.  </li>
												    </ul>
													<h5>Cons of Print Newsletters for Content Marketing:</h5>
													 <ul>
													    <li> * Lacks the immediacy and accessibility of other digital communication channels. </li>
														<li>* Budget and timeline may strain marketing resources. </li>
														<li>* With people moving with greater frequency, keeping your list clean may be difficult. </li>
														<li>* Cost prohibitive to publish in more than one language for most companies. </li>
                                                    </ul>
													<h5>Guidelines for Content Marketing with a Print Newsletter:</h5>
													<p> <strong>First impressions: </strong>Even though your newsletter is most likely free to subscribers, will a new reader perceive value in it? The layout and openness of the design, color and weight of the paper and the balance of text and graphics are being scrutinized in a nanosecond. Does it say, read me now or shelve for later? All elements of your newsletter design should enhance your overall brand message and meet readers’ expectations.</p>
                                                    <p> <strong>Format: </strong> Newsletters don’t have to be letter size portrait booklets to be called a newsletter. Other formats may work better, depending on your industry. Explore different sizes, orientations and folds based on the purpose and content length of the publication.</p><p> <strong>Content: </strong> Your goal is to communicate to your audience on a regular basis with non-salesy, fun, interesting information. It does not have to be filled with information about your business at all. Simply suggest a few things if you wish, then direct them back to your website for more.</p>
                                                    <p> <strong>Sales tool: </strong> You can attract more readers and more potential customers by featuring your company’s area of expertise. This should be tips or best practices that are useful to readers regardless of whether they do business with you. A utility company might offer “energy efficiency tips” or a hotel conference center could offer a meeting and planning guide for corporate clients.</p>
                                                    <p> <strong>Offline promotion: </strong> Promote free subscriptions to your newsletter. A simple way to generate leads is to include a call to action in all your company’s marketing communications, such as ads, brochures, catalog, trade show signage, etc. that encourages customers to call or email for a subscription.</p>
                                                   <p>Online promotion: Include a sign-up box on your website for your free print newsletter and include an archive of sample newsletters to show exactly what the subscriber can expect. Promote it through all your social media channels to drive people back to your website to sign up. You may even want to run a contest using social channels, enticing people to sign up for the printed material.</p>
                                                  <p> <strong>Maximize exposure: </strong> Having gone to the time and expense of publishing a newsletter, you want to cast your net wide. Here are a few additional tactics you can employ:</p>
												  <ul>
												         <li>  1. Mail your newsletter to all former customers or potential customers who have requested information on your product or service in the past but never ordered from you. Just be sure you make it easy for them to opt out. </li>  
														 <li>  2. Give your salespeople copies of the newsletter to hand out on sales calls, at trade shows and exhibition booths.</li> 
														 <li>  3. Send the newsletter to all potential clients who may have a strong interest in a solution-oriented article you published that may appeal to them. For example, if your technology solution helped a local hospital save thousands of dollars on staffing costs, send a copy of the newsletter to all the hospitals in your service area.</li> 
													</ul>
													<p>When people actually want to receive information about your company, doesn’t it make sense to include a print newsletter in your marketing mix?</p>
									  
									 </div>
								</div>
							</div>
						  </div>
					   </div>
				      </div>
					   <!--End of accordin panel-->
				   </div>
				  </div>
			 </div>
		   </div>
		   
		   <!--End of mailed_newsletter-->
		   <div class="bonus-section">
			  <div class="red-arrow"><i class="rt-red"><img src="img/arrow-rt.png" alt="" title=""></i>
			  <i class="lft-red"><img src="img/arrow-lft.png" alt="" title=""></i>
			  </div>
			 <div class="row">
			    <div class="col-lg-12">
				 <span class="bonus-text">Bonus</span>
				 <h2 class="arialblack">built in affiliate program ! <br> get your newsletter free and-earn a commission on all sales</h2>
				 <p>We strongly belive that newsletters are the best way to stay in front of your customers.So much so, that we are willing to bet on it.Your subscription comes with a replicates site just like this one.</p>
				 <span class="b-para">You can resell the newsletter program and earn <span class="text-red"> ${{ $monthly_commission }}</span>   per new subsciber each and every and every month.</span>
				 <a href="#" class="learn-link">Learn More</a>
				 
				</div>
			 </div>
			 </div>
			   <!--Start of customer_satisfaction div-->
		   <div class="customer_satisfaction">
		   <div class="gurantee-img"><img src="img/seal-100-percent.png"></div>
		          <div class="row no-margin">
				  <div class="col-md-9">
				    <div class="gurantee_box">
					 <h3>Our 100% satisfaction guarantee</h3>
					  <p> Our 100% satisfaction guarantee means you get to test drive our newsletter templates risk free for 7 days. If you're not completely satisfied with our service or product in the first 7 days we'll give you a full refund.</p>
					  <p>Beyond the first 7 days, you can be assured there are no contracts and no commitments. You can cancel at any time.</p>
					  <p>Disclaimer: Spend 5 days “test driving” our newsletter. If you cancel within the first week, you will be charged nothing. After the first week, your credit card will automatically be charged your monthly subscription fee, which will recur automatically after each period. This does not include printing or postage.</p>
				  </div>
				  <ul class="row gurantee_rules">
				  <li class="col-sm-6"><h4> How long does it take to setup my Newsletter?</h4>
				  <p>That really depends on you! Pick a theme, customize a few areas and walk away. You may want to upload your own banner? That's up to you! </p>
				  </li>
				  <li class="col-sm-6"><h4>Are there any contracts or commitments?</h4>
				  <p>No. Everything is month-to-month. You can cancel at any time. </p>
				  </li>
				  </ul>
				   <ul class="row gurantee_rules">
				  <li class="col-sm-6"><h4>Are there any upfront setup costs or fees?</h4>
				  <p>There are no upfront setup fees. We spent 15 years developing our sophisticated system to be self-serving and highly automated. But don't worry, we're always available to help and support you!</p>
				  </li>
				  <li class="col-sm-6"><h4>What if I get confused? Who will help me?</h4>
				  <p>Any of our 12 dedicated customer service reps will work with you to ensure you're getting the highest level of service possible and the results you're looking for.
				  AND, we have "Live Chat" and answer the phone M-F 8-8 PST!</p>
				  </li>
				   
				  </ul>
				  </div>
				  <div class="col-md-3  hidden-xs hidden-sm no-padding">
				      <img src="images/boy_img.png" alt="" title=""class="img-responsive text-center">
				  </div>
				  </div>
		    </div>
			<!--End of customer_satisfaction div-->
			
			<!--Start of get_ready div -->
			<div class="get_ready">
			   
				  <div class="row no-margin">
				     <div class="col-sm-12 text-center">
					    <h2>ready to get <strong>Started?</strong></h2>
						<a href="javascript:void(0);" class="btn ready_btn ">Get started now risk free</a>
					 </div>
				  </div>
				
			</div>
			<!--End of get_ready div -->
 @include('include.footer')
</div>
<script type="text/javascript" src="{{ URL::asset('js/javascript.js') }}"></script>
<script type="text/javacript">
var $jz = jQuery.noConflict();
$jz(document).ready(function() {
  //Set the carousel options
  $jz('#quote-carousel').carousel({
    pause: true,
    interval: 4000,
  });
});

</script>


