	<div class="main-div">
	@include('include.header')
    <div class="notation-section business-content">
    <div class="row">
    <div class="col-md-12">

       <div class="alert alert-success fs-17 m-t-20">Thank you for submitting your information. As an extension of our gratitude, please accept our Free gift to you. This comprehensive eBook will give you a few options regarding the sale of your home.<br />
		</div>
      	<div class="video-img m-b-30"> 
							   	<iframe width="100%" height="500" src="http://gobignewsletter.stagemyapp.com{{ $pdffile }}" frameborder="0" type='application/pdf'></iframe>
							   </div>
       <a href="http://gobignewsletter.stagemyapp.com{{ $pdffile }}" download="info"><button type="button" class="download-btn m-b-30">Download</button></a>
   </div>
</div>
</div>
@include('include.footer')
</div>