@extends('include.header')
@section('main')
<section class="all-pagesbg">
<h1>"Get your website free"</h1>
<div class="pagesoverlay"></div>
</section>
<div class="container">
<div class="row">	
<div class="website-section query col-md-8 col-md-offset-2">
<h3>What?</h3>
<strong class="imp-para">Mail 10,000 pieces in 6 months or less and get your website FREE for 1 year.</strong>
<h3 class="m-b-0">How!</h3>
<p>Once you have cumulated 10,000 direct mail pieces, simply email <a href="mailto:help@gobigprinting.com">help@gobigprinting.com</a> with your full name. We’ll validate your orders in the Order History section of your direct mail center account and adjust your website fees.</p>
<h2>Direct Mail Center</h2>
<div class="direct-mail">
<a href="http://gobigprinting.com/coaches" class="btn-blues" target="_blank">Enter or create your free account here</a>
<div class="yellow-arrow2"><img src="{{ asset('/images/right-yellow-arrow2.png') }}"></div>

</div>
<div class="m-t-15 m-b-15"><img src="{{ asset('/images/website-free-info.png') }}"></div>
<h3 class="m-b-20">So, now you know...</h3>
<div class="direct-mail">
<div class="panel-footer p-l-40 p-r-40">
<input type="button" value="Get your website" class="home-btns" id="web" onclick="window.location='{{ url("auth/register") }}'">
</div>
<div class="yellow-arrow2"><img src="{{ asset('/images/right-yellow-arrow2.png') }}"></div>
</div>

</div><!--@end about us-->
</div></div>
@include('include.footer')
@stop



