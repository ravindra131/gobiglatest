<div class="main-div">
@include('include.header')

  <div class="row no-margin">
   <div class="top-header">
      <div class="top-content"><h1> HOW IT WORK </h1></div>     
		</div>
		</div>
		<div class="row no-margin">
		<div class="sub-content">
		 <span class="signup-price-h">Customize Online...</span>
		<div class="industries arrow-pos m-b-50"><strong>Pick a Theme, EDIT ARTICLES, <br/> ATTACH Your List, Select Mail DATE </strong>
		<div class="down-arrow1"></div>
		</div>
		</div>
		</div>
		<div class="row no-margin">
		 <div class="col-md-8">
		    
		 </div>
		<div class="col-md-4">
		   <div class="subscribe-wrap">
		    <div class="subscribe-login">
				<span>Gain Access to <br/> THEMES & SAMPLES </span>
				<span class="text-red m-b-10">ENTER BELOW</span>
				</div>
			       <div class=" referral_box bg-color padding-25">
					    <form>
						  <div class="form-group">
							<input type="fname" class="form-control" placeholder="First Name">
						  </div>
						  <div class="form-group">
							<input type="email" class="form-control" placeholder="Email">
						  </div>
						  <div class="form-group">
                             <input type="email" class="form-control" placeholder="phone no.">
						  </div>
						   <button type="submit" class="btn btn-submit login_btn subscribe-btn ">SUBSCRIBE</button>
						</form>
				    </div>
					<div class="theme-lib m-t-25 m-l-20">Our newsletters can be as personalized as you wish! Choose from our ever growing <br/> "Theme" library!</div>
					
						
			
		 </div>
		</div>
		
    </div>
	
	<div class="row no-margin">
			<div class="custom-list m-t-75">
			<span class="signup-price-h">How  does it work ?</span>
			<span class="bold-cont custom-cont">customize & personalize your newsletter <br/> in your design center</span>
			<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
			</div>
	</div>		
	<div class="row no-margin">
			<div class="col-md-4">
			<span class="handwritten-h font-weight-400">Choose your theme</span>
			<img src="images/26.png" class="p-t-20">
			</div>
			<div class="col-md-4">
			<span class="handwritten-h font-weight-400">Pick your banner</span>
			<img src="images/27.png" class="p-t-20">
			</div>
			<div class="col-md-4">
			<span class="handwritten-h font-weight-400">Personalize it</span>
			<img src="images/28.png" class="p-t-20">
			</div>
			
	</div>
	<div class="custom-opt m-t-35 ">
			<span class="bold-cont custom-cont ">edit the articles-upload photoes <br/> we create new content each month <br/> but,you can edit the articles if you wish</div>
			<div class="customer_satisfaction p-t-20 p-b-20 m-t-50 m-b-50">
			<div class="row no-margin">
				  <div class="col-md-12">
				    <div class="gurantee_box">
					 <h3>Our 100% satisfaction guarantee</h3>
					  <p> Our 100% satisfaction guarantee means you get to test drive our newsletter templates risk free for 7 days. If you're not completely satisfied with our service or product in the first 7 days we'll give you a full refund.</p>
					  <p>Beyond the first 7 days, you can be assured there are no contracts and no commitments. You can cancel at any time.</p>
					  <p>Disclaimer: Spend 5 days “test driving” our newsletter. If you cancel within the first week, you will be charged nothing. After the first week, your credit card will automatically be charged your monthly subscription fee, which will recur automatically after each period. This does not include printing or postage.</p>
				  </div>
				  <ul class="row gurantee_rules">
				  <li class="col-sm-6"><h4> How long does it take to setup my Newsletter?</h4>
				  <p>That really depends on you! Pick a theme, customize a few areas and walk away. You may want to upload your own banner? That's up to you! </p>
				  </li>
				  <li class="col-sm-6"><h4>Are there any contracts or commitments?</h4>
				  <p>No. Everything is month-to-month. You can cancel at any time. </p>
				  </li>
				  </ul>
				   <ul class="row gurantee_rules">
				  <li class="col-sm-6"><h4>Are there any upfront setup costs or fees?</h4>
				  <p>There are no upfront setup fees. We spent 15 years developing our sophisticated system to be self-serving and highly automated. But don't worry, we're always available to help and support you!</p>
				  </li>
				  <li class="col-sm-6"><h4>What if I get confused? Who will help me?</h4>
				  <p>Any of our 12 dedicated customer service reps will work with you to ensure you're getting the highest level of service possible and the results you're looking for.
				  AND, we have "Live Chat" and answer the phone M-F 8-8 PST!</p>
				  </li>
				   
				  </ul>
				  </div>
				  
				  </div>
				  </div>

    
	<div class="row no-margin">
			<div class="challenges">
			<span class="bold-cont custom-cont">the challenge with newsletters is...</span>
			<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
			</div>
      </div>

	
	


@include('include.footer')
</div>

