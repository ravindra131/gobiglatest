<div class="main-div">
@include('include.header')


<section class="all-pagesbg">
<h1>About Us</h1>
<div class="pagesoverlay"></div>
</section>
<div class="addthis_inline_share_toolbox home-social"></div>
 <div class="container m-t-40">
<div class="green-heading  about-us"><div class="text-center"><h3>About Us<span class="icon-cross-headiing"><img src="{{ asset('/images/grey-icon-heading.png') }}"><span></h3></div>

<p class="text-center print-link"><strong>Who is the team behind all of this?
</strong></p>
<!--p class="m-b-0 printing-img"><img src="{{ asset('/images/about-logo.png') }}"></p-->
<h4>Who is GoBig Printing?</h4>
<p> <span class="building-img"><img src="{{ asset('/images/ims-building.png') }}"></span> Based in San Diego California, GoBig works with local and national businesses that need to accelerate contact with potential and existing customers to increase sales. A versatile company, GoBig Printing is committed to promoting businesses through creative print and online media. Comprised of experienced and talented people, GoBig Printing is able to readily understand a your business and provide practical marketing solutions to build sales and enhance customer satisfaction.

GoBig Printing has the prerequisite resources and track record to help businesses achieve marketing, sales and
customer relationship goals. </p>
<h4>Who is GoBig Printing?</h4>
<p>GoBig Printing is our premier printing product. Our "Smart Templates™" and private catalog fulfillment system, otherwise known as Web2Print or DVP (Digital Variable Printing) has become a must-have for a multitude of companies. Powered by Impact Marketing Specialists, our State of the Art Sophisticated Technology delivers your brand to your customers via our DVP solutions.</p>

<h4>Who is GoBig Newsletters?</h4>
<p>GoBig Newsletters is our state of the art "Print on Demand" Newsletter program. Newsletters as Unique as YOU are!</p>


<h4>What we Believe</h4>
<p class="m-b-30"> It is our core intention to develop a brand awareness across all mediums that continues to build on and
maintain the integrity of the brand.
• Building strong brands is the cornerstone of GoBig Printing.
• Implementing them across multiple mediums is our mission.</strong></p>
<span class="m-b-0"><img src="{{ asset('/images/arrow.png') }}"></span>

<h4>How to Contact Us</h4>

<h4>Customer Service:</h4>
<p>(orders, questions, online help)</p>
<p>888.326.3309 or 949.348.2292</p>
<p>Please complete the form on the right to email us.</p>

<h4>GoBig:</h4>
<p>(Marketing questions, Partner programs)</p>
<p>760.208.8429</p>
<p>info@GoBigPrinting.com</p>
<p>Office hours are M - F 9AM - 5PM PST</p>
<p>&nbsp;</p>
</div>
</div><!--@end about us-->
@include('include.footer')
</div>

