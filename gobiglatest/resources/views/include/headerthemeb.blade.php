<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>gobbigweb</title>
		<meta name="description" content="">
		<meta name="author" content="user047">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
		{!! HTML::style('/css/bootstrap-themeb.min.css') !!}
		{!! HTML::style('/font-awesome-4.6.1/css/font-awesome.min.css') !!}
		{!! HTML::style('/css/stylethemeb.css') !!}
		
</head>
<body>
	<div class=" container">
		<div class="shadow-section">
		 <div class="top-head">
		 </div>
		 <div class="top-head1">
		 </div>
		<div class="head-wrap">
		 <div class="row no-margin">
		  <div class="col-md-2 col-sm-3 logo-wrap">
		   <a href="{!! url('/theme', array('theme_id' => 2)) !!}">
			 <img src="{{ asset('/images/user-page-logo.png') }}" alt="" title="">
		   </a>
		  </div>
		<div class="col-md-10 col-sm-9 head-contact">
		<div class="row hidden-xs">
			<div class="col-md-5 col-sm-6 company-name m-t-15">
				<span> Cash4urHouse.com</span>
			</div>
		  <div class="col-md-7 col-sm-6">
			<div class="help-contact">
				We are here to help: <span>< Phone ></span>
			</div>
		  </div>
		</div>
		<nav class="navbar header-nav  " role="navigation">
			<div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
				</button> 
			</div>
		<div class="collapse navbar-collapse no-padding" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav m-t-10">
				<li><a href="{!! url('/theme', array('theme_id' => 2)) !!}">Home</a></li>
				<li><a href="{!! url('/theme/sell-home', array('theme_id' => 2)) !!}">Sell your Home</a></li>
				<li><a href="{!! url('/theme/cash-offer', array('theme_id' => 2)) !!}">Get A Cash Offer Today</a></li>
				<li><a href="{!! url('/theme/questions', array('theme_id' => 2)) !!}">Questions</a></li>
				<li><a href="{!! url('/theme/how-it-works', array('theme_id' => 2)) !!}">How it Works</a></li>
				<li><a href="{!! url('/theme/testimonials', array('theme_id' => 2)) !!}">Testimonials</a></li>
			</ul>
		</div>
		</nav>
		</div>
		</div>
		</div>
        <div class="condition-heading">
		</div>
		<div class="condition-wrap">
		<h1>I Buy Houses “As-Is” For Cash In Any Condition </h1>
		</div>
		@yield('main')
		@yield('scripts')