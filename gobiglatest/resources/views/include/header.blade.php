<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<?php
	
        $userresult = getWebsiteData();
		
		// Default Values
		$company_name = 'Gobignewsletter'; 
		
        $logo = getenv('APP_URL').'/images/logo.png'; 
		$subdomain_url = '';
		if($userresult != null && count($userresult) > 0)
		{
			if($userresult[0]->companyname != null && $userresult[0]->companyname != '')
			{
				$company_name = $userresult[0]->companyname;
			}
			if($userresult[0]->logo != null && $userresult[0]->logo != '')
			{
				$logo =getenv('APP_URL').$userresult[0]->logo;
			}
			if($userresult[0]->domain != null && $userresult[0]->domain != '')
			{
				$subdomain_url = $userresult[0]->domain;
			}
		}
        ?>
		
      	<title>{{ $company_name }}</title>
		<meta name="description" content="">
		<meta name="author" content="user047">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
		<link href="{{ URL::asset('css/bootstrap.css') }}"  type="text/css" rel="stylesheet">
		<link href="{{ URL::asset('css/stylesheet.css') }}" type="text/css" rel="stylesheet">
		<link href="{{ URL::asset('css/font-awesome.css') }}" type="text/css" rel="stylesheet">
		<link href="{{ URL::asset('css/calibri.css') }}" type="text/css" rel="stylesheet">
		<link href="{{ URL::asset('css/full-slider.css') }}" type="text/css" rel="stylesheet">
		<link href="{{ URL::asset('css/responsive.css') }}" type="text/css" rel="stylesheet">
		<link href="{{ URL::asset('css/oldstyle.css') }}" type="text/css" rel="stylesheet">
		<link href="{{ URL::asset('css/parsley.css') }}" type="text/css" rel="stylesheet">
  
		{!! HTML::style('css/style.css') !!}
		{!! HTML::style('font-awesome-4.6.1/css/font-awesome.css') !!}
   </head>
   
   
	 <body>
	 
	    <div class="wrapper">
		<!--Start of main_banner div-->
		   <div class="main_banner">
		   <!--Start of navigation div-->
			<nav id="mainNav" class="<?php if(Request::url() == getenv('APP_URL') || ($subdomain_url != '' && Request::url() == 'http://'.$subdomain_url)){ echo 'header-slider'; }?> navbar navbar-default navigation">
               <div class="container">
               <!-- Brand and toggle get grouped for better mobile display -->
                   <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                    </button>
                     <a class="navbar-brand page-scroll logo" href="{{ url('/') }}"><img src="{{ $logo }}" alt="" title="" class="img-responsive"></a>
                     
                     
                   </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse p-r-0" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav  nav_menu">
                  @if(session('statut') == 'visitor')  
											<li {!! classActivePath('try-it-free') !!}><a href="{!! url('/try-it-free') !!}">Pricing</a></li>
										    <li  {!! classActivePath('how-it-works') !!}><a href="{!! url('/how-it-works') !!}">How it works</a></li>
											<li {!! classActivePath('about-us') !!}><a href="{!! url('/about-us') !!}">About us</a></li>
											
											<li {!! classActivePath('testimonials') !!}><a href="{!! url('/testimonials') !!}">Testimonials</a></li>
											<li {!! classActivePath('pricing') !!}><a href="{!! url('/pricing') !!}">Affiliate</a></li>
											<li {!! classActivePath('auth/login') !!} class="login-btn"><a href="{!! url('auth/login') !!}">Login</a></li>
										@elseif(session('statut') == 'admin')
											<li><a href="{!! url('admin') !!}">AdminDashboard</a></li>
										@elseif(session('statut') == 'user')
											<li {!! classActivePath('dashboard/'.auth()->user()->id) !!}>
												<a href="{!! url('/dashboard', array('user_id' => auth()->user()->id)) !!}">Dashboard</a>
											</li>
											<li {!! classActivePath('content/'.auth()->user()->id) !!}>
												<a href="{!! url('/content', array('user_id' => auth()->user()->id)) !!}">My Content</a>
											</li>
											<li {!! classActivePath('visitors/'.auth()->user()->id) !!}>
												<a href="{!! url('/visitors', array('user_id' => auth()->user()->id)) !!}">My Visitors</a>
											</li>
											<li {!! classActivePath('affiliates/'.auth()->user()->id) !!}>
												<a href="{!! url('/affiliates', array('user_id' => auth()->user()->id)) !!}">My Affiliates</a>
											</li>
											<!-- <li {!! classActivePath('referal-url/'.auth()->user()->id) !!} {!! classActivePath('affiliates/'.auth()->user()->id) !!}>
                    							<a href="#" class="dropdown-toggle" data-toggle="dropdown">My Affiliates<b class="caret"></b></a>
                    							<ul class="dropdown-menu">
                        							<li>
                            							<a href="{!! url('/referal-url', array('user_id' => auth()->user()->id)) !!}">Referral URL</a>
                        							</li>
                        							<li>
                            							<a href="{!! url('/affiliates', array('user_id' => auth()->user()->id)) !!}">Affiliaton Record</a>
                        							</li>
                    							</ul>
                							</li> -->
                							<li {!! classActivePath('account/'.auth()->user()->id) !!} {!! classActivePath('change-password/'.auth()->user()->id) !!} {!! classActivePath('banners/'.auth()->user()->id) !!}>
                    							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-user"></span> <span class="username-small">{{ ucfirst(auth()->user()->username) }}</span><b class="caret"></b></a>
                    							<ul class="dropdown-menu">
                    								<li><a href="{!! url('/account', array('user_id' => auth()->user()->id)) !!}">My Profile</a></li>
                    								<li><a href="{!! url('/change-password', array('user_id' => auth()->user()->id)) !!}">Change Password</a></li>
                    								<li><a href="{!! url('/banners', array('user_id' => auth()->user()->id)) !!}">Banners</a></li>
                        							<li>
                            							<a href="{!! url('auth/logout') !!}">Logout</a>
                        							</li>
                    							</ul>
                							</li>
                							@elseif(session('statut') === 'redac')
                																		<li {!! classActivePath('try-it-free') !!}><a href="{!! url('/try-it-free') !!}">TRY IT FREE</a></li>
										    <li><a href="{!! url('/') !!}">How it works</a></li>
											<li {!! classActivePath('about-us') !!}><a href="{!! url('/about-us') !!}">About us</a></li>
											
											<li {!! classActivePath('testimonials') !!}><a href="{!! url('/testimonials') !!}">Testimonials</a></li>
                							<li {!! classActivePath('account/'.auth()->user()->id) !!} {!! classActivePath('change-password/'.auth()->user()->id) !!} {!! classActivePath('banners/'.auth()->user()->id) !!}>
                    							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-user"></span> <span class="username-small">{{ ucfirst(auth()->user()->username) }}</span><b class="caret"></b></a>
                    							<ul class="dropdown-menu">
                    							<li>
												<a href="{!! url('/dashboard', array('user_id' => auth()->user()->id)) !!}">Dashboard</a>
											</li>
                    								<li><a href="{!! url('/account', array('user_id' => auth()->user()->id)) !!}">My Profile</a></li>
                    								<li><a href="{!! url('/change-password', array('user_id' => auth()->user()->id)) !!}">Change Password</a></li>
                        							<li>
                            							<a href="{!! url('auth/logout') !!}">Logout</a>
                        							</li>
                    							</ul>
                							</li>
										@endif
				</ul>
				
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
			<!--End of navigation div-->
		  
		   </div>
		   
		 
		 
   </div>
