<!--Start of footer-->
			<footer>
			<!--Start of footer_links div-->
			 <div class="footer_links">
			 
			   <div class="container">
			     <div class="row">
				    <div class="col-lg-12">
					
					   <ul class="main_list">
					    <li>Business & Personal <br/> Services
						  <ul class="sub_menu">
						  <li><a href="#"> Accounting</a></li>
						  <li><a href="#">Attorney- Business</a></li>
						   <li><a href="#">Attorney- Family</a></li>
						    <li><a href="#">Beauty salon/hair stylist</a></li>
							 <li><a href="#">Child Care</a></li>
							  <li><a href="#">Consulting</a></li>
							  <li><a href="#">Dance School</a></li>
							  <li><a href="#">E-Commerce</a></li>
							  <li><a href="#">Fitness- CrossFit Gym</a></li>
							  <li><a href="#">Fitness- Health Club</a></li>
							  <li><a href="#">Fitness- Personal Trainer</a></li>
							  <li><a href="#">Fitness- Yoga</a></li>
							  <li><a href="#">Fitness- Pilates</a></li>
							  <li><a href="#">Financial Advisor</a></li>
							  <li><a href="#">Pet Grooming</a></li>
							  <li><a href="#">Private Investigator</a></li>
							  <li><a href="#">Other</a></li>
						  </ul>
						</li>
						<li>Medical & Health Services
						  <ul class="sub_menu">
						  <li><a href="#"> Acupuncturist</a></li>
						  <li><a href="#">Assisted Living</a></li>
						   <li><a href="#">Chiropractor</a></li>
						    <li><a href="#">Dentist</a></li>
							 <li><a href="#">Dermatologist</a></li>
							  <li><a href="#">Doctor</a></li>
							  <li><a href="#"> Optometrist / Eye Doctor</a></li>
							  <li><a href="#"> Otolaryngologist</a></li>
							  <li><a href="#">Home Health Care</a></li>
							  <li><a href="#">Lasik Surgeon</a></li>
							  <li><a href="#">Massage Therapist</a></li>
							  <li><a href="#">OBGYN</a></li>
							  <li><a href="#">Orthodontist</a></li>
							 <li><a href="#">Orthopedist</a></li>
							 <li><a href="#">Plastic Surgeon</a></li>
							 <li><a href="#">Podiatrist</a></li>
							 <li><a href="#">Spa</a></li>
							 <li><a href="#">Therapist/Psychiatrist</a></li>
							 <li><a href="#">Veterinarian</a></li>
							 <li><a href="#">Other</a></li>
							  
							  
							  
						  </ul>
						</li>
						<li>Home, Office & Contracting Services
						  <ul class="sub_menu">
						  <li><a href="#"> Architect</a></li>
						  <li><a href="#">Carpet Cleaning</a></li>
						   <li><a href="#">Dry Cleaner</a></li>
						    <li><a href="#">Drywalling</a></li>
							 <li><a href="#">Electrician</a></li>
							  <li><a href="#">Flooring</a></li>
							  <li><a href="#">Garage Door Install</a></li>
							  <li><a href="#">General Contractor</a></li>
							  <li><a href="#">Swimming Pool Service</a></li>
							  <li><a href="#">Handyman</a></li>
							  <li><a href="#">House Cleaning</a></li>
							  <li><a href="#"> HVAC</a></li>
							  <li><a href="#">Interior Design</a></li>
							  <li><a href="#">Janitorial</a></li>
							  <li><a href="#">Landscaping</a></li>
							  <li><a href="#">Lawn Care</a></li>
							  <li><a href="#">Locksmith</a></li>
							  <li><a href="#">Moving Service</a></li>
							  <li><a href="#">Pest Control</a></li>
							  <li><a href="#">Plumbing</a></li>
							  </ul>
						</li>
						<li>Entertainment & Event Services
						  <ul class="sub_menu">
						  <li><a href="#">Bakery</a></li>
						  <li><a href="#">Bar / Grill</a></li>
						   <li><a href="#">Catering</a></li>
						    <li><a href="#">Event Planning</a></li>
							 <li><a href="#">Florist</a></li>
							  <li><a href="#">Limo Servicet</a></li>
							  <li><a href="#">Venue</a></li>
							  <li><a href="#">Other</a></li>
						  </ul>
						</li>
						<li>Automotive Repair & Services
						  <ul class="sub_menu">
						  <li><a href="#">Auto Repair</a></li>
						  <li><a href="#">Car Audio</a></li>
						   <li><a href="#">Glass</a></li>
						    <li><a href="#">Other</a></li>
						   </ul>
						</li>
					   </ul>
					</div>
				 </div>
			   </div>
			 </div>
			 <!--end of footer_links div--> 
			 <!--Start of social_links div-->
			 <?php
				$webresult = getWebsiteData();
				// Default Values
				$fb_link = '#'; 
				$twitter_link = '#'; 
				$youtube_link = '#';
				$linkedin_link = '#';
				$google_link = '#';
				$insta_link = '#';
				if($webresult != null && count($webresult) > 0)
				{
					if($webresult[0]->facebookurl != null && $webresult[0]->facebookurl != '')
					{
						$fb_link = $webresult[0]->facebookurl;
					}
					if($webresult[0]->twitterurl != null && $webresult[0]->twitterurl != '')
					{
						$twitter_link = $webresult[0]->twitterurl;
					}
					if($webresult[0]->youtubeurl != null && $webresult[0]->youtubeurl != '')
					{
						$youtube_link = $webresult[0]->youtubeurl;
					}
					if($webresult[0]->linkedinurl != null && $webresult[0]->linkedinurl != '')
					{
						$linkedin_link = $webresult[0]->linkedinurl;
					}
					if($webresult[0]->googleurl != null && $webresult[0]->googleurl != '')
					{
						$google_link = $webresult[0]->googleurl;
					}
					if($webresult[0]->instaurl != null && $webresult[0]->instaurl != '')
					{
						$insta_link = $webresult[0]->instaurl;
					}
				}
			?>

			 <div class="social_links">
			     <div class="container">
				    <div class="row">
					   <div class="col-lg-12">
					     <ul class="list-inline text-center">
						    <li><a href="{{ $youtube_link }}" class="utube" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
							<li><a href="{{ $linkedin_link }}" class="linkedin" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							<li><a href="{{ $fb_link }}" class="fb" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="{{ $twitter_link }}" class="twit" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="{{ $google_link }}" class="google" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							<li><a href="{{ $insta_link }}" class="insta" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						 </ul>
					   </div>
					</div>
				 </div>
			 </div>
			 <!--End of social_links div-->
			 
			 <!--Start of copyright div-->
			 <div class="copyright">
			    <div class="container">
				  <div class="row">
				    <div class="col-lg-12">
					 <span class="pull-left"> Have question about signing up? Call 123-456 7890</span>
					  <span class="pull-right"> &copy; 2016 gobigwebnewsletter. All Rights Reserved.</span>
					</div>
				  </div>
				<div>
			 </div>
			 <!--End of copyright div-->
			 
			</footer>
			<!--end of footer-->
		  
		   
		</div>
		<!--End of wrpper div-->
	 
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	
	   <script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>
	   