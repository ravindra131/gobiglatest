<html>
<body>
<head>

</head>
<body>

<table cellpadding="10" align="center" cellspacing="0" border="0"  style="max-width:680px; margin:auto;border:1px solid #ddd;">
<tbody>
<tr style="background-color:#f9f9f9;" bgcolor="#f9f9f9;">
<td><a href="http://gobignewsletter.stagemyapp.com/" target="_blank"><img src="{{ asset('/images/logo.png') }}" alt="GoBigNewsletter" width="180" style="border:none;" hspace="10"></a></td>
</tr>
<tr>
<td>
<table width="100%" style="width:100%; " cellpadding="0" cellspacing="0">
<tr>
<td width="50">&nbsp;</td>
<td width="540">
<table width="100%" style="width:100%; " cellpadding="0" cellspacing="0">
<tbody>
<tr><td height="15"></td></tr>
<tr><td><p style="font-size:16px; color:#333; font-family:Tahoma, sans-serif;margin:0px;">Hi {{ ucfirst($agentname) }},</p></td></tr>
<tr><td height="15"></td></tr>
<tr><td><p style="font-size:14px; color:#333; font-family:Tahoma, sans-serif;margin:0px;">We did not receive your payment. So your website is inactive now hence you can not access your website.</p></td></tr>
<tr><td height="15"></td></tr>
<tr><td><p style="font-size:14px; color:#333; font-family:Tahoma, sans-serif;margin:0px;"> <b>Note:</b> If you have any questions, or to get help, please visit Help & Support or contact us at email-id of support team.</p></td></tr>
<tr><td height="15"></td></tr>
<tr><td><p style="font-size:14px; color:#333; font-family:Tahoma, sans-serif;margin:0px;"><a href="http://gobignewsletter.stagemyapp.com/" target="_blank"> http://GoBigNewsletter.com/</a></p></td></tr>
<tr><td height="15"></td></tr>
<tr><td><p style="font-size:14px; color:#333; font-family:Tahoma, sans-serif;margin:0px;">
Regards,<br>
Team GoBigNewsletter 
</p></td></tr>
<tr><td height="20"></td></tr>
<tr><td align="center"><p style="font-size:13px; color:#000; font-family:Tahoma, sans-serif;margin:0px;text-align:center;"></br></br>
This is a Computer-generated email, please do not reply to this message.</p>
</br>
</td></tr>
</tbody></table>
</td>
<td width="50">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</tbody></table>
</body>
</html>