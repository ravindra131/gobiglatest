<?php
if($theme_id =='1') {
	$theme = 'include.headerthemea';
}
else if($theme_id =='2') {
	$theme = 'include.headerthemeb';
}
else {
	$theme = 'include.headerthemec';
}
?>
@extends($theme)
@section('main')
    <div class="question-answer userpage-content">
	 <h2>About us</h2> 
	<p>The art of Building Communications using an ultimate set of toolz.
Who is <strong>< Company Name ></strong>?Based in <strong>< City >, < Company Name ></strong> works with local and national businesses that need to accelerate contact with potential and existing customers to increase sales. A versatile company, <strong>< Company Name ></strong> is committed to promoting businesses through creative print and online media. Comprised of experienced and talented people, <strong>< Company Name ></strong> is able to readily understand your business and provide practical marketing solutions to build sales and enhance customer satisfaction. <strong>< Company Name ></strong> has the prerequisite resources and track record to help businesses achieve marketing, sales andcustomer relationship goals.
What we BelieveIt is our core intention to develop a brand awareness across all mediums that continues to build on and maintain the integrity of the brand. Building strong brands is the cornerstone of <strong>< Company Name ></strong>. Implementing them across multiple mediums is our mission.
How to Contact Us Customer Service:(orders, questions, online help)<strong>< Phone ></strong> Office hours are M - F 9AM - 5PM PST
Please complete the form on the right to email us.</p>
	</div>
@if($theme_id =='1')
	@include('include.footerthemea')
@elseif($theme_id =='2')
	@include('include.footerthemeb')
@else
	@include('include.footerthemec')
@endif
@stop