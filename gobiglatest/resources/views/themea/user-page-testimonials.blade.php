<?php
if($theme_id =='1') {
	$theme = 'include.headerthemea';
}
else if($theme_id =='2') {
	$theme = 'include.headerthemeb';
}
else {
	$theme = 'include.headerthemec';
}
?>
@extends($theme)
@section('main')
     <div class="testimonails-section">
	    <h2>Testimonials </h2>
		   <div class="testimonail-wrap">
		      <p>“Loremipsum dolor sit amet, consec-tetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies.”
               <span> Lorem Ipsum</span>
                <i> Consectetuer Adipiscing</i></p>
				<p>“Loremipsum dolor sit amet, consec-tetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies.”
               <span> Lorem Ipsum</span>
                <i> Consectetuer Adipiscing</i></p>
				<p>“Loremipsum dolor sit amet, consec-tetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies.”
               <span> Lorem Ipsum</span>
                <i> Consectetuer Adipiscing</i></p>
				<p>“Loremipsum dolor sit amet, consec-tetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies.”
               <span> Lorem Ipsum</span>
                <i> Consectetuer Adipiscing</i></p>
				<p>“Loremipsum dolor sit amet, consec-tetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies.”
               <span> Lorem Ipsum</span>
                <i> Consectetuer Adipiscing</i></p>
		     </div>
	</div>
	 </div>
@if($theme_id =='1')
	@include('include.footerthemea')
@elseif($theme_id =='2')
	@include('include.footerthemeb')
@else
	@include('include.footerthemec')
@endif
@stop