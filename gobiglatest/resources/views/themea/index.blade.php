<?php
if($theme_id =='1') {
  $theme = 'include.headerthemea';
}
else if($theme_id =='2') {
  $theme = 'include.headerthemeb';
}
else {
  $theme = 'include.headerthemec';
}
?>
@extends($theme)      
@section('main')
				<div class="house-section">
          
          
            <div class="house-wrap">
            @if($theme_id =='1')
            <img src="{{ asset('/images/user-page-2A1.png') }}" alt="" title="" class="img-responsive">
            @elseif($theme_id =='2')
            <img src="{{ asset('/images/user-page-2B1.png') }}" alt="" title="" class="img-responsive">
            @elseif($theme_id =='3')
            <img src="{{ asset('/images/user-page-2C1.png') }}" alt="" title="" class="img-responsive">
            @endif
            <img src="{{ asset('/images/shadow.png') }}" alt="" title="" class="img-responsive hidden-xs">
            <div class="selling-text">
            <h2>Sell Your House Fast In: </h2>
            <span>Get A Guaranteed Fair All Cash Offer. </span>
            <dfn> We buy houses in Greenville and around SC. Let us make a fair <br/>all-cash offer on your house. No hassles. No obligation</dfn>
                            </div>
              
            <div class="quote-form">
            <p>We Buy Properties </p>
                              <h3> IN ANY CONDITION</h3>
                              <span class="simply"> Simply fill out yhe form below to receive a<br/> Free quote</span>
                              <hr class="rule">
                              <form>
                    <div class="form-group">
                      <label>Property Address*</label>
                      <input type="address" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Phone*</label>
                      <input type="phone" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Email*</label>
                      <input type="email" class="form-control">
                    </div>
                    <input type="submit" class="offer-btn" value="Get My Fair Cash Offer"> </a>
                    </form>
            </div>
          </div>
           <div class="notation-section ">
              <div class="notation-box">
                <img src="{{ asset('/images/book.png') }}" alt="" title="" class="img-responsive">
                    <p>Stop the frustration of your unwanted property. Let us buy your SC house now, regardless of condition. Avoiding foreclosure? Facing divorce? Moving? Upside dowm in your mortgage? Liens? It does’t matter whether you live in it, you’re renting it out, it’s vacent, or not even habitable. <a href="javascript:void(0);">We help owner who have </a>inherited an unwanted </p>
              </div>
              <div class="business-section">
                            <div class="row">
                          <div class="col-md-6">
                            <div class="video-img">
                              <img src="{{ asset('/images/business-video.png') }}" alt="video">
                            </div>
                          </div>
                          <div class="col-md-6 business-content">
                            <h2>We buy houses in < City >.  How Does it Work?</h2>
                            <p class="text-565"> We are a small family owned business in <  City >, and we buy houses! It’s that simple. @if($theme_id == '1') <a href="{!! url('/theme/how-it-works', array('theme_id' => 1)) !!}"> Click here to see how it works</a>@elseif($theme_id == '2') <a href="{!! url('/theme/how-it-works', array('theme_id' => 2)) !!}"> Click here to see how it works</a>@else($theme_id == '3') <a href="{!! url('/theme/how-it-works', array('theme_id' => 3)) !!}"> Click here to see how it works</a> @endif.We will buy any property, <b>“As-Is”for CASH!</b> Are you tired of being a landlord?  Are you facing foreclosure? Have you recently gone through a divorce?  Did you inherit a property?  Does your property need repairs?  If any of these challenges are a big burden to you and your family then please contact us ASAP.  We help people in this situation all the time.  We are kind, considerate, empathetic and caring.  We don’t judge you or make pre-conceived notions about your decisions.  We simply want to help.</p>
                                    <p > So, if this seems like a good fit for you,<b> please take a minute and fill out the form above</b>.We’ll reach out to you ASAP without any goofy sales pitch!  We’ll just chat to see if this is right for you!  It has to work for both of us, so we’ll treat you like family!  </p>
                </div>
                        </div>
                      </div>
                   <div class="row m-t-40">
                <div class="col-md-8 col-sm-7">
                        <div class="sellneed-wrap">
                <h2> Sell your house to us!  Here’s Why!</h2>
                <p class="text-565 m-b-30">Have you ever tried to sell your house?  There are a lot of parts! It gets confusing and can take a long time.  AND, you will loose a lot of money to the agent listing & selling the house.  We’re here to tell you that selling through an Agent isn’t the only way to go. It doesn’t have to be. </p>
                <p>You don’t have to be frustrated with the process or that unwanted property you may have.<b><i>  Let us buy it from you, regardless of the condition.</i></b>  Honestly, we’ve bought total dumps before so don’t think yours may be too far-gone. It isn’t!  </p>
                <p>The great thing about our process is that you don’t have to lift a finger.  We do all the work for you.  We close quickly and on the date of your choosing. You don’t have endless piles of paper work or confusing verbiage to sort through. It doesn’t take having a law degree to get through selling your house.Especially to us!  </p>
                <p>Because we are private buyers you will save thousands by not paying any fees or commissions.  We’ll give you a fair cash offer, plus pay all of the closing fees.  The best part is you don’t have to fix up a thing.</p>
                <p>Here's what Larry. from <  City >, had to say:</p>
                <p class="text-565 m-b-30">“I'm so glad I called < Company Name >.The property I had was a total money pit.  I had no money to fix it up and owed back-taxes on it.  I needed to get rid of it before I lost more money.  Fortunately, <  Company Name > gave me a Cash offer only 1 day after I called.  They paid all the costs and it closed in two weeks! I saved thousands in silly fees I would have had to pay. I didn't have to do anything - not even lift up a broom to sweep.”I thought I had to use a realtor to do this sort of thing.  Thank goodness < Company Name > called.  </p>
                            
                          </div>
                      </div>  
                      
                        <div class="col-md-4 col-sm-5">
                            <div class="view-section" >
                              <h3>What are client saying <span class="pull-right"><img src="{{ asset('/images/testimonials.png') }}"></span></h3>
                              <div class="view-wrap" id="scrollbox3">
                              <div class="client-view">
                              <p>“Loremipsum dolor sit amet, consec-tetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies.”</p>
                              <dfn> Lorem Ipsum</dfn>
                              <h4> Consectetuer Adipiscing</h4>
                              </div>                            
                              
                              </div>
                              </div>
                              </div>
                            </div>
                      </div></div>
                  
@if($theme_id =='1')
  @include('include.footerthemea')
@elseif($theme_id =='2')
  @include('include.footerthemeb')
@else
  @include('include.footerthemec')
@endif
@stop
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
$('#scrollbox3').enscroll({
    showOnHover: false,
    verticalTrackClass: 'track3',
    verticalHandleClass: 'handle3'
 });
});
</script>			
@stop