<?php
if($theme_id =='1') {
	$theme = 'include.headerthemea';
}
else if($theme_id =='2') {
	$theme = 'include.headerthemeb';
}
else {
	$theme = 'include.headerthemec';
}
?>
@extends($theme)
@section('head')
    {!! HTML::style('css/parsley.css') !!}
@stop
@section('main')
              <div class="notation-section business-content">
			            <div class="row">
			                <div class="col-md-7">
							    <h2>How it Works </h2>
							    <p class="text-565 m-b-25">We Buy Houses Anywhere In Greenville And Within This Area, And At ANy Price. Check Out How Our Process Works. We’re Ready To Give You A Fair Offer For For Your House If you want to sell your Greenville house... we’re ready to give you a fair all-cash offer. </p>
								<p>Stop the frustration of your unwanted property. Let us buy your SC house now, regard-less of condition.  
								Avoiding foreclosure? Facing divorce? Moving? Upside dowm in your mortgage?  Liens? It does’t matter whether you live in it, you’re renting it out, it’s vacent, or not even if the house needs repairs 	that you can’t pay for... </p>
							   <div class="video-img m-b-30"> 
							   	<img src="{{ asset('/images/business-video.png') }}" alt="video">
							   </div>
			                    <p class="text-565 m-b-25">We Buy Houses Anywhere In Greenville And Within This Area, And At ANy Price. Check Out How Our Process Works. We’re Ready To Give You A Fair Offer For For Your House If you want to sell your Greenville house... we’re ready to give you a fair all-cash offer. </p>
								<p>Stop the frustration of your unwanted property. Let us buy your SC house now, regard-less of condition.  </p>
								<p>Avoiding foreclosure? Facing divorce? Moving? Upside dowm in your mortgage?  Liens? It does’t matter whether you live in it, you’re renting it out, it’s vacent, or not even if the house needs repairs 	that you can’t pay for... </p>
			                 </div><!--- col-md-7 div End-->
			                <div class="col-md-5 ">
							   <div class="quote-form form-wrap">
								<p>We Buy Properties </p>
                              <h3> IN ANY CONDITION</h3>
                              <span class="simply"> Simply fill out yhe form below to receive a<br/> Free quote</span>
                              <hr class="rule">
                              <form>
                    <div class="form-group">
                      <label>Property Address*</label>
                      <input type="address" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Phone*</label>
                      <input type="phone" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Email*</label>
                      <input type="email" class="form-control">
                    </div>
                    <input type="submit" class="offer-btn" value="Get My Fair Cash Offer"> </a>
                    </form>
								</div>
							</div>
			            </div><!--- row div End-->
			        </div><!--- howit-work-section div End-->
@if($theme_id =='1')
	@include('include.footerthemea')
@elseif($theme_id =='2')
	@include('include.footerthemeb')
@else
	@include('include.footerthemec')
@endif
@stop
@section('scripts')
<script>
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
        };
        </script>
{!! HTML::script('/js/parsley.min.js') !!}
@stop				 