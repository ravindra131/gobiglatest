<?php
if($theme_id =='1') {
	$theme = 'include.headerthemea';
}
else if($theme_id =='2') {
	$theme = 'include.headerthemeb';
}
else {
	$theme = 'include.headerthemec';
}
?>
@extends($theme)
@section('main')
            <div class="question-answer">
	           <p>We understand that you probably have a few questions regarding the way our home buying service works. Take a look at a few of our most frequently asked questions along with the answers to them! While we have tried to cover all of the basics, you may still have a couple of questions more specific to your needs. If you still happen to have any questions after reading over our list below, please do not hesitate to give us a call or fill out one of our contact forms. We would be more than happy to provide you with detailed answers to all of your questions. </p>
	            <i>Q: How does your service differ from that of a real estate agent?</i>
	            <p>A: Real estate agents are in the business of listing and showing properties in order to receive a hefty commission once they sell. On the other hand, we are in the business of only buying properties from homeowners who want to get rid of them quickly. While selling your home using a real estate agent may take months, we are able to facilitate all cash offers sometimes on the very same day. </p>
		        <i>Q: Are your prices fair?</i>
		         <p>A: Since we are looking to quickly take the property off of your hands with an all cash offer, we are also understandably looking for a discount. This is why most of the homes we purchase are below market value. The benefit of using a fast home buying service like ours is that you don't have to wait to put the money in you pocket. In fact, absolutely no time, effort or expense is required of you in order to sell your home as fast as by the end of this week. We also stand behind our no-obligation pricing commitment, which means that you can respectfully decline our cash offer at anytime, but chances are,you won't want to. Yes, it really is that easy and pain-free of a process. </p>
		         <i>Q: Do you charge any fees or commissions?</i>
		         <p>A: The short answer is NO, we do not charge any fees or commissions. Unlike real estate agents, we are not listing your property for sale. Instead, we are actually buying it from you. This means that you won't have to incur any additional listing fees or hidden agent commissions in order to sell your home. You won’t be giving up the 3-6% commission fees you’d have to pay a real estate agent, and you won’t have to wait 6-12 months.  Agents are great if you can wait 6-12 months to sell and you are ok to put in time and money into making your property “Staged” (Looking good to the prospective buyer). Our approach is different – and much easier!  We will review your property, submit a cash offer and then it is up to you to decide whether you accept it or not. Plain and simple. There is 100% NO RISK to you. We take all of the risk by hopingthat we will be able to make any necessary repairs and then sell it for a profit. While this is the best case scenario, we don't necessarily always make a profit when we sell the homes that we buy.  </p>
			   <i>Q: Will you be listing my property for sale?</i>
			     <p>A: No, we are not real estate agents, so we do not ever list properties for sale. We are actually professional home buyers, which means we will buy your home outright and you won't ever have to worry about involving an agent. Once we purchase the home from you, we may choose to repair the property and then resell it to a new owner or we may even decide to keep it as a rental property. </p>
			     <i>Q: How will you determine the offer value of my property?</i>
			    <p>A: Put simply, we are dedicated to providing you with a fair price based on quick value research. Our team of professional home buyers will not only consider the location of the property and the current condition of it but also comparable properties that have recently sold in the area in order to determine a proper cash price. Rest assured that we will diligently review all aspects of your property and marketplace data before coming to a final offer that works for both you and us. </p>
				<i>Q: Do I hold any obligation once my information is submitted?</i>
				<p>A: In no way, shape or form are you obligated to accept our offer for your home when you fill out our form or contact us. After obtaining the details of your property, we will review the details to see where we are. In some cases we might even ask you to provide us with more information if necessary. Once we write you an all cash offer, it is then up to you to determine if you want to accept it or not. Our same day home buying service is 100% NO-HASSLE and 100%. </p>
				<p>OBLIGATION-FREE. Fill out the form to submit the details of your property or give us a call at <strong>< Phone ></strong> to discuss your situation with one of our friendly buying agents </p>
	    </div>
@if($theme_id =='1')
	@include('include.footerthemea')
@elseif($theme_id =='2')
	@include('include.footerthemeb')
@else
	@include('include.footerthemec')
@endif
@stop