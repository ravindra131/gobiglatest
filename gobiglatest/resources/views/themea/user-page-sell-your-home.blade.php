<?php
if($theme_id =='1') {
	$theme = 'include.headerthemea';
}
else if($theme_id =='2') {
	$theme = 'include.headerthemeb';
}
else {
	$theme = 'include.headerthemec';
}
?>
@extends($theme)
@section('head')
    {!! HTML::style('css/parsley.css') !!}
@stop

@section('main')
<div class="notation-section business-content">
<div class="row">
	<div class="col-md-7">
       <div class="sell-home-section">
	   <h2>Live in <strong>< City ></strong> and looking to sell your home the fast and easy way? </h2>
	   <p>Well, get ready for some really good news! We specialize in the purchase of < City > properties in any condition. Please keep reading below to learn if your property qualifies for an ALL CASH OFFER </p>
	   <ul>
	   <li>Are you facing foreclosure or are on the verge of it? </li>
	    <li>Do you currently own rental property you no longer want? </li>
		  <li>Do your tenants frustrate and inconvenience you? </li>
		  <li>Do you own a now vacant property you need to get rid of? </li>
		    <li>Have you inherited a property you don't want or need to own? </li>
		     <li>Do you need to quickly sell your home so you can relocate?</li>
			  <li>Are you going through divorce and need to sell off properties? </li>
			  <li>Do you want to sell your home without paying expensive realtor fees?</li>
			   <li>Do you have little to no equity in your property and need to sell now? </li>
			    <li>Do you no longer have the energy or the means to keep up with your home? </li>
				</ul>
				<p>If you happened to answer “YES” to any of these questions, we are here to help you once again gain your financial independence! Our team members have years of combined experience working with homeowners who are faced with unfortunate financial issues or who simply want to sell their home without all the hassle. </p>
				<p>We encourage you to make yourself aware of your home selling options by filling out our short contact form or give us a call at <strong>< Phone ></strong>. Once you fill out the form, one of our friendly team members will contact you as soon as possible in order to inform you of the best options available for you in your current situation. At no point will we push you into making any obligations or commitments, as we are only here to help you determine the very best financial outcome for you and your family. Better yet, unlike real estate agents, we never charge you. That's right. Our services are<i> 100% FREE OF CHARGE.</i>  </p> 
				<p>It's time to get the straightforward answers you've been searching for.<br/> Fill out the brief form and hold tight as we will be contacting you shortly!  </p>
	   </div>
	   </div>
	   <div class="col-md-5 ">
	   <div class="quote-form form-wrap">
		<p>We Buy Properties </p>
                              <h3> IN ANY CONDITION</h3>
                              <span class="simply"> Simply fill out yhe form below to receive a<br/> Free quote</span>
                              <hr class="rule">
                              <form>
                    <div class="form-group">
                      <label>Property Address*</label>
                      <input type="address" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Phone*</label>
                      <input type="phone" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Email*</label>
                      <input type="email" class="form-control">
                    </div>
                    <input type="submit" class="offer-btn" value="Get My Fair Cash Offer"> </a>
                    </form>
		</div>
		</div>
	   </div>
</div>	   
	   
@if($theme_id =='1')
	@include('include.footerthemea')
@elseif($theme_id =='2')
	@include('include.footerthemeb')
@else
	@include('include.footerthemec')
@endif
@stop
@section('scripts')
<script>
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
        };
        </script>
{!! HTML::script('/js/parsley.min.js') !!}
@stop				 