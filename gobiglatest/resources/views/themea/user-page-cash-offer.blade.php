<?php
if($theme_id =='1') {
	$theme = 'include.headerthemea';
}
else if($theme_id =='2') {
	$theme = 'include.headerthemeb';
}
else {
	$theme = 'include.headerthemec';
}
?>
@extends($theme)
@section('head')
    {!! HTML::style('css/parsley.css') !!}
@stop

@section('main')
  <div class="notation-section business-content">
<div class="row">
	<div class="col-md-7">      		
            <div class="userpage-content">
				<h2> Sell Your Home In Just a Few Short Days! </h2>
				<p>We Pay<i> COLD, HARD CASH </i>for Properties in <strong>< City ></strong> in All Conditions.  </p>
				<p>Let's face it. Due to a number of complicated factors, selling a home in today's market can be rather stressful, time-consuming and even quite expensive.  </p>
				<p>We are highly dedicated to alleviating that stress from your life so you can finally rest assured knowing that you can sell your home without the wait or all of the hassle. No matter the condition of your home or your current situation, we are ready to make an ALL CASH OFFER right away. That means you don't have to fix up your property in order to have it appeal to buyers, show it, wait for an offer or evict current tenants. </p>
				<p>If you have ever attempted to previously sell your home using a real estate agent, you were likely surprised by the costly fees that go along with such a process. While some homeowners who are not in a hurry to sell don't mind footing the bill to list their home, this is not always practical. Whether you are facing a foreclosure, going through a divorce, relocating for a new opportunity or simply no longer want to maintain your property,<i> we are here to provide you with some much needed relief absolutely free of charge.</i> </p>
				<p>Our fast cash offers are always more than fair as we feel that every property owner should be treated with respect and dignity. We have been in the business of buying homes in and around <strong>< City ></strong> for years now and are ready to provide you with a cash offer for your property on the spot. Since we are not real estate agents, but simply home buyers & investors, we are able to take the hassle out of rapidly selling your home. The members of our <strong>< City ></strong> home buying team are incredibly knowledgeable about the local area and market prices, which means we can help you sell your home FAST despite its current condition.  </p>
				<p>Are you ready to sell your home so you can get on with your life?  </p>
				<p>Fill out our short contact form now in order to get started or give us a call at <strong>< Phone ></strong> to speak to one of our helpful home buyers!  </p>
				
				
			</div>
			</div>
	   <div class="col-md-5 ">
	   <div class="quote-form form-wrap">
		<p>We Buy Properties </p>
                              <h3> IN ANY CONDITION</h3>
                              <span class="simply"> Simply fill out yhe form below to receive a<br/> Free quote</span>
                              <hr class="rule">
                              <form>
                    <div class="form-group">
                      <label>Property Address*</label>
                      <input type="address" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Phone*</label>
                      <input type="phone" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Email*</label>
                      <input type="email" class="form-control">
                    </div>
                    <input type="submit" class="offer-btn" value="Get My Fair Cash Offer"> </a>
                    </form>
		</div>
		</div>
	   </div>
</div>	 
@if($theme_id =='1')
	@include('include.footerthemea')
@elseif($theme_id =='2')
	@include('include.footerthemeb')
@else
	@include('include.footerthemec')
@endif
@stop
@section('scripts')
<script>
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
        };
        </script>
{!! HTML::script('/js/parsley.min.js') !!}
@stop				 