<div id="newheader" >
@include('include.header')
</div>

	<section class="all-pagesbg">
<h1>Select Your Theme</h1>
<div class="pagesoverlay"></div>
</section>
<div class="container">
<div class="row m-t-30">
{!! Form::open(['url' => url('auth/getregister'), 'id' => 'themeforma']) !!}
<input type="hidden" name="themeid" value="1">
<div class="col-sm-4 m-t-20">
<div class="view-demo-theme">
<img src="{{ asset('/images/01.png') }}">
<div class="captionoverlay"><a href="{!! url('http://themea.buyshouses4cash.com') !!}" class="login_btn pull-left" target="_blank">View Demo</a>
<!-- <a href="javascript:void(0);" class="gowib-btn pull-right">Select your Theme</a> -->
<input type="submit" class="gowib-btn pull-right" name="select" value="Select your Theme">
</div>
</div>
</div>
{!! Form::close() !!}
{!! Form::open(['url' => url('auth/getregister'), 'id' => 'themeformb']) !!}
<input type="hidden" name="themeid" value="2">
<div class="col-sm-4 m-t-20">
<div class="view-demo-theme">
<img src="{{ asset('/images/02.png') }}">
<div class="captionoverlay"><a href="{!! url('http://themeb.buyshouses4cash.com') !!}" class="login_btn pull-left" target="_blank">View Demo</a>
<!-- <a href="javascript:void(0);" class="gowib-btn pull-right">Select your Theme</a> -->
<input type="submit" class="gowib-btn pull-right" name="select" value="Select your Theme">
</div>
</div>
</div>
{!! Form::close() !!}
{!! Form::open(['url' => url('auth/getregister'), 'id' => 'themeformc']) !!}
<input type="hidden" name="themeid" value="3">
<div class="col-sm-4 m-t-20">
<div class="view-demo-theme">
<img src="{{ asset('/images/03.png') }}">
<div class="captionoverlay"><a href="{!! url('http://themec.buyshouses4cash.com') !!}" class="login_btn pull-left" target="_blank">View Demo</a>
<!-- <a href="javascript:void(0);" class="gowib-btn pull-right">Select your Theme</a> -->
<input type="submit" class="gowib-btn pull-right" name="select" value="Select your Theme">
</div>
</div>
</div>
{!! Form::close() !!}
</div>
</div>
@include('include.footer')

