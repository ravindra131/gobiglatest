@extends('include.header')
@section('head')
    {!! HTML::style('css/parsley.css') !!}
@stop
@section('main')
<section class="all-pagesbg">
<h1>Password Reset</h1>
<div class="pagesoverlay"></div>
</section>
{!! Form::open(['url' => 'password/reset', 'class' => 'bg-light-form form-horizontal', 'method' => 'post', 'data-parsley-validate']) !!}
<div class="container m-t-40">
<div class="green-heading text-center"><h3>password reset form<span class="icon-cross-headiing"><img src="{{ asset('/images/grey-icon-heading.png') }}"><span></h3>
</div>
@if(session()->has('error'))
					@include('partials/error', ['type' => 'danger', 'message' => session('error')])
				@endif
				<input type="hidden" name="token" value="{{ $token }}">	
<div class="row m-t-20">
<div class="col-md-8 col-md-offset-2">
<div class="form-group">
<label class="col-md-3 control-label">Email address </label>
<div class="col-md-9">
	 {!! Form::email('email', null, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'inputEmail',
            'data-parsley-required-message' => 'Please enter e-mail address.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-type'             => 'email',
            'data-parsley-type-message'     => 'Please enter a valid e-mail address.'
        ]) !!}
</div>
</div>
<div class="form-group">
<label class="col-md-3 control-label">Password </label>
<div class="col-md-9">
	{!! Form::password('password', [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'inputPassword',
            'data-parsley-required-message' => 'Please enter password.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-minlength'        => '6',
            'data-parsley-minlength-message'=> 'Please enter password of minimum length 6.'
        ]) !!}
</div>
</div>
<div class="form-group">
<label class="col-md-3 control-label">Confirm Password  </label>
<div class="col-md-9">
	{!! Form::password('password_confirmation', [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'inputCPassword',
            'data-parsley-required-message' => 'Please enter confirm password.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-equalto'			=> '#inputPassword',
            'data-parsley-equalto-message'  => 'Password and confirm password not match.'
        ]) !!}
</div>
</div>
</div>
</div>
<p class="m-t-30 text-center"><input type="submit" Value="Reset" class="gowib-btn-all fs-22"></p>
</div>
{!! Form::close() !!}
@include('include.footer')
@stop

@section('scripts')

	<script>
		$(function() { $('.badge').popover();	});
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
        };
        </script>
{!! HTML::script('/js/parsley.min.js') !!}
@stop