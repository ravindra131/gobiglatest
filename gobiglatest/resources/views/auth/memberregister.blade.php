<div class="main-div">
@include('include.header')


    {!! HTML::style('/css/parsley.css') !!}
    <meta name="csrf-token" content="{{ csrf_token() }}" />


<section class="all-pagesbg">
<h1>Buying Site</h1>
<div class="pagesoverlay"></div>
</section>
{!! Form::open(['url' => url('auth/register'), 'class' => 'form-horizontal', 'method' => 'post', 'data-parsley-validate']) !!}


<div class="container m-t-40">
<div class="green-heading text-center"><h3>registration form<span class="icon-cross-headiing"><img src="{{ asset('/images/grey-icon-heading.png') }}"><span></h3>
</div>
<div class="row m-t-20">
<div class="col-md-8 col-md-offset-2">
<div class="bg-light-form">
<!--<input type="hidden" name="role" value="2">-->
<input type="hidden" name="packageid" value="{{ $packageid }}">
<input type="hidden" name="amount" value="{{ $amount }}">
<div class="form-group">
<label class="col-md-3 control-label">Enter Full Name </label>
<div class="col-md-9">
	{!! Form::text('username', null, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'username',
            'data-parsley-required-message' => 'Please enter full name.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-pattern'          => '/^[a-zA-Z\s]*$/',
            'data-parsley-pattern-message'  =>  'Please enter a valid name.'
        ]) !!}
</div>
</div>
<div class="form-group">
<label class="col-md-3 control-label">Email address </label>
<div class="col-md-9">
	        {!! Form::email('email', null, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'email',
            'data-parsley-required-message' => 'Please enter e-mail address.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-type'             => 'email',
            'data-parsley-type-message'     => 'Please enter a valid e-mail address.',
            'onchange' => 'return checkemail()'
        ]) !!}
        <div class="alert alert-danger" style="display:none;" id="emailerror"></div>
</div>
</div>
<div class="form-group">
<label class="col-md-3 control-label">Password </label>
<div class="col-md-9">
	{!! Form::password('password', [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'password',
            'data-parsley-required-message' => 'Please enter password.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-minlength'        => '6',
            'data-parsley-minlength-message'=> 'Please enter password of minimum length 6.'
        ]) !!}
</div>
</div>
<div class="form-group">
<label class="col-md-3 control-label">Confirm Password  </label>
<div class="col-md-9">
		{!! Form::password('cpassword', [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'cpassword',
            'data-parsley-required-message' => 'Please enter confirm password.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-equalto'			=> '#password',
            'data-parsley-equalto-message'  => 'Password and confirm password not match.'
        ]) !!}
</div>
</div>
</div><!--#End form section-->
</div><!--#End form section-->
<div class="col-md-12 m-t-50">
<div class="bg-light-form form-horizontal">
<div class="form-group">
    <p class="fs-20 text-center m-b-30 text-black">Create a Unique Website Name,  Try using your First Name</p>
<label class="col-md-2 www-label text-right col-sm-2">http://</label>
<div class="col-md-6 col-sm-4 ">
		{!! Form::text('domain', null, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'domain',
            'data-parsley-required-message' => 'Please enter domain name.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-pattern'          => '/^[a-zA-Z0-9]*$/',
            'data-parsley-pattern-message'  =>  'Please enter a valid domain name.',
            'onkeyup' => 'return checkdomain(this.value)'
        ]) !!}
<div class="text-danger" style="display:none;" id="domerror"></div>
</div>
<label class="col-md-4 col-sm-6 www-label">.users.gobignewsletter.stagemyapp.com</label>
</div>
</div>
</div>

</div>
<p class="m-t-30 text-center">
	<input type="button" Value="Previous" class="login_btn fs-22 m-r-10"  onclick="window.location='{{ url("/try-it-free") }}'">
    <input type="submit" Value="Continue" class="login_btn fs-22" onclick="hidediv()" id="continue">
</p>
</div>

{!! Form::close() !!}
@include('include.footer')
</div>
@section('scripts')
	<script>
		$(function() { $('.badge').popover();	});
		window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
        };

        function hidediv()
        {
        	$('#emailerror').hide();
			$('#emailerror').html('');
        }
        </script>
{!! HTML::script('/js/parsley.min.js') !!}
{!! HTML::script('js/validation.js') !!}

