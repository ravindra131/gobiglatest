@include('include.header')

    {!! HTML::style('css/parsley.css') !!}


<section class="all-pagesbg">
<h1>Forgot Password</h1>
<div class="pagesoverlay"></div>
</section>
{!! Form::open(['url' => 'password/email', 'class' => 'form-horizontal', 'method' => 'post', 'data-parsley-validate']) !!}
<div class="container m-t-40">
<div class="green-heading text-center"><h3>forgot password form<span class="icon-cross-headiing"><img src="{{ asset('/images/grey-icon-heading.png') }}"><span></h3>
</div>
@if(session()->has('status'))
    @include('partials/error', ['type' => 'success', 'message' => session('status')])
@endif
@if(session()->has('error'))
	@include('partials/error', ['type' => 'danger', 'message' => session('error')])
@endif	
<div class="row m-t-20">
<div class="col-md-8 col-md-offset-2">
<div class="bg-light-form">
<div class="form-group">
<label class="col-md-3 control-label">Email address </label>
<div class="col-md-9">
	 {!! Form::email('email', null, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'inputEmail',
            'data-parsley-required-message' => 'Please enter e-mail address.',
            'data-parsley-trigger'          => 'change focusout',
            'data-parsley-type'             => 'email',
            'data-parsley-type-message'     => 'Please enter a valid e-mail address.'
        ]) !!}
</div>
</div>
<p class="m-t-30 text-center"><input type="submit" Value="Send" class="login_btn"></p>
</div>
</div>
</div>
</div>
{!! Form::close() !!}
	@include('include.footer')

<script>
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
        };
        </script>
{!! HTML::script('/js/parsley.min.js') !!}
