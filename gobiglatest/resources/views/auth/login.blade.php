<div class="main-div">

@include('include.header')

    {!! HTML::style('css/parsley.css') !!}


<section class="all-pagesbg">
<h1>Login</h1>
<div class="pagesoverlay"></div>
</section>

<div class="container m-t-40">
<div class="green-heading text-center"><h3>login form<span class="icon-cross-headiing"><img src="{{ asset('/images/grey-icon-heading.png') }}"><span></h3>
</div>
{!! Form::open(['url' => 'auth/login', 'class' => ' form-horizontal', 'method' => 'post', 'data-parsley-validate']) !!}
@if(session()->has('error'))
					@include('partials/error', ['type' => 'danger', 'message' => session('error')])
				@endif	
<div class="row m-t-20">
<div class="col-md-8 col-md-offset-2">
<div class="bg-light-form">
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Email address </label>
<div class="col-md-9 col-sm-8">
	{!! Form::text('log', null, [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'inputEmail',
            'data-parsley-required-message' => 'Please enter e-mail address.',
            'data-parsley-trigger'          => 'submit'
        ]) !!}
</div>
</div>
<div class="form-group">
<label class="col-md-3 col-sm-4 control-label">Password </label>
<div class="col-md-9 col-sm-8">
	{!! Form::password('password', [
            'class'                         => 'form-control',
            'required',
            'id'                            => 'inputPassword',
            'data-parsley-required-message' => 'Please enter password.',
            'data-parsley-trigger'          => 'submit',
            'data-parsley-minlength'        => '6',
            'data-parsley-minlength-message'=> 'Please enter password of minimum length 6.'
        ]) !!}
		<p class="m-t-5 text-right">
	<a href="{!! url('/password/email') !!}">Forgot Password</a>
</p>
</div>
</div>
<p class="m-t-10 text-center"><input type="submit" Value="Login" class="login_btn fs-22"></p>
</div>
</div>

</div>
</div>
{!! Form::close() !!}
	@include('include.footer')
</div>

<script>
window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="text-danger parsley" role="alert"></div>'
        };
        </script>
{!! HTML::script('/js/parsley.min.js') !!}


