<div id="newheader" >
@include('include.header')
</div>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<link href="{{ asset('css/payment.css') }}" rel="stylesheet" type="text/css" />
<fieldset id="third">
		<section class="all-pagesbg">
<h1>Review & Checkout</h1>
<div class="pagesoverlay"></div>
</section>
<form method="post" id="paymentForm">
{!! csrf_field() !!}
<div class="container m-t-40">
<table class="checkout-table" cellspacing="0" cellpadding="0">
<tr class="bg-black">
<th>Description</th>
<th>Price</th>
</tr>
<tr>
<td colspan="2" height="15px"></td>
</tr>

<tr class="bor-all">
@if($pid == 1)
<td id="permonth"><a href="javascript:void(0);"><i class="fa fa-times-circle"></i></a>${{ $amount }} per month</td>
@elseif($pid == 2)
<td id="peryear"><a href="javascript:void(0);"><i class="fa fa-times-circle"></i></a>${{ $amount }} annually</td>
@endif
<td id="price">${{ $amount }} USD</td>
</tr>
<tr class="bor-all">
<td class="text-right"> Subtotal:</td>
<td id="subtotal">${{ $amount }} USD</td>
</tr>
<tr class="bor-all green-bg">
<td class="text-right"> Total Due Today:</td>
<td id="totaldue">${{ $amount }} USD</td>
</tr>
<tr class="bor-all">
<td class="text-right"> Total Recurring:</td>
@if($pid == 1) 
<td id="recuring">${{ $amount }} USD Monthly</td>
@elseif($pid == 2)
<td id="recuring">${{ $amount }} USD Annually</td> 
@endif
</tr>
</table><!--# End table-->

<div id="PaymentContainerDiv">
<div class="green-heading text-center m-t-40"><h3>get your website<span class="icon-cross-headiing"><img src="{{ asset('/images/grey-icon-heading.png') }}"><span></span></span></h3>
</div> 
<!-- class="text-center m-t-30">
<span class="domain-regist">Domain Registrant Information</span>
</div-->
<input type="hidden" name="package_id" id="package_id" value="{{ $pid }}">
<input type="hidden" name="payableamount" id="payableamount" value="{{ $amount }}">
@if($user_id != '')
<input type="hidden" name="user_id" id="user_id" value="{{ $user_id }}">
@endif
<div class="bg-light-payment m-t-50">
<div class="row domain-reg domain-payment">
<!-- <div class="col-md-5">
<h3>Promotional Code</h3>
<div class="form-group">
<input type="text" value="" name="pcode" class="form-control" >
<input type="button" class="gowib-btn-black m-t-20" value="Validate Code">
</div>
</div> -->
<div class="col-md-6 col-md-offset-1">
<h3>Payment Method</h3>
<div class="row">
	<div id="orderInfo" style="display:none;" class="alert alert-danger"></div>
	<label class="col-md-3 control-label">Card Type </label>
		<div class="col-md-9 form-group">
			<select class="form-control" name="card_type" id="card_type">
			<option value="Visa">Visa</option>
           <option value="MasterCard">MasterCard</option>
           <option value="Maestro">Maestro</option>
           <option value="Discover">Discover</option>
           <option value="Amex">Amex</option>
			</select>
		</div>
</div>
<div class="row">
	<label class="col-md-3 control-label">Card Number </label>
		<div class="col-md-9 form-group">
			<input type="text" name="card_number" id="card_number" class="form-control">
		</div>
</div>
<div class="row">
	<label class="col-md-3 control-label">Expiry Date </label>
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-6 form-group">
				 <select class="form-control" name="expiry_month" id="expiry_month">
					<option value="01">January</option>
					<option value="02">February</option>
					<option value="03">March</option>
					<option value="04">April</option>
					<option value="05">May</option>
					<option value="06">June</option>
					<option value="07">July</option>
					<option value="08">August</option>
					<option value="09">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
				 </select>
				</div>
				<div class="col-md-6 form-group">
				 <select class="form-control" name="expiry_year" id="expiry_year">
					<option value="2016">2016</option>
					<option value="2017">2017</option>
					<option value="2018">2018</option>
					<option value="2019">2019</option>
					<option value="2020">2020</option>
					<option value="2021">2021</option>
					<option value="2022">2022</option>
					<option value="2023">2023</option>
					<option value="2024">2024</option>
					<option value="2025">2025</option>
					<option value="2026">2026</option>
					<option value="2027">2027</option>
					<option value="2028">2028</option>
					<option value="2029">2029</option>
					<option value="2030">2030</option>
					<option value="2031">2031</option>
					<option value="2032">2032</option>
					<option value="2033">2033</option>
					<option value="2034">2034</option>
					<option value="2035">2035</option>
				 </select>
				</div>
			</div>		 
		</div>
</div>
<div class="row">
	<label class="col-md-3 control-label">CSV Code  </label>
		<div class="col-md-9 form-group">
			<input type="text" id="cvv" name="cvv" class="form-control">
		</div>
</div>

<div class="row">
	<label class="col-md-3 control-label">Name on Card </label>
		<div class="col-md-9 form-group">
			<input type="text" id="name_on_card" name="name_on_card" class="form-control">
		</div>
</div>
</div>

</div>
</div><!--#End BG LIGHT PAYMENT-->

<div class="text-center check-style m-t-30">
<p>
    <input id="iagree" name="iagree" type="checkbox" value="iagree" checked />
<label for="iagree"> I have read and agree to the Terms of Service <label>
</p>
<div class="" id="scrollbox2">
	<p class="no-margin"><i>I am aware that I can cancel anytime. I will contact GoBig Printing Inc via phone: (949) 916-3578 or via eMail: help@gobigprinting.com to cancel.</p>
		<p class="no-margin">I understand that my credit card will be charged each month that I am active unless I choose the Annual, one time payment; in which I will be charged annually until I cancel.</p>
                 <p class="no-margin">I am aware that the $1 trial is good for 7 days and if I don�t cancel before that time I will be charged the current monthly/annual subscription.</p>
		<p class="no-margin">I understand that if my credit card fails, after the grace period of 14 days, I will loose the domain name I set up using: BuysHouses4Cash.com.</p>
		<p class="no-margin">I understand that by being a paying subscriber, I am automatically an affiliate with resale options that earn a commission.</p>
		<p class="no-margin">I am 18 years or older.</i></p>
   </p>
	</div>
<p class="m-t-20">
<input type="button" Value="Previous" class="gowib-btn-all fs-22" style="margin:20px;" onclick="window.location='{{ url("/try-it-free") }}'">
	<input type="button" Value="Complete order" class="gowib-btn-all fs-22" name="card_submit" id="cardSubmitBtn" data-toggle="modal">
</p>
</div>
</div>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="myModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="congrates-box text-center">
	    <div class="right-box">
		<img src="{{ asset('/images/right-icon.png') }}" alt="">
		</div>
         <h2>Congratulations </h2> 
        <h3> You have filled the basic steps/details</h3>
         <!--@if($pid == 1)
          <h3> You have successfully subscribed the services by paying ${{ $amount }} monthly</h3>
          @elseif($pid == 2)
          <h3> You have successfully subscribed the services by paying ${{ $amount }} annually</h3>
          @endif-->
           <h3 class="text-2a"> Now Continue to building</h3>	
			 <h2 class="m-t-0 fs-22">YOUR WEBSITE </h2>
			@if(session('statut') != 'user')
           <p><a href="{!! url('auth/login') !!}" class="gowib-btn-all fs-18 btn-block">Click To Continue</a></p>
           @else
			<p><a href="{!! url('/dashboard', array('user_id' => auth()->user()->id)) !!}" class="gowib-btn-all fs-18 btn-block">Click To Continue</a></p>
			@endif	
			<p class="text-center no-margin"><a href="{!! url('/') !!}" class="text-9a fs-18"><u>Cancel</u></a></p>		   
		</div>
	  </div>
    </div>
  </div>
</div>
</div>

{!! Form::close() !!}
</fieldset>
@include('include.footer')

{!! HTML::script('js/jquery.creditCardValidator.js') !!}
 <script type="text/javascript">
function cardFormValidate(){
     var cardValid = 0;
      
     //Card validation
     $('#card_number').validateCreditCard(function(result) {
         var cardType = (result.card_type == null)?'':result.card_type.name;
         if(cardType == 'Visa'){
             var backPosition = result.valid?'2px -163px, 260px -87px':'2px -163px, 260px -61px';
         }else if(cardType == 'MasterCard'){
             var backPosition = result.valid?'2px -247px, 260px -87px':'2px -247px, 260px -61px';
         }else if(cardType == 'Maestro'){
             var backPosition = result.valid?'2px -289px, 260px -87px':'2px -289px, 260px -61px';
         }else if(cardType == 'Discover'){
             var backPosition = result.valid?'2px -331px, 260px -87px':'2px -331px, 260px -61px';
         }else if(cardType == 'Amex'){
             var backPosition = result.valid?'2px -121px, 260px -87px':'2px -121px, 260px -61px';
         }else{
             var backPosition = result.valid?'2px -121px, 260px -87px':'2px -121px, 260px -61px';
         }
         $('#card_number').css("background-position", backPosition);
         if(result.valid){
             $("#card_type").val(cardType);
             $("#card_number").removeClass('required');
             cardValid = 1;
         }else{             
             $("#card_number").addClass('required');
             cardValid = 0;
         }
     });
      
     //Form validation
     var cardName = $("#name_on_card").val();
     var expMonth = $("#expiry_month").val();
     var expYear = $("#expiry_year").val();
     var cvv = $("#cvv").val();
     var agreevalue = $("#iagree").prop('checked');
     var regName = /^[a-z ,.'-]+$/i;
     var regMonth = /^01|02|03|04|05|06|07|08|09|10|11|12$/;
     var regYear = /^2016|2017|2018|2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030|2031$/;
     var regCVV = /^[0-9]{3,3}$/;
     if (cardValid == 0) {
         $("#card_number").addClass('required');
		 $('#orderInfo').html('Credit card is invalid. Please try again').show();
         $("#card_number").focus();		 
         return false;
     }else if (!regMonth.test(expMonth)) {
         $("#card_number").removeClass('required');
         $("#expiry_month").addClass('required');
         $("#expiry_month").focus();		
         return false;
     }else if (!regYear.test(expYear)) {
         $("#card_number").removeClass('required');
         $("#expiry_month").removeClass('required');
         $("#expiry_year").addClass('required');
         $("#expiry_year").focus();		
         return false;
     }else if (!regCVV.test(cvv)) {
         $("#card_number").removeClass('required');
         $("#expiry_month").removeClass('required');
         $("#expiry_year").removeClass('required');
         $("#cvv").addClass('required');
		 $('#orderInfo').html('CSV Code is invalid. Please try again').show();
         $("#cvv").focus();		
         return false;
     }else if (!regName.test(cardName)) {
         $("#card_number").removeClass('required');
         $("#expiry_month").removeClass('required');
         $("#expiry_year").removeClass('required');
         $("#cvv").removeClass('required');
         $("#name_on_card").addClass('required');
         $('#orderInfo').html('Name is invalid. Please try again').show();
         $("#name_on_card").focus();		 
         return false;
     }else if(agreevalue == false)
     {
		$('#orderInfo').html('Please accept our Terms and Conditions to continue.').show();
     	return false;
     }else{
         $("#card_number").removeClass('required');
         $("#expiry_month").removeClass('required');
         $("#expiry_year").removeClass('required');
         $("#cvv").removeClass('required');
         $("#name_on_card").removeClass('required');
         $('#cardSubmitBtn').prop('disabled', false); 
		 $('#orderInfo').html('').hide();		 
         return true;
     }
 }
    
$(document).ready(function() {       
    
    // //Card form validation on input fields
	
     $('#card_number').on('keyup',function(){
         //Card validation
     $('#card_number').validateCreditCard(function(result) {
         var cardType = (result.card_type == null)?'':result.card_type.name;
         if(cardType == 'Visa'){
             var backPosition = result.valid?'2px -163px, 260px -87px':'2px -163px, 260px -61px';
         }else if(cardType == 'MasterCard'){
             var backPosition = result.valid?'2px -247px, 260px -87px':'2px -247px, 260px -61px';
         }else if(cardType == 'Maestro'){
             var backPosition = result.valid?'2px -289px, 260px -87px':'2px -289px, 260px -61px';
         }else if(cardType == 'Discover'){
             var backPosition = result.valid?'2px -331px, 260px -87px':'2px -331px, 260px -61px';
         }else if(cardType == 'Amex'){
             var backPosition = result.valid?'2px -121px, 260px -87px':'2px -121px, 260px -61px';
         }else{
             var backPosition = result.valid?'2px -121px, 260px -87px':'2px -121px, 260px -61px';
         }
         $('#card_number').css("background-position", backPosition);
         if(result.valid){
             $("#card_type").val(cardType);
             $("#card_number").removeClass('required');
             cardValid = 1;
         }else{             
             $("#card_number").addClass('required');
             cardValid = 0;
         }
     });
	 
     });
    
    //Submit card form
    $("#cardSubmitBtn").on('click',function(){
		
		if(!cardFormValidate())
		{			
			return false;
		}
            var formData = $('#paymentForm').serialize();
            //console.log(formData);
            $.ajax({
                type:'POST',
                url:'{!! url('/paypal/success') !!}',
                dataType: "json",
                data:formData,
                beforeSend: function(){  
                    $("#cardSubmitBtn").val('Processing....');
                    $("#cardSubmitBtn").prop("disabled",true);
                },
                success:function(data){
                	console.log(data);
                    if (data.status == 1) {
                    	$('#orderInfo').hide();
						$('#PaymentContainerDiv').html('<h3>Your payment was successful!</h3>').addClass('bg-light-payment m-t-50 text-center');
                        $('#myModal').modal('show');
                    }else{
                    	$("#cardSubmitBtn").prop("disabled",false);
                    	$("#cardSubmitBtn").val('Complete order');                        
                        $('#orderInfo').html('Credit card is invalid. Please try again.').show();
                    }
                },
				error:function() {
					
					$('#orderInfo').html('Credit card is invalid. Please try again.').show();
					
				}
            });
    });
});
</script>

