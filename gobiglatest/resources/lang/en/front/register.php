<?php

return [
	'title' => 'Registration',
	'infos' => 'To register please fill this form :',
	'email' => 'Email',
	'pseudo' => 'User name',
	'password' => 'Password',
	'confirm-password' => 'Confirm password',
	'warning' => 'Attention',
	'warning-name' => '30 characters maximum',
	'warning-password' => 'At least 8 characters',
	'ok' => 'You have been registered !',
	'error'=> 'These credentials do not match our records.'
];