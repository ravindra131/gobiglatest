<?php

return [
	'title' => 'Form',
	'text' => 'Please fill this form :',
	'name' => 'Website name',
	'testimonials' => 'Testimonials',
	'about' => 'About Us',
	'logo'  => 'Logo'
];