<?php

return [
	'title' => 'Laravel 5',
	'sub-title' => 'An awesome PHP framework',
	'home' => 'Home',
	'contact' => 'Contact',
	'blog' => 'Blog',
	'register' => 'Register',
	'forget-password' => 'Forgotten password',
	'login' => 'Login',
	'administration' => 'Administration',
	'redaction' => 'Redaction',
	'logout' => 'Logout',
	'dashboard' => 'Dashboard',
	'forms'     => 'Forms'
];
