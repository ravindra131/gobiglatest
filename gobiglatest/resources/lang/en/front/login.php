<?php

return [
	'login' => 'Login',
	'text' => 'To log on this site you must fill this form :',
	'email' => 'Email',
	'password' => 'Password',
	'remind' => 'Remember me',
	'forget' => 'Forgot password !',
	'register' => 'Not registered ?',
	'register-info' => 'To register quickly just click on the button !',
	'registering' => 'Register',
	'credentials' => 'These credentials do not match our records.',
	'log' => 'Email or User name',
	'maxattempt' => 'You have reached the maximum number of login attempts. Try again in one minute.',
	'package' => 'Select Package'
];