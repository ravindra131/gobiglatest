<?php

return [
	'email-title' => 'Email verification',
	'email-intro'=> 'To validate your email ',
	'email-link' => 'Click on this link',
	'message' => 'Thanks for signing up! Please check your email.',
	'success' => 'You have successfully verified your account!',
	'again' => 'You must verify your email before you can access the site. ' .
                '<br>If you have not received the confirmation email check your spam folder.'.
                '<br>To get a new confirmation email please <a href="' . url('auth/resend') . '" class="alert-link">Click here</a>.', 
    'resend' => 'A verification message has been re-sent. Please check your email.'
];

