<?php

return [
	'dashboard' => 'Packages',
	'messages' => 'Packages',
	'month' => 'Monthly Fee',
	'annual' => 'Annual Fee',		
	'destroy' => 'Destroy',
	'destroy-warning' => 'Really destroy this package ?',
	'fail' => 'Update fail.'	
];