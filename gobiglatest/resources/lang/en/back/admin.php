<?php

return [
	'administration' => 'Administration',
	'redaction' => 'Redaction',
	'home' => 'Back on site',
	'logout' => 'Logout',
	'dashboard' => 'Dashboard',
	'users' => 'Total Members',
	'see-all' => 'See all',
	'add' => 'Add Member',
	'messages' => 'Messages',
	'packages' => 'Member Package Management',
	'pdf' => 'Upload PDF',
	'new-registers' => 'New members !'
];
