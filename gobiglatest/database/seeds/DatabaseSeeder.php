<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Role, App\Models\User, App\Models\Package, App\Models\Content;
use App\Services\LoremIpsumGenerator;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$lipsum = new LoremIpsumGenerator;

		Role::create([
			'title' => 'Administrator',
			'slug' => 'admin'
		]);

		Role::create([
			'title' => 'Visitor',
			'slug' => 'redac'
		]);

		Role::create([
			'title' => 'Agent',
			'slug' => 'user'
		]);

		User::create([
			'username' => 'admin',
			'email' => 'admin@la.fr',
			'password' => bcrypt('admin@123'),
			'address' => 0,
			'domain_name' => 0,
			'seen' => true,
			'role_id' => 1,
			'theme_id' => 0,
			'referal_id' => 0,
			'company' => 0,
			'domain_status' => 0,
			'valid' => true,
			'confirmed' => true,
			'phoneno' => 0,
			'cityname' => 0,
			'state' => 0,
			'zipcode' => 0
		]);

		User::create([
			'username' => 'themea',
			'email' => 'themea@mail.com',
			'password' => bcrypt('themea'),
			'address' => 0,
			'domain_name' => 'themea',
			'seen' => true,
			'role_id' => 3,
			'theme_id' => 1,
			'referal_id' => 0,
			'company' => 0,
			'domain_status' => 1,
			'valid' => false,
			'confirmed' => true,
			'phoneno' => 0,
			'cityname' => 0,
			'state' => 0,
			'zipcode' => 0
		]);

		User::create([
			'username' => 'themeb',
			'email' => 'themeb@mail.com',
			'password' => bcrypt('themeb'),
			'address' => 0,
			'domain_name' => 'themeb',
			'seen' => true,
			'role_id' => 3,
			'theme_id' => 2,
			'referal_id' => 0,
			'company' => 0,
			'domain_status' => 1,
			'valid' => false,
			'confirmed' => true,
			'phoneno' => 0,
			'cityname' => 0,
			'state' => 0,
			'zipcode' => 0
		]);

		User::create([
			'username' => 'themec',
			'email' => 'themec@mail.com',
			'password' => bcrypt('themec'),
			'address' => 0,
			'domain_name' => 'themec',
			'seen' => true,
			'role_id' => 3,
			'theme_id' => 3,
			'referal_id' => 0,
			'company' => 0,
			'domain_status' => 1,
			'valid' => false,
			'confirmed' => true,
			'phoneno' => 0,
			'cityname' => 0,
			'state' => 0,
			'zipcode' => 0
		]);
                
        Package::create([
			'monthly_fee' => 10,
			'annual_fee' => 100
                ]);
		Service_packages::create([
			'user_id' => 1,
			'monthly_fee' => 100,
			'annual_fee' => 1000
                ]);

        Content::create([
			'user_id' => '2',
			'theme_id' => '1',
			'domain' => 'themea.users.gobigweb.stagemyapp.com',
			'city' => '< City >',
			'phone' => '< Phone >',
			'video_url' => '',
			'facebookurl' => '',
			'twitterurl' => '',
			'logo' => '',
			'agent_domain_check' => 0,
			'agent_domain' => '',
			'companyname' => '< Company name >'
		]);

		Content::create([
			'user_id' => '3',
			'theme_id' => '2',
			'domain' => 'themeb.users.gobigweb.stagemyapp.com',
			'city' => '< City >',
			'phone' => '< Phone >',
			'video_url' => '',
			'facebookurl' => '',
			'twitterurl' => '',
			'logo' => '',
			'agent_domain_check' => 0,
			'agent_domain' => '',
			'companyname' => '< Company name >'
		]);

		Content::create([
			'user_id' => '4',
			'theme_id' => '3',
			'domain' => 'themec.users.gobigweb.stagemyapp.com',
			'city' => '< City >',
			'phone' => '< Phone >',
			'video_url' => '',
			'facebookurl' => '',
			'twitterurl' => '',
			'logo' => '',
			'agent_domain_check' => 0,
			'agent_domain' => '',
			'companyname' => '< Company name >'
		]);

	}

}
