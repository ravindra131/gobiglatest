<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('affiliation', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('agent_id');
            $table->integer('total_clicks')->default(0);
            $table->integer('total_signup')->default(0);
            $table->integer('total_payment')->default(0);
            $table->integer('total_earning')->default(0);
            $table->integer('payment_due')->default(0);
            $table->string('status', 50)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('affiliation');
    }
}
