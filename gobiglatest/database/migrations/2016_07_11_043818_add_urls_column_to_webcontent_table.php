<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUrlsColumnToWebcontentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('webcontent', function (Blueprint $table) {
            
            $table->string('facebookurl',60)->after('video_url')->nullable();
            $table->string('twitterurl',60)->after('facebookurl')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('webcontent', function (Blueprint $table) {

            $table->dropColumn('facebookurl');
            $table->dropColumn('twitterurl');
        });
    }
}
