<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSliderimagesToWebcontentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('webcontent', function (Blueprint $table) {
            
            $table->string('slider1',160)->after('logo')->nullable();
			$table->string('slider2',160)->after('slider1')->nullable();
			$table->string('slider3',160)->after('slider2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('webcontent', function (Blueprint $table) {

            $table->dropColumn('slider1');
            $table->dropColumn('slider2');
            $table->dropColumn('slider3');
        });
    }
}
