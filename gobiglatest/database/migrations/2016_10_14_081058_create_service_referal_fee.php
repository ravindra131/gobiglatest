<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceReferalFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicereferalfees', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('type', 50);
            $table->integer('monthly_bonus');
            $table->integer('annual_bonus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('servicereferalfees');
    }
}
