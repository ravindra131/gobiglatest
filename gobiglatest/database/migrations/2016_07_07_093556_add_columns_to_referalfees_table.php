<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToReferalfeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('referalfees', function (Blueprint $table) {
            
            $table->integer('monthly_bonus')->after('type');
            $table->integer('annual_bonus')->after('monthly_bonus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('referalfees', function (Blueprint $table) {

            $table->dropColumn('monthly_bonus');
            $table->dropColumn('annual_bonus');
        });
    }
}
