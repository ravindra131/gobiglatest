<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendemailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sendemail', function(Blueprint $table)
        {
             $table->increments('id');
             $table->integer('visitor_id')->unsigned();  
             $table->integer('template_id')->unsigned();
             $table->string('email_date',60);
             $table->boolean('email_status')->default(false);
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sendemail');
    }
}
