<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAgentOwnDomainColumnToWebcontentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('webcontent', function (Blueprint $table) {
            
            $table->integer('agent_domain_check')->after('twitterurl')->default(0);
            $table->string('agent_domain',60)->after('agent_domain_check')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::table('webcontent', function (Blueprint $table) {
			$table->dropColumn('agent_domain_check');
            $table->dropColumn('agent_domain');
        });
    }
}
