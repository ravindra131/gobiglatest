<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColumnsToWebcontent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('webcontent', function (Blueprint $table) {
            
            $table->string('slider4',160)->after('slider3')->nullable();
			$table->string('slider5',160)->after('slider4')->nullable();
			$table->string('linkedinurl',60)->after('twitterurl')->nullable();
			$table->string('youtubeurl',60)->after('linkedinurl')->nullable();
			$table->string('googleurl',60)->after('youtubeurl')->nullable();
			$table->string('instaurl',60)->after('googleurl')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::table('webcontent', function (Blueprint $table) {

            $table->dropColumn('slider4');
            $table->dropColumn('slider5');
            $table->dropColumn('linkedinurl');
			$table->dropColumn('youtubeurl');
			$table->dropColumn('googleurl');
			$table->dropColumn('instaurl');
        });
    }
}
