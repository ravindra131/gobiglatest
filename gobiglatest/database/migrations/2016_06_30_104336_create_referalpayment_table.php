<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferalpaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referalpayment', function(Blueprint $table)
        {
             $table->increments('id');
             $table->integer('agent_id')->unsigned();  
             $table->integer('ref_id')->unsigned();
             $table->string('amount',60);
             $table->boolean('paid')->default(false);
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('referalpayment');
    }
}
