<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailcontentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('emailcontent', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('subject', 500);
            $table->text('message');
            $table->string('duration', 30);
            $table->string('status', 30);
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('emailcontent');
    }
}
