<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
        $table->string('company', 100)->after('referal_id')->nullable();
        $table->string('phoneno', 20)->nullable();
        $table->string('cityname', 250)->nullable();
        $table->string('state', 250)->nullable();
        $table->string('zipcode', 250)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('company');
            $table->dropColumn('phoneno');
            $table->dropColumn('cityname');
            $table->dropColumn('state');
            $table->dropColumn('zipcode');
        });
    }
}
