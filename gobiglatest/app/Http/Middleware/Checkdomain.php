<?php

namespace App\Http\Middleware;



use Closure;
use App\Models\ServicePackage;
use App\Models\User;
use App\Models\Content;
use DB;

use Illuminate\Http\RedirectResponse;
class Checkdomain
{
 
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
protected $table = 'webcontent';
    public function handle($request, Closure $next)
    {		
		$URL = 'http://'.$_SERVER['SERVER_NAME'];
	    //$URL = 'Stephen.buyshouses4cash.com';
		//$userresult = User::select('users.id')->where('users.domain_status','=',1)->where('users.domain_name', '=', $URL)->get();
		//$userresult = Content::select('webcontent.id')->where('webcontent.domain', '=', $URL)->get();
		
     	 $userresult = Content::select('webcontent.*','users.domain_status')
				->join('users','webcontent.user_id','=','users.id')				
				->where('users.domain_status','=',1)
				->where('users.confirmed','=',1)			
				->Where('webcontent.domain', '=', $_SERVER['SERVER_NAME'])
				->get();	
      
      //echo count($userresult);die;
		if(count($userresult) > 0 || $URL==getenv('APP_URL') ){
						
			return $next($request);
		}
     
		return new RedirectResponse(url('errors/404'));
        
    }
}
