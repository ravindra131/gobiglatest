<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Repositories\UserRepository;
use App\Repositories\PackageRepository;
use App\Jobs\SendMail;
use App\Models\Package;
use App\Models\Visitors;
use App\Models\Affiliation;
use Cookie;
use DB;
class AuthController extends Controller
{

	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest', ['except' => 'getLogout']);
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  App\Http\Requests\LoginRequest  $request
	 * @param  Guard  $auth
	 * @return Response
	 */
	public function postLogin(
		Request $request,
		Guard $auth)
	{
		$logValue = $request->input('log');
		//$logAccess = filter_var($logValue, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $throttles = in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
			return redirect('/auth/login')
				->with('error', trans('front/login.maxattempt'))
				->withInput($request->only('log'));
        }

		$credentials = [
			'email'  => $logValue, 
			'password'  => $request->input('password')
		];
		if(!$auth->validate($credentials)) {
			if ($throttles) {
	            $this->incrementLoginAttempts($request);
	        }

			return redirect('/auth/login')
				->with('error', trans('front/login.credentials'))
				->withInput($request->only('log'));
		}
			
		$user = $auth->getLastAttempted();

	//	if($user->confirmed) {
			if ($throttles) {
                $this->clearLoginAttempts($request);
            }

			$auth->login($user, $request->has('memory'));

			if($request->session()->has('user_id'))	{
				$request->session()->forget('user_id');
			}
			if($user->role_id==1)
			{
				return redirect('admin');
			}
			else
			{
				$user_id = $user->id;
				return redirect('/dashboard/'.$user_id);
			}
			
	//	}
		
	//	$request->session()->put('user_id', $user->id);	

	//	return redirect('/auth/login')->with('error', trans('front/verify.again'));			
	}

	//function to show select themes view
	public function getthemes()
	{
		return view('auth.themes');
	}

	//function to show registeration form view
	public function getregister(Request $request)
	{
		if($request->input('themeid') != '')
		{
			$themeid = $request->input('themeid');
		}
		else
		{
			$themeid = '';
		}
		$subdomain = $_SERVER['SERVER_NAME'];
		return view('auth.register', compact('themeid','subdomain'));
	}

	/**
	 * Handle a registration request for the application.
	 *
	 * @param  App\Http\Requests\RegisterRequest  $request
	 * @param  App\Repositories\UserRepository $user_gestion
	 * @return Response
	 */
	public function postRegister(
		Request $request,
		UserRepository $user_gestion,
		PackageRepository $package_gestion)
	{
      
		if (Cookie::get('refid') !== null)
		{
			$user = $user_gestion->storedata(
			$request->all(), 
			$confirmation_code = str_random(30),
			$referal_id = Cookie::get('refid')
		);
		}
		else
		{
			$user = $user_gestion->storedata(
			$request->all(), 
			$confirmation_code = str_random(30),
			$referal_id = 0
		);
		}
		$this->dispatch(new SendMail($user));
		if(!empty($user))
		{

			$user_id = $user->id;
			$affiliation = new Affiliation;
			$total_signups = $affiliation->add_signups(Cookie::get('refid'));
			if($total_signups > 0)
			{
				$current_user = [
                'email' => $user->email,
                'password' => $request->input('password'),
            	];
            	\Auth::attempt($current_user);
				return redirect('/getpackages/'.$user_id)->withCookie(Cookie::forget('refid'));
			}
			else
			{
				$current_user = [
                'email' => $user->email,
                'password' => $request->input('password'),
            ];
            \Auth::attempt($current_user);
            if($request->packageid == '')
            {
            	return redirect('/getpackages/'.$user_id);
            }
            else
            {
            	$pid = $request->packageid;
            	$amount = $request->amount;
            	return redirect('/getmember-payment/'.$user_id.'/'.$pid.'/'.$amount);
            }
			
			}
			
		}

	}

	//function to check unique email address at the time of signup
	public function checkemail(UserRepository $user_gestion, Request $request)
	{
		$email = $request->input('user_email');
		$result = $user_gestion->check_email($email);
		return $result;
	}

	//function to check unique domain name at the time of signup
	public function checkdomain(UserRepository $user_gestion, Request $request)
	{
		$domain = $request->input('user_domain');
		$results = $user_gestion->check_domain($domain);
		return $results;
	}

	public function getmemberregister(Request $request, $packageid, $amount)
	{

		$subdomain = $_SERVER['SERVER_NAME'];
		return view('auth.memberregister', compact('subdomain','packageid','amount'));
	}

	
}
