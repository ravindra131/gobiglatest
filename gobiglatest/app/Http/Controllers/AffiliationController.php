<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Affiliation;

use App\Models\Referalfees; 
use App\Models\Referalpayment;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Validator;
use DB;
class AffiliationController extends Controller
{
    public function index()
    {
        $aff_data = new Affiliation;
        $aff_results = $aff_data->getalldata();
        if(count($aff_results))
        {
            for($i=0; $i<count($aff_results); $i++)
            {
                $aff_results[$i]->agentname = DB::table('users')->where('id', $aff_results[$i]->agent_id)->value('username');
            }
            $results = $aff_results;
        }
        else
        {
            $results = '';
        }
    	return view('back.affiliation.index', compact('results'));
    }
	
	public function getcommission()
    {
        $aff_data = new Referalpayment;
        $aff_results = $aff_data->getdata();
		
        if(count($aff_results))
        {
            for($i=0; $i<count($aff_results); $i++)
            {
                $aff_results[$i]->agentname = DB::table('users')->where('id', $aff_results[$i]->agent_id)->value('username');
            }
            $results = $aff_results;
        }
        else
        {
            $results = '';
        }
    	return view('back.affiliation.index', compact('results'));
    }
	
	
	
    public function showfees()
    {
    	return view('back.affiliation.addreferalfee');
    }
    public function setfees(Request $request)
    {
    	$data['type'] = $request->input('type');
    	$data['monthly_bonus'] = $request->input('monthly_bonus');
        $data['annual_bonus'] = $request->input('annual_bonus');
    	$rule  =  array(
                   			'type'       => 'required',
                    		'monthly_bonus'      =>  array('required', 'regex:/^[0-9]*$/'),
                            'annual_bonus' => array('required', 'regex:/^[0-9]*$/')
                		);
		$validator = Validator::make($data,$rule);
		if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->messages());
        }
        else
        {
            $rfee = new Referalfees;
			$rfee->savefeeData($data);
			return Redirect::to('/referalfee')->withMessage('Commission inserted successfully.');      
        }
    }
    public function showrefview()
    {
    	$allfees = new Referalfees;
        $result = $allfees->getallfees();
        if(!empty($result))
        {
            $fees = $result;
        }
        else
        {
            $fees = '';
        }
    	return view('back.affiliation.referalfee', compact('fees'));
    }

    public function editrefview($feeid)
    {
        $editfees = new Referalfees;
        $fees = $editfees->getfeesbyid($feeid);
        return view('back.affiliation.editreferalfee', compact('fees'));
    }

    public function editreffees(Request $request)
    {
        $data['feeid'] = $request->input('feeid');
        $data['type'] = $request->input('type');
        $data['monthly_bonus'] = $request->input('monthly_bonus');
        $data['annual_bonus'] = $request->input('annual_bonus');
        $rule  =  array(
                            'type'       => 'required',
                            'monthly_bonus'      =>  array('required', 'regex:/^[0-9]*$/'),
                            'annual_bonus' => array('required', 'regex:/^[0-9]*$/')
                        );
        $validator = Validator::make($data,$rule);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->messages());
        }
        else
        {
            $updated_fee = new Referalfees;
            $updated_fee->updatefeeData($data);
            return Redirect::to('/referalfee')->withMessage('Commission updated successfully.');      
        }
    }

    public function deletefees($feesid)
    {
        $deletefees = new Referalfees;
        $deletefees->delete_fees($feesid);
        return Redirect::to('/referalfee')->withMessage('Commission deleted successfully.');
    }

    public function change_paidstatus(Request $request)
    {
        $data['agent_id'] = $request->input('id');
        $status = new Referalpayment;
        $result = $status->change_payment_status($data);
        $aff = new Affiliation;
        $status_result = $aff->updatedata($data);
        if($status_result > 0)
        {
            $alldata = $aff->get_affiliates_by_id($data['agent_id']);
            return $alldata;
        }
        else
        {
            return $status_result;
        }
    }

    public function searchaffiliation(Request $request)
    {
        $search = $request->SearchText;
        $record_no = $request->records_no;
        $main_query = Affiliation::select('affiliation.*', 'users.username as agentname','users.id')                   
                    ->join('users', 'users.id', '=', 'affiliation.agent_id')->orderBy('affiliation.id', 'desc');
                   
        if($search != '' || $search != null)
        {
            $main_query->where(function ($query) use ($search) {
                    $query->where('users.username', 'like', '%'.$search.'%');
                    });
        }
        if($record_no != '' || $record_no != null)
        {
            $results = $main_query->paginate($record_no);
        }
        else
        {
            $results = $main_query->paginate(10);
        }
         if($request->ajax())
         {
            $data = [];
            $data['status'] = 'success';
            $data['html'] = \View::make('back.affiliation.part')->withResults($results)->render();
            $data['count'] = count($results);
         }
         else
         {
            return view('back.affiliation.index', compact('results'));
         }

        return response()->json($data);
    }

    public function download_affiliates(Request $request)
    {
        $agent_name = $request->SearchText;
        $main_query = Affiliation::select('affiliation.*','users.id','users.username as agentname')
                    ->join('users','affiliation.agent_id','=','users.id')->orderBy('affiliation.id', 'desc')->where('users.id','!=','');
        if($agent_name != '' || $agent_name != null)
        {
            $main_query->where(function ($query) use ($agent_name) {
                    $query->where('users.username', 'like', '%'.$agent_name.'%');
                    });
        }
        $affiliates = $main_query->get();
        $filename = 'affiliates-'.time().'.csv';
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('Agentname', 'Total Cicks', 'Total Signup', 'Total Payment', 'Total Earning', 'Payment Due'));
        foreach ($affiliates as $row) 
        {
            fputcsv($handle, array($row->agentname, $row->total_clicks, $row->total_signup, $row->total_payment, $row->total_earning, $row->payment_due));
        }
        fclose($handle);

            $headers = array(
                        'Content-Type' => 'text/csv',
                    );

            return \Response::download($filename, $filename, $headers)->deleteFileAfterSend(true);
    }
}
