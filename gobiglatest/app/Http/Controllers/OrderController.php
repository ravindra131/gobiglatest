<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Contactus;
use DB;
use Mail;

class OrderController extends Controller
{
    public function showpartnerorder()
    {
    	$payment = new Payment;
    	$orders = $payment->getalldata_partner();
        $total_orders = Payment::count();
    	return view('back.order.showorderview', compact('orders','total_orders'));
    }
    public function showmemberorder()
    {
        $payment = new Payment;
        $orders = $payment->getalldatamember();
        $total_orders = Payment::count();
        return view('back.order.showorderview', compact('orders','total_orders'));
    }
    public function searchorder(Request $request)
    {

        $search = $request->SearchText;
        $packageid = $request->package_id;
        $from_date = $request->from_date;
        $to_date = $request->to_date; 
        $record_no = $request->record_no;
        // \Session::put('recordno', $record_no);
        // $value = session()->get('recordno');
        $main_query = Payment::select('payment.*', 'users.username as agentname','users.id')                   
                    ->join('users', 'users.id', '=', 'payment.user_id')->orderBy('payment.id', 'desc');
                   
        if($search != '' || $search != null)
        {
            $main_query->where(function ($query) use ($search) {
                    $query->where('users.username', 'like', '%'.$search.'%');
                    });
        }
        if($packageid != '' || $packageid != null)
        {
            $main_query->where(function ($query) use ($packageid) {
                    $query->where('payment.package_id', '=', $packageid);
                    });
        }
         if($from_date != '' || $from_date != null)
        {
            $main_query->where(function ($query) use ($from_date) {
                    $query->where('payment.created_at', '>=', $from_date);
                    });
        }
        if($to_date != '' || $to_date != null)
        {
            $main_query->where(function ($query) use ($to_date) {
                    $query->where('payment.created_at', '<=', $to_date+1);
                    });
        }
        if($record_no != '' || $record_no != null)
        {
            $orders = $main_query->paginate($record_no);
        }
        else
        {
             $orders = $main_query->paginate(10);
        }
        
        if($request->ajax())
        {

            $data = [];
            $data['status'] = 'success';
            $data['html'] = \View::make('back.order.part')->withOrders($orders)->render();
            $data['count'] = count($orders);
        }
        else
        {
            $total_orders = Payment::count();
            return view('back.order.showorderview', compact('orders','total_orders'));
        }

        return response()->json($data);
    }

    public function show_contact_queries()
    {
        $results = Contactus::orderBy('id', 'desc')->paginate(10);
        if(count($results))
        {
            $queries = $results;
        }
        else
        {
            $queries = '';
        }
        return view('back.contactus.showall', compact('queries'));
    }

    public function search_contact_query(Request $request)
    {
        $name = $request->names;
        $email = $request->email_address;
        $main_query = Contactus::select('*')->orderBy('id', 'desc');
                   
        if($name != '' || $name != null)
        {
            $main_query->where(function ($query) use ($name) {
                    $query->where('name', 'like', '%'.$name.'%');
                    });
        }
        if($email != '' || $email != null)
        {
            $main_query->where(function ($query) use ($email) {
                    $query->where('email', 'like', '%'.$email.'%');
                    });
        }

        $queries = $main_query->paginate(10);

        if($request->ajax())
        {

            $data = [];
            $data['status'] = 'success';
            $data['html'] = \View::make('back.contactus.part')->withQueries($queries)->render();
            $data['count'] = count($queries);
        }
        else
        {
            return view('back.contactus.showall', compact('queries'));
        }

        return response()->json($data);
    }

    public function reply_contact_query($id)
    {
        return view('back.contactus.reply', compact('id'));
    }

    public function send_reply(Request $request)
    {
        $pid = $request->input('pid');
        $result = Contactus::where('id', $pid)->get();
        if(count($result))
        {
            $data = [
            'title'  => $result[0]->name,
            'reply_msg' => $request->input('rmessage')
        ];
         Mail::send('emails.contactusreply', $data, function($message) use($result) {
            $message->to($result[0]->email)->subject('Contact Us Reply Email');
        });
            return Redirect::to('/show-queries')->withMessage('Reply sent successfully.');
        }

    }

    public function download_orders(Request $request)
    {
        $search = $request->SearchText;
        $packageid = $request->package_id;
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $main_query = Payment::select('payment.*','users.id','users.username as agentname')
                ->join('users','payment.user_id','=','users.id')->orderBy('payment.id', 'desc')
                ->where('users.id','!=','');
        if($search != '' || $search != null)
        {
            $main_query->where(function ($query) use ($search) {
                    $query->where('users.username', 'like', '%'.$search.'%');
                    });
        }
        if($packageid != '' || $packageid != null)
        {
            $main_query->where(function ($query) use ($packageid) {
                    $query->where('payment.package_id', '=', $packageid);
                    });
        }
         if($from_date != '' || $from_date != null)
        {
            $main_query->where(function ($query) use ($from_date) {
                    $query->where('payment.created_at', '>=', $from_date);
                    });
        }
        if($to_date != '' || $to_date != null)
        {
            $main_query->where(function ($query) use ($to_date) {
                    $query->where('payment.created_at', '<=', $to_date);
                    });
        }
        $orders = $main_query->get();
        $filename = 'orders-'.time().'.csv';
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('Agentname', 'Package name', 'Amount', 'Order date'));
        foreach ($orders as $row) 
        {
            if($row->package_id == 1)
            {
                $row->package_name = 'Monthly';
            }
            elseif($row->package_id == 2)
            {
                $row->package_name = 'Annual';
            }
            else
            {
                $row->package_name = 'Free';
            }
            fputcsv($handle, array($row->agentname, $row->package_name, $row->amount, $row->created_at));
        }
        fclose($handle);

            $headers = array(
                        'Content-Type' => 'text/csv',
                    );

            return \Response::download($filename, $filename, $headers)->deleteFileAfterSend(true);
    }
}
