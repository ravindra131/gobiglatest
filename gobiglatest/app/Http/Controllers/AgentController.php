<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Redirect;
use App\Repositories\PackageRepository;
use Input, DB;
use App\Models\Content;
use App\Models\Visitors;
use App\Models\Affiliation;
use App\Models\User;
use App\Models\Payment;
use App\Models\ServicePackage;
use Cookie;
use Session;
use App\Repositories\UserRepository;
use App\Jobs\SendMail;
use App\Libraries\PaypalPro;
use Mail;
class AgentController extends Controller
{
    public function showdashboard($userid)
	{
		$userconfirmed = DB::table('users')->where('id', $userid)->value('confirmed');
		$content = new Content;
		$con_result = $content->getcontent($userid);
		if(!empty($con_result))
		{
			$subdomain_name = $con_result[0]->domain;
			$domainname = $con_result[0]->agent_domain;
		}
		else
		{
			$subdomain_name = '';
			$domainname = '';
		}
		$package_result = Payment::where('user_id','=',\Auth::user()->id)->where('transaction_id','!=',0)->orderBy('id','desc')->value('package_id');
		if($package_result != '')
		{
			$package = $package_result;
		}
		else
		{
			$package = '';
		}
		$payment_info = Payment::where('user_id','=',\Auth::user()->id)->where('transaction_id','!=',0)->orderBy('id','desc')->get();
		//echo(count($payment_info));die;
		$paypal_profile_status = [];
		if(count($payment_info))
		{
			$profileid = $payment_info[0]->transaction_id;
			$paypal = new PaypalPro;	
			$paypalParams = ['PROFILEID' => $profileid];
			$paypal_profile = $paypal->getRecurringProfileStatus($paypalParams);
			if($paypal_profile['ACK'] == 'Success' && $paypal_profile['STATUS'] != 'Cancelled')
			{
				$paypal_profile_status = $paypal_profile;
			}
			
		}
		return view('front.agent.dashboard',compact('subdomain_name','domainname','userconfirmed','paypal_profile_status','package'));
	}
	
	public function cancelSubscription($user_id = null)
	{
		if($user_id == null)
		{
			$user_id = \Auth::user()->id;
		}
		
		$payment_info = Payment::where('user_id','=',$user_id)->where('transaction_id','!=',0)->orderBy('id','desc')->get();
		$paypal_profile_status = [];
		if(count($payment_info))
		{
			$profileid = $payment_info[0]->transaction_id;
			$paypal = new PaypalPro;	
			$paypalParams = ['PROFILEID' => $profileid, 'ACTION' => 'Cancel']; 
			$paypal_profile_status = $paypal->cancelRecurringProfileStatus($paypalParams);
			$update_status = User::where('id','=',$user_id)->update(array('domain_status' => 0));
                        if($update_status > 0)
			{
				$agent_email = User::where('id',$user_id)->value('email');
				$agent_name = User::where('id',$user_id)->value('username');
				$datas = [
            				'title'  => $agent_name
        				 ];
				Mail::send('emails.cancelsubscription', $datas, function($message) use($agent_email)
				{
            		$message->to($agent_email)->subject('Cancel Subscription');
        		});
        		$admin_email = User::where('id','=',1)->value('email');
				Mail::send('emails.cancelsubscriptionbyagent', $datas, function($message) use($admin_email)
				{
            		$message->to($admin_email)->subject('Cancel Subscription');
        		});
			}
		}		
		
		return redirect()->back()->withMessage('Subscription cancelled successfully.');
		
	}

	public function showvisitors($userid)
	{
		$visitor = new Visitors;
		$result = $visitor->getVisitors($userid);
		if(!empty($result))
		{
			$visitor_result = $result;
		}
		else
		{
			$visitor_result = '';
		}
		return view('front.agent.visitors', compact('visitor_result'));
		
	}
	public function showcontent($user_id)
	{
		/* $user_info = DB::table('users')->where('id', $user_id)->get();
		 $package_info = ServicePackage::where('user_id', $user_id)->get();
		if(count($package_info))
		{
			$monthly_fees = $package_info[0]->monthly_fee;
			$annual_fees = $package_info[0]->annual_fee;
		}
		else
		{
			$monthly_fees = '';
			$annual_fees = '';
		}*/
		$content = new Content;
		$con_result = $content->getcontent($user_id);
		if(!empty($con_result))
		{
			$id = $con_result[0]->id;
			/*$city = $con_result[0]->city;
			$phone = $con_result[0]->phone;
			$video_url = $con_result[0]->video_url;*/
			$companyname = $con_result[0]->companyname;
			$facebookurl = $con_result[0]->facebookurl;
			$twitterurl = $con_result[0]->twitterurl;
			$youtubeurl = $con_result[0]->youtubeurl;
			$linkedinurl = $con_result[0]->linkedinurl;
			$googleurl = $con_result[0]->googleurl;
			$instaurl = $con_result[0]->instaurl;
			$logo = $con_result[0]->logo;
			$domain = $con_result[0]->domain;
			$slider1 = $con_result[0]->slider1;
		}
		else
		{
			$id = '';
			/*$city = '';
			$phone = '';
			$video_url = '';*/
			$companyname = '';
			$facebookurl = '';
			$twitterurl = '';
			$youtubeurl = '';
			$linkedinurl = '';
			$googleurl = '';
			$instaurl = '';
			$logo = '';
			$domain = '';
			$slider1 = '';
		}
		 return view('front.agent.content', ['id' => $id, 'companyname' => $companyname, 'facebookurl' => $facebookurl, 'twitterurl' => $twitterurl, 'youtubeurl' => $youtubeurl, 'linkedinurl' => $linkedinurl, 'googleurl' => $googleurl, 'instaurl' => $instaurl, 'logo' => $logo, 'domain' => $domain, 'slider1' => $slider1]);
	}
	public function addcontent(Request $request)
	{
			//$data['domain'] = ($request->domain) ? $request->domain : $request->input('userdomain').'.users.'.$_SERVER['SERVER_NAME'];
      		$data['domain'] = ($request->domain) ? $request->domain : $request->input('userdomain').'.users.gobignewsletter.stagemyapp.com';
			//$data['theme_id'] = $request->input('themeid');
			$data['user_id'] = $request->input('userid');
			//$data['city'] = $request->input('city');
			//$data['phone'] = $request->input('phone');
			//$data['video_url'] = $request->input('video_url');
			$data['companyname'] = $request->input('companyname');
			$data['facebookurl'] = $request->input('facebookurl');
			$data['twitterurl'] = $request->input('twitterurl');
			$data['youtubeurl'] = $request->input('youtubeurl');
			$data['linkedinurl'] = $request->input('linkedinurl');
			$data['googleurl'] = $request->input('googleurl');
			$data['instaurl'] = $request->input('instaurl');
			$data['slider1'] = $request->input('sliderimage');
			
			$destinationPath = 'uploads/';
			if($request->hasFile('logo'))
			{
				$filename = $request->file('logo')->getClientOriginalName();
				$request->file('logo')->move($destinationPath, $filename);
				$data['logo'] = '/uploads/' . $filename;
			}
			
			/* $result['user_id'] = $request->input('userid');
			$result['monthly_fee'] = $request->input('monthprice');
			$result['annual_fee'] = $request->input('annualprice');
			$datas['company'] = $request->input('company_name');
			$datas['phoneno'] = $request->input('phone_no');
			$datas['address'] = $request->input('address');
			$datas['cityname'] = $request->input('city_name');
			$datas['state'] = $request->input('state');
			$datas['zipcode'] = $request->input('zipcode');*/
			$rule  =  array(
                    		'companyname' => 'required',
							'facebookurl' => array('regex:/^(https?:\/\/)?(www\.)?facebook.com\//'),
							'twitterurl' => array('regex:/^(https?:\/\/)?(www\.)?twitter.com\//'),
							'youtubeurl' => array('regex:/^(https?:\/\/)?(www\.)?youtube.com\//'),
							'linkedinurl' => array('regex:/^(https?:\/\/)?(www\.)?linkedin.com\//'),
							'googleurl' => array('regex:/^(https?:\/\/)?(www\.)?plus.google.com\//'),
							'instaurl' => array('regex:/^(https?:\/\/)?(www\.)?instagram.com\//')
                		) ;
			$validator = Validator::make($data,$rule);
			if ($validator->fails())
        	{
           		 return redirect()->back()->withErrors($validator->messages());
        	}
        	else
        	{
        		/* $query = User::where('id', $request->input('userid'))->update($datas);
        		$data_exist = ServicePackage::where('user_id', $result['user_id'])->get();
        		if(count($data_exist))
        		{
        			$addpackage = ServicePackage::where('user_id', $result['user_id'])->update($result);
        		}
        		else
        		{
        			$addpackage = ServicePackage::insertGetId($result);
        		}*/
            	$content = new Content;
            	if($request->input('contentid') == '')
            	{
            		$result = $content->save_user_content($data);
            		if($result > 0)
            		{
            			return redirect('/dashboard/'.$data['user_id'])->withMessage('Your website content added successfully.');
                          		       // $domain_name = $request->input('userdomain').'.stagemyapp.com';
						// $file_path = '/etc/apache2/sites-available/'. $domain_name . '.conf';
						// $theme = '/var/www/staging/themes/public';
						// if (!file_exists($file_path))
						// {
						// 	copy('/etc/apache2/sites-available/dummy.stagemyapp.com.conf', $file_path);
						// 	file_put_contents($file_path,str_replace('{my_server_name}',$domain_name,file_get_contents($file_path)));
						// 	file_put_contents($file_path,str_replace('{my_public_path}',$theme,file_get_contents($file_path)));
						// 	return redirect()->back()->withMessage('Your content added successfully.'); 
						// }
						// else
						// {
						// 	return redirect()->back()->withMessage('Oops something went wrong! Please try again.');
						// }
           			}
            		else
            		{
            			return redirect()->back()->withMessage('Oops something went wrong! Please try again.');
            		}
            	}
            	else
            	{
            		$updated_result = $content->edit_user_content($data,$request->input('contentid'));
            		return redirect('/dashboard/'.$data['user_id'])->withMessage('Your website content updated successfully.'); 
            	}
			}
		
	 }
	 public function addnotes(Request $request)
	 {
	 	$data['visitorid'] = $request->input('vid');
	 	$data['notes'] = $request->input('notes');
	 	$visitor = new Visitors;
	 	$add_result = $visitor->add_notes($data);
	 	if($add_result >= 0)
	 	{
	 		$agent_note = DB::table('visitors')->where('id', $data['visitorid'])->value('notes');
	 		return $agent_note;
	 	}
	 }
	 public function showurl($userid)
	 {
	 	$referal_url = 'http://'.$_SERVER['SERVER_NAME'].'/referral/'.$userid;
	 	return view('front.agent.referalurl',compact('referal_url'));
	 }
	 public function showaffiliates($userid)
	 {
	 	
	 	//$affiliates = new Affiliation;
	 	//$agent_affiliates = $affiliates->get_affiliates_by_id($userid);
		$agent_affiliates = User::where('referbyid', $userid)->get();
	 	if(count($agent_affiliates))
	 	{
			for($i=0; $i<count($agent_affiliates); $i++)
            {
                $agent_affiliates[$i]->payment = DB::table('referalpayment')->where('ref_id', $agent_affiliates[$i]->id)->value('amount');
            }
	 		$aff_results = $agent_affiliates;
	 	}
	 	else
	 	{
	 		$aff_results = '';
	 	}
	 	return view('front.agent.affiliates',compact('aff_results'));
	 }


	 public function countaffiliates($userid)
	 {
	 	// $data = Session::all();
	 	// print_r($data);die;
	 	$cookie = Cookie::make('refid', $userid,43200);
	 	$data['agent_id'] = $userid;
	 	$data['referal_url'] = $_SERVER['SERVER_NAME'].'/referal/'.$userid;
	 	$affiliation = new Affiliation;
	 	$addresult = $affiliation->insertdata($data);
	 	if($addresult > 0)
	 	{
	 		$add_clicks = $affiliation->add_clicks($userid);
	 	}
	 	else
	 	{
	 		$add_clicks = $affiliation->add_clicks($userid);
	 	}
	 	return redirect('auth/getregister')->withCookie($cookie);
	 }

	 //function to show select package view
	public function getpackages(PackageRepository $package_gestion, $user_id)
	{
		//$packages = $package_gestion->index();
      	$ServicePackageData = new ServicePackage;
		$packages = $ServicePackageData->getServicePackage('1');
		return view('auth.packages', compact('packages','user_id'));
	}

	public function getmemberpackage(PackageRepository $package_gestion, $user_id, $pid,$amount)
	{
		return view('auth.memberpayment', compact('user_id','pid','amount'));
	}
	
	//function to add own domain name of agent into table
	public function add_domainname(Request $request)
	{
		$data['user_id'] = $request->input('agentid');
		$data['agent_domain_check'] = $request->input('owndomain');
		$data['agent_domain'] = $request->input('domain_name'); 
		$rule  =  array(
                   			'agent_domain'    => array('required', 'unique:webcontent', 'regex:/^[a-zA-Z0-9]+([a-zA-Z0-9-.]+)?.(com|org|net|mil|edu|de|COM|ORG|NET|MIL|EDU|DE|cf)$/')
                		) ;
			$validator = Validator::make($data,$rule);
			if ($validator->fails())
        	{
           		 return redirect()->back()->withErrors($validator->messages());
        	}
        	else
        	{
        		$add_agentdomain = new Content;
				$add_agentdomain_result = $add_agentdomain->addagentdomain($data);
				if($add_agentdomain_result >= 0)
				{
					return redirect()->back()->withMessages('Your website content added successfully to your own domain.');
				}
				else
				{
					return redirect()->back()->withMessage('Oops something went wrong! Please try again.');
				}
        	}
	}

	public function showaccount($userid)
	{
		$user_info = DB::table('users')->where('id', $userid)->get();
		return view('front.agent.account', compact('user_info'));
	}

	public function editagentprofile(Request $request)
	{
		$agentid = $request->input('userid');
		$data['username'] = $request->input('username');
                $data['address'] = $request->input('address');
		$data['email'] = $request->input('email');
		$data['domain_name'] = $request->input('domain_name');
		if($request->input('domain_name') != '')
		{
			$rule  =  array(
                   			'username'    => array('required', 'regex:/^[a-zA-Z\s]*$/'),
                    		'email'  => array('required','unique:users,email,'. $agentid),
                    		'domain_name'   => array('required','unique:users,domain_name,'. $agentid)
                		) ;
		}
		else
		{
			$rule  =  array(
                   			'username'    => array('required', 'regex:/^[a-zA-Z\s]*$/'),
                    		'email'  => array('required','unique:users,email,'. $agentid)
                		) ;
		}
		
			$validator = Validator::make($data,$rule);
			if ($validator->fails())
        	{
           		 return redirect()->back()->withErrors($validator->messages());
        	}
        	else
        	{
        		$query = User::where('id', $agentid)->update($data);
        		if($query > 0)
        		{
        			$user_exist = Content::where('user_id', $agentid)->get();
        			if(!empty($user_exist))
        			{
        				$domainname = $request->input('domain_name').'.users.'.$_SERVER['SERVER_NAME'];
        				Content::where('user_id', $agentid)->update(array('domain' => $domainname));
        			}
        			return redirect()->back()->withMessage('Your Profile information updated successfully.');
        		}
        		else
        		{
        			return redirect()->back()->withErrors('Oops something went wrong. Please try again.');
        		}
        	}
	}

	public function changepassword($id)
	{
		return view('front.agent.changepassword', compact('id'));
	}

	public function editagentpassword(Request $request)
	{
		$agent_id = $request->input('userid');
		$data['password'] = bcrypt($request->input('password'));
		$current_password = User::where('id', $agent_id)->value('password');
		if(\Hash::check($request->input('password'), $current_password))
		{
			return redirect()->back()->withErrors('You used this password recently. Please choose a different one.');
		}
		else
		{
			$query = User::where('id', $agent_id)->update($data);
			if($query > 0)
			{
				return redirect()->back()->withMessage('Your Password changed successfully.');
			}
			else
			{
				return redirect()->back()->withErrors('Oops something went wrong. Please try again.');
			}
		}
	}

	public function remove_domain(Request $request)
	{
		$agentid = $request->agentid;
		$query = Content::where('user_id', $agentid)->update(array('agent_domain_check' => 0, 'agent_domain' => ''));
		if($query > 0)
		{
			return redirect()->back()->withMessages('Your domain removed successfully.');
		}
		else
		{
			return redirect()->back()->withMessages('Oops something went wrong. Please try again.');
		}
	}

	/**
	 * Handle a confirmation request.
	 *
	 * @param  App\Repositories\UserRepository $user_gestion
	 * @param  string  $confirmation_code
	 * @return Response
	 */
	public function getConfirm(
		UserRepository $user_gestion,
		$confirmation_code,
		$id)
	{
		
		$status = $user_gestion->confirm($confirmation_code);
		if($status == 1)
		{
			return redirect('/content/'.$id)->with('ok', trans('front/verify.success'));
		}
		else
		{
			\App::abort(403);
		}
	}

	/**
	 * Handle a resend request.
	 *
	 * @param  App\Repositories\UserRepository $user_gestion
	 * @param  Illuminate\Http\Request $request
	 * @return Response
	 */
	public function getResend(
		UserRepository $user_gestion,
		Request $request,
		$userid)
	{
		if($userid != '')	{
			$user = $user_gestion->getById($userid);
			$this->dispatch(new SendMail($user));

			return redirect('/')->with('ok', trans('front/verify.resend'));
		}

		return redirect('/');        
	}

	public function showbanner($id)
	{
		$referal_url = 'http://'.$_SERVER['SERVER_NAME'].'/referral/'.$id;
		return view('front.agent.banners', compact('referal_url'));
	}

	public function remove_contentlogo($agent_id)
	{
		$result = Content::where('user_id', $agent_id)->update(array('logo' => ''));
		if($result > 0)
		{
			return redirect()->back()->withMessage('Your logo removed successfully.');
		}
		else
		{
			return redirect()->back()->withMessage('Oops something went wrong. Please try again.');
		}
	}
}
