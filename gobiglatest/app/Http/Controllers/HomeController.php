<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Jobs\ChangeLocale;

use App\Models\Package;
use App\Models\Referalfees;
use App\Repositories\PackageRepository; 

use App\Models\Contactus;
use App\Models\ServicePackage;
use App\Models\User;
use App\Models\Content;

use Mail;


class HomeController extends Controller
{

	/**
	 * Display the home page.
	 *
	 * @return Response
	 */
	public function index(PackageRepository $package_gestion)
	{
		$packages = $package_gestion->index();
      	$URL = 'http://'.$_SERVER['SERVER_NAME'];
		
      	if($URL == getenv('APP_URL'))
        {
        	$slider1_image = '';
			$member_id = '1';
			
        }
      	else
      	{
        	$userresult = Content::select('*')->where('domain', '=', $_SERVER['SERVER_NAME'])->get();
			$slider1_image = $userresult[0]->slider1;
			$member_id = $userresult[0]->user_id;
			
			
     	}
		$commission = Referalfees::select('monthly_bonus')->get();
		$monthly_commission = $commission[0]->monthly_bonus;
		//print_r($packages);die;
		return view('front.index',compact('packages','slider1_image','member_id','monthly_commission'));
	}

	/**
	 * Change language.
	 *
	 * @param  App\Jobs\ChangeLocaleCommand $changeLocale
	 * @param  String $lang
	 * @return Response
	 */
	public function language( $lang,
		ChangeLocale $changeLocale)
	{		
		$lang = in_array($lang, config('app.languages')) ? $lang : config('app.fallback_locale');
		$changeLocale->lang = $lang;
		$this->dispatch($changeLocale);
		return redirect()->back();
	}
	public function selecttheme($theme_id)
	{
		return view('themea.index',compact('theme_id'));
		
	}
	public function showsellhome($theme_id)
	{
		return view('themea.user-page-sell-your-home',compact('theme_id'));
		
	}
	public function showcashoffer($theme_id)
	{
		return view('themea.user-page-cash-offer',compact('theme_id'));
		
	}

	public function showquestions($theme_id)
	{
		return view('themea.user-page-questions',compact('theme_id'));
	}

	public function showhowitworks($theme_id)
	{
		return view('themea.user-page-how-its-work',compact('theme_id'));
	}

	public function showtestimonial($theme_id)
	{
		return view('themea.user-page-testimonials',compact('theme_id'));
	}

	public function showaboutus($theme_id)
	{
		return view('themea.aboutus',compact('theme_id'));
	}
	public function get_tryitfree()
	{     
	    //$URL = $_SERVER['SERVER_NAME'];
	    //$URL = 'swati';
		//$userresult = User::select('users.id')->where('users.domain_status','=',1)->where('users.domain_name', '=', $URL)->get();
		//$userresult = Content::select('webcontent.user_id')->where('webcontent.domain', '=', $URL)->get();
    // echo($userresult);die;
      //echo count($userresult);die;
		//if(count($userresult) == 0){
			//$user_id=1;
		//}else{ 
			 //$user_id = $userresult[0]['user_id'];
			
		//}
	    $ServicePackageData = new ServicePackage;
		$Packageresult = $ServicePackageData->getServicePackage('1');
		//print_r($Packageresult);die;
		return view('front.tryitfree',compact('Packageresult'));
		
	}

	
	public function get_aboutus()
	{
		$url = $_SERVER['SERVER_NAME'];
		return view('front.aboutus',compact('url'));
	}

	public function get_contactus()
	{
		return view('front.contact');
	}

	public function get_pricing(PackageRepository $package_gestion)
	{
		$packages = $package_gestion->index();
		return view('front.pricing', compact('packages'));
	}

	public function get_testimonials()
	{
		return view('front.testimonials');
	}

	public function get_affiliateprogram()
	{
		return view('front.affiliateprogram');
	}

	public function get_affiliateterm()
	{
		return view('front.affiliateterm');
	}

	public function add_contactus(Request $request)
	{
		$data['name'] = $request->input('name');
		$data['email'] = $request->input('email');
		$data['message'] = $request->input('message');
		$query = Contactus::insertGetId($data);
		if($query > 0)
		{
			$datas = [
            'title'  => $data['name']
        ];
         Mail::send('emails.contactusmail', $datas, function($message) use($data) {
            $message->to($data['email'])->subject('Contact Us Email');
            $message->from('support@GoBigPrinting.com','GobigWeb');
        });
			return redirect()->back()->withMessage('Your Query submitted successfully.');
		}
	}

	public function get_privacypolicy()
	{
		return view('front.privacypolicy');
	}

	public function get_website_free()
	{
		return view('front.getfreewebsite');
	}

	public function get_faq()
	{
		return view('front.getfaq');
	}

	public function get_howitworks()
	{
		return view('front.howitworks');
	}
}
