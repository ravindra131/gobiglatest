<?php namespace App\Http\Controllers;

use App\Repositories\ContactRepository;
use App\Repositories\UserRepository;
use App\Repositories\BlogRepository;
use App\Repositories\CommentRepository;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller {

    /**
     * The UserRepository instance.
     *
     * @var App\Repositories\UserRepository
     */
    protected $user_gestion;

    /**
     * Create a new AdminController instance.
     *
     * @param  App\Repositories\UserRepository $user_gestion
     * @return void
     */
    public function __construct(UserRepository $user_gestion)
    {
		$this->user_gestion = $user_gestion;
    }

	/**
	* Show the admin panel.
	*
	* @param  App\Repositories\ContactRepository $contact_gestion
	* @param  App\Repositories\BlogRepository $blog_gestion
	* @param  App\Repositories\CommentRepository $comment_gestion
	* @return Response
	*/
	public function admin()
	{	
		
		$nbrUsers = $this->user_gestion->getNumber();
		return view('back.index', compact('nbrUsers'));
	}

	/**
	 * Show the media panel.
	 *
     * @return Response
	 */
	public function filemanager()
	{
		$url = config('medias.url') . '?langCode=' . config('app.locale');
		
		return view('back.filemanager', compact('url'));

	}
       
        //function to get view of admin edit profile
	public function getprofile()
	{
		$userid = \Auth::user()->id;
		$results = User::where('id','=',$userid)->get();
		if(count($results))
		{
			$profile_data = $results;
		}
		else
		{
			$profile_data = '';
		}
		return view('back.admin.profile', compact('profile_data'));
	}

	//function to edit profile of admin
	public function editprofile(Request $request)
	{
		$user_id = $request->input('userid');
		$data['username'] = $request->input('fullname');
		$data['email'] = $request->input('email');
		$rule  =  array(
                   		'username'    => array('required', 'regex:/^[a-zA-Z\s]*$/'),
                    		'email'  => array('required','unique:users,email,'. $user_id)
                		) ;
		$validator = Validator::make($data,$rule);
		if ($validator->fails())
                {
                   return redirect()->back()->withErrors($validator->messages());
                 }
               else
               {
		  $updated_result = User::where('id','=',$user_id)->update($data);
		  if($updated_result > 0)
		  {
		    return redirect()->back()->withMessage('Profile updated successfully.');
		  }
	          else
		  {
		    return redirect()->back()->withErrors('Oops something went wrong. Please try again.');
		  }
		}
	}

	//function to get view of admin change password
	public function changepassword()
	{
		return view('back.admin.changepassword');
	}

	//function to edit password of admin
	public function editpassword(Request $request)
	{
		$uid = \Auth::user()->id;
		$data['password'] = bcrypt($request->input('password'));
		$current_password = User::where('id','=',$uid)->value('password');
		if(\Hash::check($request->input('password'), $current_password))
		{
			return redirect()->back()->withErrors('You used this password recently. Please choose a different one.');
		}
		else
		{
			$result = User::where('id','=',$uid)->update($data);
			if($result > 0)
			{
				return redirect()->back()->withMessage('Password changed successfully.');
			}
			else
			{
				return redirect()->back()->withErrors('Oops something went wrong. Please try again.');
			}
		}
	}


}
