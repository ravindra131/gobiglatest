<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Visitors;

use App\Models\Emailcontent;

use App\Models\Uploads;

use App\Models\Content;

use App\Models\Sendemail;

use DB, Mail;

use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Redirect;

class VisitorsController extends Controller
{
    public function index()
    {
    	$visitors = new Visitors;
    	$vresults = $visitors->getallvisitors();
    	return view('back.visitors.showallvisitors', compact('vresults'));
    }
	public function addvisitor(Request $request)
	{
		$data['agent_id'] = $request->input('memberid');
		$data['firstname'] = $request->input('firstname');
		$data['email'] = $request->input('email');
		$rule  =  array(
                   			'firstname'    => 'required|regex:/^[a-zA-Z\s]*$/',
                    		'email' => 'required|email'
                		) ;
		$validator = Validator::make($data,$rule);
		if ($validator->fails())
        {

            return redirect()->back()->withErrors($validator->messages());

        }
        else
        {
        	$check_email_exists = Visitors::whereemail($request->input('email'))->where('agent_id', '=', $request->input('memberid'))->count();
        	if ($check_email_exists)
        	{
        		return redirect()->back()->withEmailer('This email already exists. Please choose another email.');     
        	}
        	else
        	{
            	$register = new Visitors;
				$result = $register->saveData($data);
				if($result > 0)
				{
    				$email_content_result = new Emailcontent;
					$email_content = $email_content_result->showall();
					if(count($email_content))
					{
						foreach($email_content as $row)
						{
							$sendemail = new Sendemail;
							$sendemail->visitor_id = $result;
							$sendemail->template_id = $row->id;
							$sendemail->email_status = 0;
							$sendemail->email_date = date('Y-m-d',strtotime(date('Y-m-d').' +'.$row->duration.' day '));						
							$sendemail->save();				
						
						}
					}
					\Artisan::call('cron:email');
					return Redirect::to('/add-visitor/thankyou');
				}			
				else
				{
					return redirect()->back()->withMessage('Oops something went wrong!');     
				}
			}
		}
	}
	
	public function showthankyou()
	{
		$upload = new Uploads;
		$fileresult = $upload->getfile();
		$pdffile = $fileresult[0]->filepath;
		
		return view('front.thankyou', compact('pdffile'));
			
		
	}

    public function showemailcampaign()
    {
    	$emailcampaign = new Emailcontent;
    	$email_result = $emailcampaign->showall();
    	if(!empty($email_result))
    	{
    		$eresults = $email_result;
    	}
    	else
    	{
    		$eresults = '';
    	}
    	return view('back.visitors.listsetupemail', compact('eresults'));
    }
    public function editemailcampaign($id)
    {
    	$editemail = new Emailcontent;
    	$edit_result = $editemail->showcontentbyid($id);
    	return view('back.visitors.editemailsetup', compact('edit_result'));
    }
    public function editemailcontent(request $request)
    {
    	$data['id'] = $request->input('cid');
    	$data['subject'] = $request->input('subject');
    	$data['message'] = $request->input('area');
    	$data['duration'] = $request->input('duration');
    	$data['status'] = $request->input('status');
    	$rule  =  array(
                   			'subject'    => 'required',
                    		'message'  => 'required',
                    		'duration' => 'required',
                    		'status' => 'required'
                		) ;
		$validator = Validator::make($data,$rule);
		if ($validator->fails())
        {
           	return redirect()->back()->withErrors($validator->messages());
        }
        else
        {
        	$upemailcontent = new Emailcontent;
        	$updated_result = $upemailcontent->update_emailcontent($data);
        	if($updated_result >= 0)
        	{
                if($request->input('testid') == '')
                {
                    return Redirect::to('/email-campaign')->withMessage('Email content updated successfully.');
                }
                else
                {
                    $data['email'] = $request->input('email');
                    Mail::send('emails.testemail', array('msg' => $data['message']),   function($message) use($data)
                    {
                        $message->from('gobigweb3@gmail.com', 'GoBigweb');
                        $message->to($data['email'])->subject($data['subject']);

                     });
                    return redirect()->back()->withMessage('Test email sent successfully.');
                }
        	}
        	else
        	{
        		return redirect()->back()->withErrors('Oops something went wrong!');
        	}
        }
    }
    public function setupemail()
    {
    	return view('back.visitors.setupemail');
    }
    public function addemailcontent(Request $request)
    {
    	$data['subject'] = $request->input('subject');
    	$data['message'] = $request->input('area');
    	$data['duration'] = $request->input('duration');
    	$data['status'] = $request->input('status');
    	$rule  =  array(
                   			'subject'    => 'required',
                    		'message'  => 'required',
                    		'duration' => 'required',
                    		'status' => 'required'
                		) ;
		$validator = Validator::make($data,$rule);
		if ($validator->fails())
        {
           	return redirect()->back()->withErrors($validator->messages());
        }
        else
        {
        	$emailcontent = new Emailcontent;
        	$insert_result = $emailcontent->insertemailcontent($data);
        	if($insert_result > 0)
        	{
                if($request->input('testid') == '')
                {
                    return Redirect::to('/email-campaign')->withMessage('Email content added successfully.');
                } 
                else
                {
                    $data['email'] = $request->input('email');
                    Mail::send('emails.testemail', array('msg' => $data['message']),   function($message) use($data)
                    {
                        $message->from('gobigweb3@gmail.com', 'GoBigweb');
                        $message->to($data['email'])->subject($data['subject']);

                     });
                    return Redirect::to('/edit-emailsetup/'.$insert_result)->withMessage('Test email sent successfully.');
                }    
        	}
        	else
        	{
        		return redirect()->back()->withErrors('Oops something went wrong!');
        	}
        }
    }

    public function delete_emailcontent($id)
    {
    	$deletecontent = new Emailcontent;
        $deletecontent->delete_emailcontent($id);
       	return Redirect::to('/email-campaign')->withMessage('Email content deleted successfully.'); 
    }

    public function testemailsetup($temp_id)
    {
        return view('back.visitors.testemailsetup',compact('temp_id'));
    }

    public function sendtestmail(Request $request)
    {
        $tempid = $request->input('tempid');
        $data['email'] = $request->input('email');
        $rule  =  array(
                            'email' => 'required|email'
                        ) ;
        $validator = Validator::make($data,$rule);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->messages());
        }
        else
        {
            $allcontent = new Emailcontent;
            $testcontent = $allcontent->showcontentbyid($tempid);
            if(!empty($testcontent))
            {
                Mail::send('emails.testemail', array('msg' => $testcontent[0]->message),   function($message) use($data, $testcontent)
                {
                    $message->from('gobigweb3@gmail.com', 'GoBigweb');
                    $message->to($data['email'])->subject($testcontent[0]->subject);

                });
                return redirect()->back()->withMessage('Test email sent successfully!');
            }
            else
            {
                return redirect()->back()->withErrors('Please set the email content first!');
            }
        }
        
    }

    public function searchvisitors(Request $request)
    {
        $search = $request->SearchText;
        $visitorsname = $request->visitorsname;
		$visitorsemail = $request->visitorsemail;
        $record_no = $request->record;
        $main_query = Visitors::select('visitors.*', 'users.username as agentname','users.domain_name as domainurl','users.id')                   
                    ->join('users', 'users.id', '=', 'visitors.agent_id')->orderBy('visitors.id', 'desc');
                   
        if($search != '' || $search != null)
        {
            $main_query->where(function ($query) use ($search) {
                    $query->where('users.username', 'like', '%'.$search.'%');
                    });
        }
         if($visitorsemail != '' || $visitorsemail != null)
        {
            $main_query->where(function ($query) use ($visitorsemail) {
                    $query->where('visitors.email', 'like', '%'.$visitorsemail.'%');
                    });
        }
         if($visitorsname != '' || $visitorsname != null)
        {
            $main_query->where(function ($query) use ($visitorsname) {
                    $query->where('visitors.firstname', 'like', '%'.$visitorsname.'%');
                    });
        }
        if($record_no != '' || $record_no != null)
        {
            $vresults = $main_query->paginate($record_no);
        }
        else
        {
             $vresults = $main_query->paginate(10);
        }
       
        if($request->ajax())
        {
            $data = [];
            $data['status'] = 'success';
            $data['html'] = \View::make('back.visitors.part')->withVresults($vresults)->render();
            $data['count'] = count($vresults);
        }
        else
        {
            return view('back.visitors.showallvisitors', compact('vresults'));
        }

        return response()->json($data);
    }

    public function download_visitors(Request $request)
    {
        $search = $request->SearchText;
        $visitorsemail = $request->visitorsemail;
        $phone_no = $request->phone_no;
        $record_no = $request->record;
        $main_query = Visitors::select('visitors.*','users.id','users.username as agentname')
                ->join('users','visitors.agent_id','=','users.id')->orderBy('visitors.id', 'desc')
                ->where('users.id','!=','');
                   
        if($search != '' || $search != null)
        {
            $main_query->where(function ($query) use ($search) {
                    $query->where('users.username', 'like', '%'.$search.'%');
                    });
        }
         if($visitorsemail != '' || $visitorsemail != null)
        {
            $main_query->where(function ($query) use ($visitorsemail) {
                    $query->where('visitors.email', 'like', '%'.$visitorsemail.'%');
                    });
        }
         if($phone_no != '' || $phone_no != null)
        {
            $main_query->where(function ($query) use ($phone_no) {
                    $query->where('visitors.phone', 'like', '%'.$phone_no.'%');
                    });
        }
        $visitors = $main_query->get();
        $filename = 'visitors-'.time().'.csv';
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('Agentname', 'Visitor Email', 'Visitor Address', 'Visitor Phone Number', 'Agent Notes'));
        foreach ($visitors as $row) 
        {
            fputcsv($handle, array($row->agentname, $row->email, $row->address, $row->phone, $row->notes));
        }
        fclose($handle);

            $headers = array(
                        'Content-Type' => 'text/csv',
                    );

            return \Response::download($filename, $filename, $headers)->deleteFileAfterSend(true);
    }
}
