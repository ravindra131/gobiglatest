<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Libraries\PaypalPro;

use App\Http\Requests;

use App\Models\Affiliation;

use App\Models\Referalfees; 
use App\Models\Referalpayment;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Validator;
use DB;
use App\Models\Payment;
use App\Models\User;
class PaypalController extends Controller {
    
    
    
    public function process()
    {
        return view('testpaypal');
    }
    
    public function success(Request $request)
    {
	if($_SERVER['REQUEST_METHOD'] == 'POST'){      
    $payableAmount = $request->input('payableamount');
    $nameArray = explode(' ',$request->input('name_on_card'));
    //Buyer information    
    $lastName = (count($nameArray) > 1) ? $nameArray[count($nameArray) - 1] : '';
    if (count($nameArray) > 1) {
            unset($nameArray[count($nameArray) - 1]);
    }
    $firstName = (count($nameArray) > 1) ? implode(' ', $nameArray) : $nameArray[0];
    $city = 'Kolkata';
    $zipcode = '700091';
    $countryCode = 'US';
    
    //Create an instance of PaypalPro class
    $paypal = new PaypalPro;
	
	//Payment details
	
	$paypalParams = array(        
        'PROFILESTARTDATE' => gmdate(DATE_ATOM), //  #Billing date start, in UTC/GMT format       
        'BILLINGFREQUENCY' => 1, 
        'amount' => $payableAmount,
        'MAXFAILEDPAYMENTS' => 3,
        'currencyCode' => 'USD',
        'creditCardType' => $request->input('card_type'),
        'creditCardNumber' => trim(str_replace(" ","",$request->input('card_number'))),
        'expMonth' => $request->input('expiry_month'),
        'expYear' => $request->input('expiry_year'),
        'cvv' => $request->input('cvv'),
        'firstName' => $firstName,
        'lastName' => $lastName,
        'city' => $city,
        'zip'   => $zipcode,
        'countryCode' => $countryCode
        );
		
    if($request->input('package_id') == 1)
    {
        $paypalParams['DESC'] = 'Gobig Web Monthly package';
        $paypalParams['BILLINGPERIOD'] = 'Month';
      
    }
    if($request->input('package_id') == 2)
    {
        $paypalParams['DESC'] = 'Gobig Web Annual package';
        $paypalParams['BILLINGPERIOD'] = 'Year';
        
    }
    

    //$response = $paypal->paypalCall($paypalParams);
    $response = $paypal->paypalRecurringCall($paypalParams);
    $paymentStatus = strtoupper($response["ACK"]);
    if ($paymentStatus == "SUCCESS")
    {
        $data['status'] = 1;
	$transactionID = $response['PROFILEID'];
        $payment = new Payment;
        $payment->user_id = $request->input('user_id');
        $payment->package_id = $request->input('package_id');
        $payment->amount = $payableAmount;
        $payment->transaction_id = $transactionID; 
        //$payment->save();
        if($payment->save())
        {
            $domain = new User;
            $domain->update_domain_status($request->input('user_id'));
            $referal_id = $domain->getagent_refid($request->input('user_id'));
            if($referal_id > 0)
            {
                $referal_fees = new Referalfees;
                $referal_fee_result = $referal_fees->getalldata();
                if(!empty($referal_fee_result))
                {
                    $ref_payment = new Referalpayment;
                    $ref_payment->agent_id = $referal_id;
                    $ref_payment->ref_id = $request->input('user_id');
                    if($request->input('package_id') == 1)
                    {
                        $ref_payment->amount = $referal_fee_result[0]->monthly_bonus;
                    }
                    else if($request->input('package_id') == 2)
                    {
                        $ref_payment->amount = $referal_fee_result[0]->annual_bonus;
                    }
                    $ref_payment->paid = 0;
                    $ref_payment->save();
                    //$referal_payment_data = $ref_payment->getamount_byagentid($referal_id);
                    //$referal_due_data = $ref_payment->getdueamount_byagentid($referal_id);
                    //$totalpayment = new Affiliation;
                    //$totalpayment->add_payment($referal_id);
                    //$totalpayment->update_earning($referal_id,$referal_payment_data);
                    //$totalpayment->update_due($referal_id,$referal_due_data);
                }
            }
        }
    }
    else
    {
        $data['status'] = 0;
    }

    echo json_encode($data);
  }
        
    }
    
    public function cancel()
    {
        
    }
    
    
    
}
