<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\PackageRepository;
use App\Models\Package;
use App\Models\Uploads;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Input;
use Response;
class PackageController extends Controller
{
    /**
	 * Create a new ContactController instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('admin', ['except' => ['create', 'store']]);
		$this->middleware('ajax', ['only' => 'update']);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param  ContactRepository $contact_gestion
	 * @return Response
	 */
	public function index(PackageRepository $package_gestion)
	{
		$messages = $package_gestion->index(); 
		return view('back.packages.index', compact('messages'));
	}
	public function edit(Package $package)
	{
		return view('back.packages.edit', array_merge(compact('package')));

	}
	

	public function uploadpdf()
	{
		$upload = new Uploads;
		$result = $upload->getfile();
		if(!empty($result))
		{
			$filename = $result[0]->filepath;
			
		}
		else
		{
			$filename = '';
		}
		return view('back.uploadpdf', compact('filename'));
	}
	public function uploadfile(Request $request)
	{
		$file = Input::file('image');
		$file_name = $request->input('filename');
		$input = array('file' => $file);
		$rules = array(
			'file' => 'required|mimes:pdf'
		);
		$validator = Validator::make($input, $rules);
		if( $validator->fails() )
		{
			return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);
 
		}
		else {
			$destinationPath = 'uploads/';
			$filename = $file->getClientOriginalName();
			Input::file('image')->move($destinationPath, $filename);
			$data['filepath'] = '/uploads/' . $filename;
			$upload = new Uploads;
			if($file_name == '')
			{
				$result = $upload->savefile($data);
				if($result >= 1)
				{
					return Response::json(['success' => true, 'file' => asset($destinationPath.$filename)]);
				}
			}
			else
			{
				$upload->updatefile($data,$file_name);
				return Response::json(['success' => true, 'file' => asset($destinationPath.$filename)]);
			}
			
		}
	}
	public function updatePackage(Request $request)
	{
		$monthly_fees = $request->input('monthly_fee');
		$annual_fees = $request->input('annual_fee');
		if(($monthly_fees*12) > $annual_fees)
		{
			$packages = new Package;
			$packages->updatepackage($request->all());
			return Redirect::to('package')->withMessage('Package fees updated successfully.');
		}
		else
		{
			return redirect()->back()->withErrors('Annual fee value should be smaller than or equal to (Monthly fee*12), because discount can not be in negative.');
		}
	}
	
}
