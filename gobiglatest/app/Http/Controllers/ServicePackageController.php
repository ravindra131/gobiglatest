<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\ServicePackageRepository;
use App\Models\ServicePackage;
use App\Models\Uploads;
use App\Models\ServiceReferalfee;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Input;
use Response;
class ServicePackageController extends Controller
{
    /**
	 * Create a new ContactController instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('admin', ['except' => ['create', 'store']]);
		$this->middleware('ajax', ['only' => 'update']);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param  ContactRepository $contact_gestion
	 * @return Response
	 */
	public function index(ServicePackageRepository $package_gestion)
	{
		$messages = $package_gestion->index();
		$userpackagedata=new ServicePackage();
		$userdata=$userpackagedata->getUserPackage();
		//print_r($userdata);die;
		return view('back.servicepackages.index')->with(['messages'=>$messages,'userdata'=>$userdata]);
	}
	public function edit(ServicePackage $servicepackage)
	{ 
		return view('back.servicepackages.edit', array_merge(compact('servicepackage')));

	}
	

	public function uploadpdf()
	{
		$upload = new Uploads;
		$result = $upload->getfile();
		if(!empty($result))
		{
			$filename = $result[0]->filepath;
			
		}
		else
		{
			$filename = '';
		}
		return view('back.uploadpdf', compact('filename'));
	}
	public function uploadfile(Request $request)
	{
		$file = Input::file('image');
		$file_name = $request->input('filename');
		$input = array('file' => $file);
		$rules = array(
			'file' => 'required|mimes:pdf'
		);
		$validator = Validator::make($input, $rules);
		if( $validator->fails() )
		{
			return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);
 
		}
		else {
			$destinationPath = 'uploads/';
			$filename = $file->getClientOriginalName();
			Input::file('image')->move($destinationPath, $filename);
			$data['filepath'] = '/uploads/' . $filename;
			$upload = new Uploads;
			if($file_name == '')
			{
				$result = $upload->savefile($data);
				if($result >= 1)
				{
					return Response::json(['success' => true, 'file' => asset($destinationPath.$filename)]);
				}
			}
			else
			{
				$upload->updatefile($data,$file_name);
				return Response::json(['success' => true, 'file' => asset($destinationPath.$filename)]);
			}
			
		}
	}
	public function updatePackage(Request $request)
	{ 
		$monthly_fees = $request->input('monthly_fee');
		$annual_fees = $request->input('annual_fee');
		if(($monthly_fees*12) > $annual_fees)
		{
			$packages = new ServicePackage;
			$packages->updatepackage($request->all());
			return Redirect::to('servicepackage')->withMessage('Service package fees updated successfully.');
		}
		else
		{
			return redirect()->back()->withErrors('Annual fee value should be smaller than or equal to (Monthly fee*12), because discount can not be in negative.');
		}
	}

	public function showrefview()
	{
		$allfees = new ServiceReferalfee;
        $result = $allfees->getallfees();
        if(!empty($result))
        {
            $fees = $result;
        }
        else
        {
            $fees = '';
        }
    	return view('back.servicepackages.servicereferalfee', compact('fees'));
	}

	 public function showfees()
    {
    	return view('back.servicepackages.addservicereferalfee');
    }

    public function setfees(Request $request)
    {
    	$data['type'] = $request->input('type');
    	$data['monthly_bonus'] = $request->input('monthly_bonus');
        $data['annual_bonus'] = $request->input('annual_bonus');
    	$rule  =  array(
                   			'type'       => 'required',
                    		'monthly_bonus'      =>  array('required', 'regex:/^[0-9]*$/'),
                            'annual_bonus' => array('required', 'regex:/^[0-9]*$/')
                		);
		$validator = Validator::make($data,$rule);
		if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->messages());
        }
        else
        {
            $rfee = new ServiceReferalfee;
			$rfee->savefeeData($data);
			return Redirect::to('/service-referal-fee')->withMessage('Service Referral Fee inserted successfully.');      
        }
    }

    public function editrefview($feeid)
    {
        $editfees = new ServiceReferalfee;
        $fees = $editfees->getfeesbyid($feeid);
        return view('back.servicepackages.editservicereferalfee', compact('fees'));
    }

    public function editreffees(Request $request)
    {
        $data['feeid'] = $request->input('feeid');
        $data['type'] = $request->input('type');
        $data['monthly_bonus'] = $request->input('monthly_bonus');
        $data['annual_bonus'] = $request->input('annual_bonus');
        $rule  =  array(
                            'type'       => 'required',
                            'monthly_bonus'      =>  array('required', 'regex:/^[0-9]*$/'),
                            'annual_bonus' => array('required', 'regex:/^[0-9]*$/')
                        );
        $validator = Validator::make($data,$rule);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->messages());
        }
        else
        {
            $updated_fee = new ServiceReferalfee;
            $updated_fee->updatefeeData($data);
            return Redirect::to('/service-referal-fee')->withMessage('Service Referral fee updated successfully.');      
        }
    }

    public function deletefees($feesid)
    {
        $deletefees = new ServiceReferalfee;
        $deletefees->delete_fees($feesid);
        return Redirect::to('/service-referal-fee')->withMessage('Service Referral Fee deleted successfully.');
    }
	
}
