<?php namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use App\Repositories\PackageRepository;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\RoleRequest;
use App\Models\User;
use App\Models\Content;
use DB;
use App\Models\Payment;
use App\Models\ServicePackage;
use Illuminate\Http\Request;
use App\Jobs\SendMail;
use App\Libraries\PaypalPro;
use Mail;
class UserController extends Controller {

	/**
	 * The UserRepository instance.
	 *
	 * @var App\Repositories\UserRepository
	 */
	protected $user_gestion;

	/**
	 * The RoleRepository instance.
	 *
	 * @var App\Repositories\RoleRepository
	 */	
	protected $role_gestion;

	/**
	 * Create a new UserController instance.
	 *
	 * @param  App\Repositories\UserRepository $user_gestion
	 * @param  App\Repositories\RoleRepository $role_gestion
	 * @return void
	 */
	public function __construct(
		UserRepository $user_gestion,
		RoleRepository $role_gestion)
	{
		$this->user_gestion = $user_gestion;
		$this->role_gestion = $role_gestion;

		$this->middleware('admin');
		$this->middleware('ajax', ['only' => 'updateSeen']);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return $this->indexSort('total');
	}

	/**
	 * Display a listing of the resource.
	 *
     * @param  string  $role
	 * @return Response
	 */
	public function indexSort($role)
	{
		// $counts = $this->user_gestion->counts();
		$users = $this->user_gestion->index(10, $role); 
		$links = $users->render();
		//$roles = $this->role_gestion->all();
		return view('back.users.index', compact('users', 'links'));		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(PackageRepository $package_gestion)
	{
		$packages = $package_gestion->index();
		return view('back.users.create', compact('packages'), $this->role_gestion->getAllSelect());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  App\requests\UserCreateRequest $request
	 *
	 * @return Response
	 */
	public function store(
		UserCreateRequest $request,
		UserRepository $user_gestion)
	{
		$user = $user_gestion->store($request->all(),$confirmation_code = str_random(30));
		$this->dispatch(new SendMail($user));
		return redirect('user')->with('ok', trans('back/users.created'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  App\Models\User
	 * @return Response
	 */
	public function show(User $user)
	{
		$content = new Content;
		$contentdata = $content->getcontent($user->id);
		if(!empty($contentdata))
		{
			$conresults = $contentdata;
		}
		else
		{
			$conresults = '';
		}
		return view('back.users.show',  compact('user','conresults'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  App\Models\User
	 * @return Response
	 */
	public function edit(User $user, PackageRepository $package_gestion)
	{
		$packages = $package_gestion->index();
		$content = new Content;
		$editresult = $content->get_some_content($user->id);
		if(!empty($editresult))
		{
			$companyname = $editresult[0]->companyname;
			$facebookurl = $editresult[0]->facebookurl;
			$twitterurl = $editresult[0]->twitterurl;
			$youtubeurl = $editresult[0]->youtubeurl;
			$linkedinurl = $editresult[0]->linkedinurl;
			$googleurl = $editresult[0]->googleurl;
			$instaurl = $editresult[0]->instaurl;
			$logo = $editresult[0]->logo;
			$slider = $editresult[0]->slider1;
		}
		else
		{
			$companyname = '';
			$facebookurl = '';
			$twitterurl = '';
			$youtubeurl = '';
			$linkedinurl = '';
			$googleurl = '';
			$instaurl = '';
			$logo = '';
			$slider = '';
		}
		$payment_id_result = DB::table('payment')->where('user_id', $user->id)->orderBy('id','desc')->value('id');
		$package_id_result = DB::table('payment')->where('user_id', $user->id)->orderBy('id','desc')->value('package_id');
		$transaction_id_result = DB::table('payment')->where('user_id', $user->id)->orderBy('id','desc')->value('transaction_id');
		if($payment_id_result != '' && $package_id_result != '' && $transaction_id_result != '')
		{
			$payment_id = $payment_id_result;
			$package_id = $package_id_result;
			$transactionid = $transaction_id_result;
		}
		else
		{
			$payment_id = '';
			$package_id = '';
			$transactionid = '';
		}
		$payment_info = Payment::where('user_id','=', $user->id)->where('transaction_id','!=',0)->orderBy('id','desc')->get();
		$paypal_profile_status = [];
		if(count($payment_info))
		{
			$profileid = $payment_info[0]->transaction_id;
			$paypal = new PaypalPro;	
			$paypalParams = ['PROFILEID' => $profileid];
			$paypal_profile = $paypal->getRecurringProfileStatus($paypalParams);
			if($paypal_profile['ACK'] == 'Success' && $paypal_profile['STATUS'] != 'Cancelled')
			{
				$paypal_profile_status = $paypal_profile;
			}
			
		}
		return view('back.users.edit', array_merge(compact('user','packages','package_id','payment_id','paypal_profile_status','transactionid'), ['companyname' => $companyname, 'facebookurl' => $facebookurl, 'twitterurl' => $twitterurl, 'youtubeurl' => $youtubeurl, 'linkedinurl' => $linkedinurl, 'googleurl' => $googleurl, 'instaurl' => $instaurl, 'slider' => $slider, 'logo' => $logo], $this->role_gestion->getAllSelect()));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  App\requests\UserUpdateRequest $request
	 * @param  App\Models\User
	 * @return Response
	 */
	public function update(
		UserUpdateRequest $request,
		User $user)
	{
		$this->user_gestion->update($request->all(), $user);
                $status = User::where('id',$user->id)->value('domain_status');
		if($status == '0')
		{
			$agent_email = $user->email;
			$agent_name = $user->username;
			$datas = [
            				'title'  => $agent_name
        				 ];
			Mail::send('emails.statuschangemail', $datas, function($message) use($agent_email)
			{
            	$message->to($agent_email)->subject('Status Inactive');
        	});
		}
		return redirect('user')->with('ok', trans('back/users.updated'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Illuminate\Http\Request $request
	 * @param  App\Models\User $user
	 * @return Response
	 */
	public function updateSeen(
		Request $request, 
		User $user)
	{
		$this->user_gestion->updateseen($request->all(), $user);

		return response()->json();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  App\Models\user $user
	 * @return Response
	 */
	public function destroy(User $user)
	{
		$this->user_gestion->destroyUser($user);

		return redirect('user')->with('ok', trans('back/users.destroyed'));
	}

	/**
	 * Display the roles form
	 *
	 * @return Response
	 */
	public function getRoles()
	{
		$roles = $this->role_gestion->all();

		return view('back.users.roles', compact('roles'));
	}

	/**
	 * Update roles
	 *
	 * @param  App\requests\RoleRequest $request
	 * @return Response
	 */
	public function postRoles(RoleRequest $request)
	{
		$this->role_gestion->update($request->except('_token'));
		
		return redirect('user/roles')->with('ok', trans('back/roles.ok'));
	}

	public function searchusers(Request $request)
	{
		$search = $request->SearchText;
		$record_no = $request->record;
        $main_query = User::select('*')->orderBy('id', 'desc');
                   
        if($search != '' || $search != null)
        {
            $main_query->where(function ($query) use ($search) {
                    $query->where('username', 'like', '%'.$search.'%');
                    });
        }
        if($record_no != '' || $record_no != null)
        {
            $users = $main_query->paginate($record_no);
        }
        else
        {
            $users = $main_query->paginate(10);
        }
        $links = $users->render();		
        if($request->ajax())
        {
        	$data = [];
        	$data['status'] = 'success';
			$data['html'] = \View::make('back.users.table')->with( compact('users', 'links'))->render();
        	$data['count'] = count($users);
        }
        else
        {
			return view('back.users.index', compact('users', 'links'));	
        }

        return response()->json($data);
	}

	public function download_users(Request $request)
	{
		$search = $request->SearchText;
		$main_query = User::select('*')->orderBy('id', 'desc');
		if($search != '' || $search != null)
        {
            $main_query->where(function ($query) use ($search) {
                    $query->where('username', 'like', '%'.$search.'%');
                    });
        }
            $users = $main_query->get();
            $filename = 'users-'.time().'.csv';
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('Agentname', 'Domain Name', 'Email Address', 'Company Name', 'Phone', 'Address', 'City', 'State', 'Zipcode'));
        foreach ($users as $row) 
        {
        	if($row->id != 1 && $row->id != 2 && $row->id != 3 && $row->id != 4)
        	{
        		fputcsv($handle, array($row->username, 'http://'.$row->domain_name.'.users.'.$_SERVER['SERVER_NAME'], $row->email, $row->company, $row->phoneno, $row->address, $row->cityname, $row->state, $row->zipcode));
        	}
        }
        fclose($handle);

            $headers = array(
                        'Content-Type' => 'text/csv',
                    );

            return \Response::download($filename, $filename, $headers)->deleteFileAfterSend(true);
	}

}
