<?php namespace App\Http\Requests;

class UserCreateRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'username' => 'required|max:30|regex:/^[a-zA-Z\s]*$/',
			'email' => 'required|email|unique:users',
			'password' => 'required|confirmed|min:6',
			'domain_name' => 'required|alpha_num|unique:users',
			'facebookurl' => 'regex:/^(https?:\/\/)?(www\.)?facebook.com\//',
			'twitterurl' => 'regex:/^(https?:\/\/)?(www\.)?twitter.com\//',
			'youtubeurl' => 'regex:/^(https?:\/\/)?(www\.)?youtube.com\//',
			'linkedinurl' => 'regex:/^(https?:\/\/)?(www\.)?linkedin.com\//',
			'googleurl' => 'regex:/^(https?:\/\/)?(www\.)?plus.google.com\//',
			'instaurl' => 'regex:/^(https?:\/\/)?(www\.)?instagram.com\//',
		];
	}

}