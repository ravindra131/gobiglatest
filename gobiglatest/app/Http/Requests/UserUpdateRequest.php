<?php namespace App\Http\Requests;

class UserUpdateRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$id = $this->user->id;
		return $rules = [
			'username' => 'required|max:30|regex:/^[a-zA-Z\s]*$/', 
			'email' => 'required|email|unique:users,email,' . $id,
			'domain_name' => 'required|alpha_num|unique:users,domain_name,' .$id
		];
	}

}
