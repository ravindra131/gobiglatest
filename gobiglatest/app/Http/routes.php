<?php

Route::group(['middleware' => ['web','Checkdomain']], function () {

	// for Home
	Route::get('/', [
		'uses' => 'HomeController@index', 
		'as' => 'home'
	]);
        
        Route::group(['prefix' => 'paypal'], function () {	
            
        // for process
	Route::get('/', [
		'uses' => 'PaypalController@process', 
		'as' => 'processpaypal'
	]);
        
        Route::post('/success', [
		'uses' => 'PaypalController@success', 
		'as' => 'sucesspaypal'
	]);
        
        Route::post('/cancel', [
		'uses' => 'PaypalController@cancel', 
		'as' => 'cancelpaypal'
	]);
        
        
        });

	//for themes demo
	Route::get('/theme/{theme_id}', 'HomeController@selecttheme');
	Route::get('/theme/sell-home/{theme_id}', 'HomeController@showsellhome');
	Route::get('/theme/cash-offer/{theme_id}', 'HomeController@showcashoffer');
	Route::get('/theme/questions/{theme_id}', 'HomeController@showquestions');
	Route::get('/theme/how-it-works/{theme_id}', 'HomeController@showhowitworks');
	Route::get('/theme/testimonials/{theme_id}', 'HomeController@showtestimonial');
	Route::get('/theme/about-us/{theme_id}', 'HomeController@showaboutus');
   
	
	Route::get('/try-it-free', 
	[
		'uses' => 'HomeController@get_tryitfree',
		'middleware' => 'Checkdomain'
	]);
	Route::get('/about-us', 'HomeController@get_aboutus');
	Route::get('/how-it-works', 'HomeController@get_howitworks');
	Route::get('/pricing', 'HomeController@get_pricing');
	Route::get('/get-website-free', 'HomeController@get_website_free');
	Route::get('/testimonials', 'HomeController@get_testimonials');
	Route::get('/contact-us', 'HomeController@get_contactus');
	Route::post('/add-contactus', 'HomeController@add_contactus');
	Route::get('/affiliate-program', 'HomeController@get_affiliateprogram');
	Route::get('/affiliate-term', 'HomeController@get_affiliateterm');
	Route::get('/privacy-policy', 'HomeController@get_privacypolicy');
	Route::get('/faq', 'HomeController@get_faq');
	Route::post('/add-visitor', 'VisitorsController@addvisitor');
	Route::get('/add-visitor/thankyou', 'VisitorsController@showthankyou');
	// Admin
	Route::get('admin', [
		'uses' => 'AdminController@admin',
		'as' => 'admin',
		'middleware' => 'admin'
	]);

	Route::get('medias', [
		'uses' => 'AdminController@filemanager',
		'as' => 'medias',
		'middleware' => 'redac'
	]);


	//for front user...
	Route::group(['middleware' => ['user']], function ()
	{
		Route::get('/cancel-subscription/{user_id}', 'AgentController@cancelSubscription');
		Route::get('/cancel-subscription/', 'AgentController@cancelSubscription');
		Route::get('/dashboard/{user_id}', 'AgentController@showdashboard');
		Route::get('/account/{user_id}', 'AgentController@showaccount');
		Route::get('/change-password/{user_id}', 'AgentController@changepassword');
		Route::get('/content/{user_id}', 'AgentController@showcontent');
		Route::get('/visitors/{user_id}', 'AgentController@showvisitors');
		Route::post('/addcontent', 'AgentController@addcontent');
		Route::post('/notes', 'AgentController@addnotes');
		Route::get('/referal-url/{user_id}', 'AgentController@showurl');
		Route::get('/affiliates/{user_id}', 'AgentController@showaffiliates');
		Route::post('/adddomainname', 'AgentController@add_domainname');
		Route::match(['get', 'post'], '/getpackages/{user_id}', 'AgentController@getpackages');
		Route::match(['get', 'post'], '/getmember-payment/{user_id}/{pid}/{amount}', 'AgentController@getmemberpackage');
		Route::post('/editprofile', 'AgentController@editagentprofile');
		Route::post('/editpassword', 'AgentController@editagentpassword');
		Route::get('/remove_agent_domain/{agentid}', 'AgentController@remove_domain');
		Route::get('/banners/{user_id}', 'AgentController@showbanner');
		Route::get('/remove_content_logo/{agentid}', 'AgentController@remove_contentlogo');
	});

	//To check domain available or not
	/*Route::group(['middleware' => ['Checkdomain']], function ()
	{
		Route::get('errors/404', 'HomeController@get_tryitfree');
	
	
	}*/
	
	Route::group(['middleware' => ['admin']], function ()
	{
	// for show User in admin...
	Route::get('user/sort/{role}', 'UserController@indexSort');

	Route::get('user/roles', 'UserController@getRoles');
	Route::post('user/roles', 'UserController@postRoles');

	Route::put('userseen/{user}', 'UserController@updateSeen');

	Route::resource('user', 'UserController');

	// Package in admin
	Route::resource('package', 'PackageController');
	Route::get('/uploadpdf', 'PackageController@uploadpdf');
	Route::post('/upload', 'PackageController@uploadfile');
	Route::post('/editpackage', 'PackageController@updatePackage');
    // for service package
	Route::resource('servicepackage', 'ServicePackageController');
	//Route::get('/servicepackage', 'ServicePackageController@getservicepackage');
	Route::post('/editservicepackage', 'ServicePackageController@updatePackage');
	
	// Affiliation Management of admin
	//Route::get('/affiliation', 'AffiliationController@index');
	Route::get('/affiliation', 'AffiliationController@getcommission'); 
	Route::get('/showaddfee', 'AffiliationController@showfees');
	Route::get('/referalfee', 'AffiliationController@showrefview');
	Route::get('/editreferalfee/{fee_id}', 'AffiliationController@editrefview');
	Route::post('/editfees', 'AffiliationController@editreffees');
	Route::post('/addfees', 'AffiliationController@setfees');
	Route::get('/deletereferalfee/{fees_id}', 'AffiliationController@deletefees');
	Route::get('/show-visitors', 'VisitorsController@index');
	Route::get('/email-campaign', 'VisitorsController@showemailcampaign');
	Route::get('/edit-emailsetup/{c_id}', 'VisitorsController@editemailcampaign');
	Route::get('/setup-email', 'VisitorsController@setupemail');
	Route::post('/addemailcontent', 'VisitorsController@addemailcontent');
	Route::post('/editemailcontent', 'VisitorsController@editemailcontent');
	Route::get('/delete-emailsetup/{co_id}', 'VisitorsController@delete_emailcontent');
	Route::get('/test-emailsetup/{temp_id}', 'VisitorsController@testemailsetup');
	Route::post('/sendtestmail', 'VisitorsController@sendtestmail');
	Route::post('/change-status', 'AffiliationController@change_paidstatus');
	//Route::get('/partner-order', 'OrderController@showpartnerorder');
	Route::get('/member-order', 'OrderController@showpartnerorder');
	Route::any('/admin/search/order', 'OrderController@searchorder');
	Route::any('/admin/search/affiliation', 'AffiliationController@searchaffiliation');
	Route::any('/admin/search/visitors', 'VisitorsController@searchvisitors');
	Route::any('/admin/search/users', 'UserController@searchusers');
	Route::get('/show-queries', 'OrderController@show_contact_queries');
	Route::any('/admin/search/contactquery', 'OrderController@search_contact_query');
	Route::get('/reply-queries/{id}', 'OrderController@reply_contact_query');
	Route::post('/sendreply', 'OrderController@send_reply');
	Route::get('/admin/download-orders/', 'OrderController@download_orders');
	Route::get('/admin/download-users/', 'UserController@download_users');
	Route::get('/admin/download-affiliates/', 'AffiliationController@download_affiliates');
	Route::get('/admin/download-visitors/', 'VisitorsController@download_visitors');
	Route::get('/admin/remove_content_logo/{agentid}', 'AgentController@remove_contentlogo');
	Route::get('/admin/cancel-subscription/{agentid}', 'AgentController@cancelSubscription');
        Route::get('/admin/profile', 'AdminController@getprofile');
	Route::post('/admin/editprofile', 'AdminController@editprofile');
	Route::get('/admin/change-password', 'AdminController@changepassword');
	Route::post('/admin/editpassword', 'AdminController@editpassword');
	Route::get('/service-referal-fee', 'ServicePackageController@showrefview');
	Route::get('/showaddservicefee', 'ServicePackageController@showfees');
	Route::post('/addservicefees', 'ServicePackageController@setfees');
	Route::get('/edit-service-referalfee/{fee_id}', 'ServicePackageController@editrefview');
	Route::post('/edit-service-fees', 'ServicePackageController@editreffees');
	Route::get('/delete-service-referalfee/{fees_id}', 'ServicePackageController@deletefees');
    });
	// Authentication routes...
	Route::get('auth/login', 'Auth\AuthController@getLogin');
	Route::post('auth/login', 'Auth\AuthController@postLogin');
	Route::get('auth/logout', 'Auth\AuthController@getLogout');
	Route::get('confirm/{token}/{id}', 'AgentController@getConfirm');

	// Resend routes...
	Route::get('auth/resend/{user_id}', 'AgentController@getResend');

	// Registration routes...
	Route::get('auth/register', 'Auth\AuthController@getthemes');
	Route::match(['get', 'post'], 'auth/getregister', 'Auth\AuthController@getregister');
	Route::match(['get', 'post'], 'auth/get-member-register/{pid}/{amount}', 'Auth\AuthController@getmemberregister');
	Route::post('auth/register', 'Auth\AuthController@postRegister');
	Route::post('auth/checkemail', 'Auth\AuthController@checkemail');
	Route::post('auth/checkdomain', 'Auth\AuthController@checkdomain');
	Route::post('auth/payment', 'Auth\AuthController@addpayment');
	// Password reset link request routes...
	Route::get('password/email', 'Auth\PasswordController@getEmail');
	Route::post('password/email', 'Auth\PasswordController@postEmail');
	// Password reset routes...
	Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
	Route::post('password/reset', 'Auth\PasswordController@postReset');
	Route::get('/referral/{user_id}', 'AgentController@countaffiliates');
	
});