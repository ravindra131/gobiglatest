<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Payment;
use App\Libraries\PaypalPro;
use App\Models\User;

class PaymentCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:paymentstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is for setting up cron for agents who will get the email on the basis of their payment status.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $data = Payment::select('payment.*','users.email as agentEmail','users.username as agentName')
                ->join('users','payment.user_id','=','users.id')
                ->where('transaction_id','!=',0)->orderBy('id','desc')->get();

        if(count($data))
        {
            foreach($data as $row)
            {
                $agentname = $row->agentName;
                $agentmail = $row->agentEmail;
                $profileid = $row->transaction_id;
                $paypal = new PaypalPro;    
                $paypalParams = ['PROFILEID' => $profileid];
                $paypal_profile = $paypal->getRecurringProfileStatus($paypalParams);
                if($row->email_sent == '0')
                {
                    if($paypal_profile['ACK'] == 'Success' && $paypal_profile['STATUS'] == 'Pending')
                    {
                        \Mail::queue('emails.sendEmailpending', compact('agentname'), function ($message) use ($row)
                        {
                            $message->subject('Regarding Payment');
                            $message->to($row->agentEmail);
                            $message->from('support@GoBigPrinting.com','GobigWeb360');
                        });
                        User::where('id','=',$row->user_id)->update(['email_sent' => 1]);
                        echo ('email sent to '.$row->agentEmail.'\n');
                    }
                }
                if($paypal_profile['ACK'] == 'Success' && ($paypal_profile['STATUS'] == 'Suspended' || $paypal_profile['STATUS'] == 'Cancelled'))
                {
                    \Mail::queue('emails.cancelPaymentMail', compact('agentname'), function ($message) use ($row)
                    {
                        $message->subject('Regarding Payment');
                        $message->to($row->agentEmail);
                        $message->from('support@GoBigPrinting.com','GobigWeb360');
                    });
                    User::where('id','=',$row->user_id)->update(['domain_status' => 0]);
                    echo ('email sent to '.$row->agentEmail.'\n');
                }
            }
        }
    }
}
