<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Sendemail;
use DB;

class EmailsCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is for setting up cron for visitors who will get the email on respectivate day differences from date of registration.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = Sendemail::select(DB::raw('distinct(sendemail.id)'), 'sendemail.*','emailcontent.subject','emailcontent.message','visitors.email','users.email as agentEmail','users.username as agentName','webcontent.phone as agentPhone','webcontent.logo as agentLogo')
                ->join('emailcontent','sendemail.template_id','=','emailcontent.id')
                ->join('visitors','visitors.id','=','sendemail.visitor_id')
                ->join('users','visitors.agent_id','=','users.id')
                ->join('webcontent','users.id','=','webcontent.user_id')
                ->where('sendemail.email_date','=',date('Y-m-d'))->where('email_status','=',0)->get();
        if(count($data))
        {
           foreach($data as $row)
           {			   
               $content = $row->message;             
               $content .= '<br />'.$row->agentName;                         
               $content .= '<br />'.$row->agentEmail;             
               $logo = $row->agentLogo;

                \Mail::queue('emails.sendEmailVisitor', compact('content','logo'), function ($message) use ($row) {
                    $message->subject($row->subject);
                    $message->to($row->email);
                    $message->from($row->agentEmail,$row->agentName);
                });
                
                Sendemail::whereid($row->id)->update(['email_status' => 1]);
                echo ('email sent to '.$row->email.'\n');
        
           }
        }
    }
}
