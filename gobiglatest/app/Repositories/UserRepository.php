<?php

namespace App\Repositories;

use App\Models\User, App\Models\Role;
use App\Models\Content;
use App\Models\Payment;
use App\Models\ServicePackage;
use DB;
use App\Libraries\PaypalPro;
class UserRepository extends BaseRepository
{

	/**
	 * The Role instance.
	 *
	 * @var App\Models\Role
	 */	
	protected $role;

	/**
	 * Create a new UserRepository instance.
	 *
   	 * @param  App\Models\User $user
	 * @param  App\Models\Role $role
	 * @return void
	 */
	public function __construct(
		User $user,
		Role $role)
	{
		$this->model = $user;
		$this->role = $role;
	}

	/**
	 * Save the User.
	 *
	 * @param  App\Models\User $user
	 * @param  Array  $inputs
	 * @return void
	 */
  	private function savedata($user, $inputs, $referal_id)
	{		
		if(isset($inputs['seen'])) 
		{
			$user->seen = $inputs['seen'] == 'true';		
		} else {

			$user->username = $inputs['username'];
			$user->email = $inputs['email'];
			$user->password = bcrypt($inputs['password']);
			if(isset($inputs['domain']))
			{
				$user->domain_name = $inputs['domain'];
			}
			
			$user->referal_id = $referal_id;
			if(isset($inputs['theme_id']))
			{
				$user->theme_id = $inputs['theme_id'];
			}
			else
			{
				$user->theme_id = '';
			}
         if($_SERVER['SERVER_NAME'] == 'gobignewsletter.stagemyapp.com')
         {
           $user->referbyid = 0;
           
         }
          else
          {
            $domain = $_SERVER['SERVER_NAME'];
            $user->referbyid = DB::table('webcontent')->where('domain', $domain)->value('user_id');
            
          }
			if(isset($inputs['role'])) {
				$user->role_id = $inputs['role'];	
			} else {
				$role_user = $this->role->where('slug', 'user')->first();
				$user->role_id = $role_user->id;
			}
		}

		$user->save();
	}

	private function save($user, $inputs)
	{		
		if(isset($inputs['seen'])) 
		{
			$user->seen = $inputs['seen'] == 'true';		
		} else {

			$user->username = $inputs['username'];
			$user->email = $inputs['email'];
			//$user->theme_id = $inputs['theme_id'];
			$user->domain_status = $inputs['domain_status'];
			if(isset($inputs['domain_name']) && $inputs['domain_status'] == 1)
			{
				$user->domain_name = $inputs['domain_name'];
			}
			else if(isset($user->domain_name) && $inputs['domain_status'] == 1)
			{
				$user->domain_name = $user->domain_name;
			}
			else
			{
				$user->domain_name = '';
			}
			if(isset($inputs['role'])) {
				$user->role_id = $inputs['role'];	
			} else {
				$role_user = $this->role->where('slug', 'user')->first();
				$user->role_id = $role_user->id;
			}
		}

		$user->save();
	}

	/**
	 * Get users collection paginate.
	 *
	 * @param  int  $n
	 * @param  string  $role
	 * @return Illuminate\Support\Collection
	 */
	public function index($n, $role)
	{
		if($role != 'total')
		{
			return $this->model
			->with('role')
			->whereHas('role', function($q) use($role) {
				$q->whereSlug($role);
			})		
			->oldest('seen')
			->latest()
			->paginate($n);			
		}

		return $this->model
		->with('role')		
		->oldest('seen')
		->latest()
		->paginate($n);
	}

	/**
	 * Count the users.
	 *
	 * @param  string  $role
	 * @return int
	 */
	public function count($role = null)
	{
		if($role)
		{
			return $this->model
			->whereHas('role', function($q) use($role) {
				$q->whereSlug($role);
			})->count();			
		}

		return $this->model->count();
	}

	/**
	 * Count the users.
	 *
	 * @param  string  $role
	 * @return int
	 */
	public function counts()
	{
		$counts = [
			'admin' => $this->count('admin'),
			'redac' => $this->count('redac'),
			'user' => $this->count('user')
		];

		$counts['total'] = array_sum($counts);

		return $counts;
	}

	/**
	 * Create a user.
	 *
	 * @param  array  $inputs
	 * @param  int    $confirmation_code
	 * @return App\Models\User 
	 */
	public function storedata($inputs, $confirmation_code = null, $referal_id)
	{
		$user = new $this->model;
		if($confirmation_code) {
			$user->confirmation_code = $confirmation_code;
		} else {
			$user->confirmed = true;
		}

		$this->savedata($user, $inputs, $referal_id);
		return $user;
	}
     
     public function store($inputs, $confirmation_code = null)
	{
		//print_r($inputs);die;
		$user = new $this->model;
		$user->password = bcrypt($inputs['password']);
		if($confirmation_code) {
			$user->confirmation_code = $confirmation_code;
		} else {
			$user->confirmed = true;
		}

		$this->save($user, $inputs);
		$data['user_id'] = $user->id;
		$data['slider1'] = $inputs['slider_image'];
		if($inputs['domain_status'] == 1)
		{
			$data['domain'] = $inputs['domain_name'].'.users.gobignewsletter.stagemyapp.com';
		}
		else
		{
			$data['domain'] = '';
		}
		$data['companyname'] = $inputs['companyname'];
		$data['facebookurl'] = $inputs['facebookurl'];
		$data['twitterurl'] = $inputs['twitterurl'];
		$data['youtubeurl'] = $inputs['youtubeurl'];
		$data['linkedinurl'] = $inputs['linkedinurl'];
		$data['googleurl'] = $inputs['googleurl'];
		$data['instaurl'] = $inputs['instaurl'];
		if(isset($inputs['logo']))
		{
			$destinationPath = 'uploads/';
			$filename = $inputs['logo']->getClientOriginalName();
			$inputs['logo']->move($destinationPath, $filename);
			$data['logo'] = '/uploads/' . $filename;
		}
		else
		{
			$data['logo'] = '';
		}
		$content = new Content;
		$result = $content->save_user_content($data);
		if($result > 0)
		{
			$payment = new Payment;
        	$payment->user_id = $user->id;
        	$payment->package_id = $inputs['package_id'];
        	if($inputs['package_id'] == 1)
        	{
        		$payment->amount = $inputs['monthly_fee'];
        	}
        	else if($inputs['package_id'] == 2)
        	{
        		$payment->amount = $inputs['annual_fee'];
        	}
        	else if($inputs['package_id'] == 3)
        	{
        		$payment->amount = 0;
        	}
        	$payment->transaction_id = 0;
        	if($payment->save())
        	{
        		return $user;
        	}
		}
	}
	/**
	 * Update a user.
	 *
	 * @param  array  $inputs
	 * @param  App\Models\User $user
	 * @return void
	 */
	public function update($inputs, $user)
	{	
		$user->confirmed = isset($inputs['confirmed']);

		$this->save($user, $inputs);
		$data['user_id'] = $user->id;
		$data['slider1'] = $inputs['slider_image'];
		if($user->domain_status == 1)
		{
			$data['domain'] = $inputs['domain_name'].'.users.gobignewsletter.stagemyapp.com';
		}
		else
		{
			$data['domain'] = '';
		}

		$data['companyname'] = $inputs['companyname'];
		$data['facebookurl'] = $inputs['facebookurl'];
		$data['twitterurl'] = $inputs['twitterurl'];
		$data['youtubeurl'] = $inputs['youtubeurl'];
		$data['linkedinurl'] = $inputs['linkedinurl'];
		$data['googleurl'] = $inputs['googleurl'];
		$data['instaurl'] = $inputs['instaurl'];
		$destinationPath = 'uploads/';
		if(isset($inputs['logo']))
		{
			$filename = $inputs['logo']->getClientOriginalName();
			$inputs['logo']->move($destinationPath, $filename);
			$data['logo'] = '/uploads/' . $filename;
		}
		$content = new Content;
		$results = $content->edit_content_by_agentid($user->id,$data);
		$payment_id = $inputs['paymentid'];
		$datas['package_id'] = $inputs['package_id'];
		if($inputs['package_id'] == 1)
        {
        	$datas['amount'] = $inputs['monthly_fee'];
        }
        else if($inputs['package_id'] == 2)
        {
        	$datas['amount'] = $inputs['annual_fee'];
        }
        else if($inputs['package_id'] == 3)
        {
        	$result = $this->cancelSubscription($user->id);
        	$datas['amount'] = 0;
		}
		if($inputs['transaction_id'] != '')
		{
			$datas['transaction_id'] = $inputs['transaction_id'];
		}
		else
		{
			$datas['transaction_id'] = 0;
		}
		$payment = new Payment;
		$pay_result = $payment->edit_payment_by_agentid($payment_id,$datas);
		if($pay_result == 0)
		{
        	$payment->user_id = $user->id;
        	$payment->package_id = $inputs['package_id'];
        	if($inputs['package_id'] == 1)
        	{
        		$payment->amount = $inputs['monthly_fee'];
        	}
        	else if($inputs['package_id'] == 2)
        	{
        		$payment->amount = $inputs['annual_fee'];
        	}
        	else if($inputs['package_id'] == 3)
        	{
        		$payment->amount = 0;
        	}
        	$payment->transaction_id = 0;
        	$payment->save();
		}
	}

	public function updateseen($inputs, $user)
	{

		$this->save($user, $inputs);
	}

	/**
	 * Get statut of authenticated user.
	 *
	 * @return string
	 */
	public function getStatut()
	{
		return session('statut');
	}

	/**
	 * Valid user.
	 *
     * @param  bool  $valid
     * @param  int   $id
	 * @return void
	 */
	public function valid($valid, $id)
	{
		$user = $this->getById($id);

		$user->valid = $valid == 'true';

		$user->save();
	}

	/**
	 * Destroy a user.
	 *
	 * @param  App\Models\User $user
	 * @return void
	 */
	public function destroyUser(User $user)
	{
		//$user->comments()->delete();
		
		$user->delete();
	}

	/**
	 * Confirm a user.
	 *
	 * @param  string  $confirmation_code
	 * @return App\Models\User
	 */
	public function confirm($confirmation_code)
	{
		$user = $this->model->whereConfirmationCode($confirmation_code)->firstOrFail();

		$user->confirmed = true;
		//$user->confirmation_code = null;
		if(\Auth::check())
		{
			if(\Auth::user()->id == $user->id)
			{
				$user->save();
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			$user->save();
			 \Auth::login($user);
			return true;
		}
		


	}
	public function check_email($email)
	{
		$ids = DB::table('users')->where('email', $email)->lists('email');
		if(count($ids) > 0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	public function check_domain($domain)
	{
		$domains = DB::table('users')->where('domain_name', $domain)->lists('domain_name');
		if(count($domains) > 0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public function cancelSubscription($user_id)
	{
		
		$payment_info = Payment::where('user_id','=',$user_id)->where('transaction_id','!=',0)->orderBy('id','desc')->get();
		$paypal_profile_status = [];
		if(count($payment_info))
		{
			$profileid = $payment_info[0]->transaction_id;
			$paypal = new PaypalPro;	
			$paypalParams = ['PROFILEID' => $profileid, 'ACTION' => 'Cancel']; 
			$paypal_profile_status = $paypal->cancelRecurringProfileStatus($paypalParams);
			return $paypal_profile_status;
		}		
		
	}

}
