<?php namespace App\Repositories;

use App\Models\Package;

class PackageRepository extends BaseRepository {

	/**
	 * Create a new ContactRepository instance.
	 *
	 * @param  App\Models\Contact $contact
	 * @return void
	 */
	public function __construct(Package $package)
	{
		$this->model = $package;
	}

	/**
	 * Save the Package.
	 *
	 * @param  App\Models\User $user
	 * @param  Array  $inputs
	 * @return void
	 */


	/**
	 * Get contacts collection.
	 *
	 * @return Illuminate\Support\Collection
	 */
	public function index()
	{
		return $this->model
		->latest()
		->get();
	}

	
	/**
	 * Update a package.
	 *
	 * @param  bool  $vu
	 * @param  int   $id
	 * @return void
	 */
	


}