<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Package extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'packages';

	/**
	 * One to Many relation
	 *
	 * @return Illuminate\Database\Eloquent\Relations\hasMany
	 */
	 public function updatepackage($data)
    {
    	//echo($data['id']);die;
        DB::table('packages')->where('id', $data['id'])->update(array('monthly_fee' => $data['monthly_fee'], 'annual_fee' => $data['annual_fee']));
    }

}
