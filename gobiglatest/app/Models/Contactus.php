<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Contactus extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contactus';

}
