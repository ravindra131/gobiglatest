<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model; 
use DB;

class Referalpayment extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'referalpayment';

	public function change_payment_status($data)
	{
		$results = DB::update('update referalpayment set paid = 1 where agent_id = ?', [$data['agent_id']]);
		return $results;
	}
	public function getalldata()
	{
		$result = DB::table('referalpayment')->get();
		if($result != '')
		{
			return $result;
		}
	}
	public function getdata()
	{
		$result = self::select('referalpayment.*','users.id',DB::raw('SUM(referalpayment.amount) As totalamount'))
				->join('users','referalpayment.agent_id','=','users.id')
				->orderBy('referalpayment.id', 'desc')
				->groupBy('referalpayment.agent_id')
				->where('users.id','!=','')
				->paginate(10);
				
		if(count($result))
		{
			return $result;
		}

	}
	public function getamount_byagentid($id)
	{
		$result = DB::table('referalpayment')->where('agent_id',$id)->where('paid',1)->sum('amount');
		if($result != '')
		{
			return $result;
		}
	}

	public function getdueamount_byagentid($id)
	{
		$result = DB::table('referalpayment')->where('agent_id',$id)->where('paid',0)->sum('amount');
		if($result != '')
		{
			return $result;
		}
	}

}
