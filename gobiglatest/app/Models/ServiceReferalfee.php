<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ServiceReferalfee extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'servicereferalfees';

	/**
	 * One to Many relation
	 *
	 * @return Illuminate\Database\Eloquent\Relations\hasMany
	 */
	public function savefeeData($data)
	{
		DB::table('servicereferalfees')->insert($data);
	}
	public function getallfees()
	{
		$results = DB::table('servicereferalfees')->get();
		return $results;
	}

	public function getfeesbyid($feeid)
	{
		$result = DB::table('servicereferalfees')->where('id', $feeid)->get();
		if($result != '')
		{
			return $result;
		}
	}

	public function updatefeeData($data)
	{
		DB::table('servicereferalfees')->where('id', $data['feeid'])->update(array('type' => $data['type'], 'monthly_bonus' => $data['monthly_bonus'], 'annual_bonus' => $data['annual_bonus']));
	}

	public function delete_fees($fees_id)
	{
		DB::table('servicereferalfees')->where('id', $fees_id)->delete();
	}

	public function getalldata()
	{
		$result = DB::table('servicereferalfees')->get();
		return $result;
	}
}
