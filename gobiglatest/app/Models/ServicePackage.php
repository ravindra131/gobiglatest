<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ServicePackage extends Model
{
	protected $table = 'service_packages';
	
	public function getServicePackage(){ 
		
	    $result = DB::table('service_packages')->where('user_id','1')->get();
		return $result;
	}
	
	function Userexist($user_id){
		$userresult = DB::table('service_packages')->where('user_id',$user_id)->get();
		return count($userresult);
		
	}
	function getUserPackage(){ 
		$userresult = self::select('service_packages.*','users.username')
				->join('users','service_packages.user_id','=','users.id')
				->where('user_id','1')
				->get();
		return $userresult;
		
	}
	
	public function updatepackage($data)
    {
        DB::table('service_packages')->where('id', $data['id'])->update(array('monthly_fee' => $data['monthly_fee'], 'annual_fee' => $data['annual_fee']));
    }

	
	
	
	
}
