<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Sendemail extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sendemail';

	protected $fillable = ['visitor_id','template_id','email_date','email_status'];

}
