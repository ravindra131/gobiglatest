<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Content extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'webcontent';

	/**
	 * One to Many relation
	 *
	 * @return Illuminate\Database\Eloquent\Relations\hasMany
	 */

	public function save_user_content($data)
	{
		$id = DB::table('webcontent')->insertGetId($data);
		return $id;
	}
	public function getcontent($user_id)
	{
		$result = DB::table('webcontent')->where('user_id', $user_id)->get();
		if($result != '')
		{
			return $result;
		}
	}
	public function edit_user_content($data,$con_id)
	{
		DB::table('webcontent')->where('id', $con_id)->update($data);
	}

	public function edit_content_by_agentid($id,$data)
	{
		$values =  DB::table('webcontent')->where('user_id', $id)->get();
		if(!empty($values))
		{
			DB::table('webcontent')->where('user_id', $id)->update($data);
		}
		else
		{
			DB::table('webcontent')->insertGetId($data);
		}
	}

	public function get_some_content($id)
	{

		$result = DB::table('webcontent')->select('*')->where('user_id', '=', $id)->get();
		if($result != '')
		{
			return $result;
		}
	}
	
	public function addagentdomain($data)
	{
		$result = DB::table('webcontent')->where('user_id', $data['user_id'])->get();
		if(!empty($result))
		{
			$id = DB::table('webcontent')->where('user_id', $data['user_id'])->update(array('agent_domain_check' => $data['agent_domain_check'], 'agent_domain' => $data['agent_domain']));
			return $id;
		}
		else
		{
			return false;
		}
	}
	
	function checkdomainformiddleware(){
		
		return $userresult = DB::select('webcontent.*','users.domain_status')
				->join('users','webcontent.user_id','=','users.id')				
				->where('users.domain_status','=',1)
				->where(function ($query) use ($URL) {
					$query->where('webcontent.agent_domain', '=', $URL);
					$query->orWhere('webcontent.domain', '=', $URL);
				})->get();
	}
}
