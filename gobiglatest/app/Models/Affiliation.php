<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Affiliation extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'affiliation';

	/**
	 * One to Many relation
	 *
	 * @return Illuminate\Database\Eloquent\Relations\hasMany
	 */

	public function insertdata($data)
	{
		$agentid = DB::table('affiliation')->where('agent_id', $data['agent_id'])->get();
		if(count($agentid) > 0)
		{
			return 0;
		}
		else
		{
			$id = DB::table('affiliation')->insertGetId($data);
		    return $id;
		}
	}

	
	
	
	
	public function add_clicks($agent_id)
	{
		$result = DB::update('update affiliation set total_clicks = total_clicks+1 where agent_id = ?', [$agent_id]);
	}

	public function getalldata()
	{
		//$result = DB::table('affiliation')->orderBy('id', 'desc')->paginate(10);
		$result = self::select('affiliation.*','users.id')
				->join('users','affiliation.agent_id','=','users.id')->orderBy('affiliation.id', 'desc')
				->where('users.id','!=','')
				->paginate(10);
		if(count($result))
		{
			return $result;
		}
	}

	
	
	
	public function add_signups($agentid)
	{
		$result = DB::update('update affiliation set total_signup = total_signup+1 where agent_id = ?', [$agentid]);
		return $result;
	}

	public function get_affiliates_by_id($id)
	{
		$result = DB::table('affiliation')->where('agent_id', $id)->get();
		if($result != '')
		{
			return $result;
		}
	}

	public function updatedata($data)
	{
		$updates = DB::update('update affiliation set total_earning = total_earning+payment_due, payment_due = 0 where agent_id = ?', [$data['agent_id']]);
		return $updates;
	}

	public function add_payment($agentid)
	{
		DB::update('update affiliation set total_payment = total_payment+1 where agent_id = ?', [$agentid]);
	}

	public function update_earning($id,$amount)
	{
		DB::table('affiliation')->where('agent_id', $id)->update(array('total_earning' => $amount));
	}

	public function update_due($id, $amount)
	{
		DB::table('affiliation')->where('agent_id', $id)->update(array('payment_due' => $amount));
	}

}
