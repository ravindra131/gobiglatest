<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Visitors extends Model
{

    protected $table = 'visitors'; // table name
    public $timestamps = 'false' ; // to disable default timestamp fields
 	// model function to store form data to database
    public static function getVisitors($userid)
    {
       $results = DB::table('visitors')->where('agent_id', $userid)->orderBy('id', 'desc')->get();
       if($results != '')
    	{
    		return $results;
    	}
    	else
    	{
    		return 0;
    	}
    }

    public function add_notes($data)
    {
       $rid = DB::table('visitors')->where('id', $data['visitorid'])->update(array('notes' => $data['notes']));
       return $rid;
    }

    public function getallvisitors()
    {

        $visitor = self::select('visitors.*','users.id','users.username as agentname','users.domain_name as domainurl')
                ->join('users','visitors.agent_id','=','users.id')->orderBy('visitors.id', 'desc')
                ->where('users.id','!=','')
                ->paginate(10);
        return $visitor;
    }
	public static function saveData($data)
    {
        $id = DB::table('visitors')->insertGetId($data);
        return $id;
    }
}
