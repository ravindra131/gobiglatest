<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Uploads extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'uploadpdf';

	/**
	 * One to Many relation
	 *
	 * @return Illuminate\Database\Eloquent\Relations\hasMany
	 */
	 public static function savefile($data)
    {
        $id = DB::table('uploadpdf')->insertGetId($data);
        return $id;
    }
    public function getfile()
    {
    	$file = DB::table('uploadpdf')->where('id', 1)->get();
    	if($file != '')
    	{
    		return $file;
    	}
    }
    public function updatefile($data,$file_name)
    {
    	DB::table('uploadpdf')->where('filepath', $file_name)->update(array('filepath' => $data['filepath']));
    }
}
