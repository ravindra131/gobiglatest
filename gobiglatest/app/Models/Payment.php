<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Payment extends Model  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'payment';

	public function getalldata_partner()
	{
		//$result = self::orderBy('id', 'desc')->paginate(1);
		$result = self::select('payment.*','users.id','users.username as agentname')
				->join('users','payment.user_id','=','users.id')->orderBy('payment.id', 'desc')
				->where('users.id','!=','')
				->where('users.role_id','=', 3)
				->paginate(10);
		return $result;
	}

	public function getalldatamember()
	{
		//$result = self::orderBy('id', 'desc')->paginate(1);
		$result = self::select('payment.*','users.id','users.username as agentname')
				->join('users','payment.user_id','=','users.id')->orderBy('payment.id', 'desc')
				->where('users.id','!=','')
				->where('users.role_id','=', 2)
				->paginate(10);
		return $result;
	}
	public function edit_payment_by_agentid($id,$data)
	{
		$result = self::where('id', $id)->update(array('package_id' => $data['package_id'], 'amount' => $data['amount'], 'transaction_id' => $data['transaction_id']));
		return $result;
	}
}
