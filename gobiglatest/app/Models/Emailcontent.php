<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Emailcontent extends Model
{

    protected $table = 'emailcontent'; // table name
    public $timestamps = 'false' ; // to disable default timestamp fields
    
    public function insertemailcontent($data)
    {
        $id = DB::table('emailcontent')->insertGetId($data);
        return $id;
    }

    public function showall()
    {
        $content = DB::table('emailcontent')->get();
        return $content;
    }

    public function showcontentbyid($id)
    {
        $result = DB::table('emailcontent')->where('id', $id)->get();
        if($result != '')
        {
            return $result;
        }
    }

    public function update_emailcontent($data)
    {
       $ucontent = DB::table('emailcontent')->where('id', $data['id'])->update(array('subject' => $data['subject'], 'message' => $data['message'], 'duration' => $data['duration'], 'status' => $data['status'])); 
       return $ucontent;
    }

    public function delete_emailcontent($id)
    {
        DB::table('emailcontent')->where('id', $id)->delete();
    }
}
